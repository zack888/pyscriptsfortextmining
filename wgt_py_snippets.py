import boto3
from boto3.dynamodb.conditions import Key, Attr

client = boto3.client(
    'dynamodb',
    # Hard coded strings as credentials, not recommended.
    aws_access_key_id='AKIAJIIVDLEE5GSVPHSQ',
    aws_secret_access_key='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'
)

dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')
table = dynamodb.Table('WGTTask')
analysis = dynamodb.Table('WGTAnalysis')

# Retrieve saved last timestamp
timestamp = 0 # NEED TO LOAD THE SAVED VALUE TO THIS VARIABLE


# This is the latest timestamp to be saved later
latest_timestamp = timestamp

response = table.scan(
    KeyConditionExpression=Key('timestamp').gt(timestamp)
)

print response

for i in response['Items']:
    # Checks if progress is 0% (new task)
    if i['status'] == 0:
        username = i['username']
        i_timestamp = i['timestamp']

        if i_timestamp > latest_timestamp:
            latest_timestamp = i_timestamp

        # THESE ARE VALUES RETRIEVED FOR ANALYSIS
        url = i['url']              # URL FOR MINING
        analysis_type = i['type']   # ANALYSIS TYPE (INTEGER): 0 for Frequency of Mentions (free)
        progress = 1

        # EXECUTE YOUR CODE HERE
        #
        #
        #
        # ######################

        # Edit progress in this line of code, each time there's progress update
        i_response = table.update_item(
            Key={
                'username': username,
                'timestamp': i_timestamp
            },
            UpdateExpression="set status = :r",
            ExpressionAttributeValues={
                ':r': progress
            },
            ReturnValues="UPDATED_NEW"
        )

        # At the end update progress to 100
        progress = 100

        i_response = table.update_item(
            Key={
                'username': username,
                'timestamp': i_timestamp
            },
            UpdateExpression="set status = :r",
            ExpressionAttributeValues={
                ':r': progress
            },
            ReturnValues="UPDATED_NEW"
        )

        # To add analysis to cloud, perform this write:
        a_response = analysis.put_item(
            Item={
                'username': username,
                'timestamp': i_timestamp,
                'type': analysis_type,
                'data': [
                    {
                        "name": "Test1",
                        "secondary": 30,
                        "value": 10
                    },
                    {
                        "name": "Test2",
                        "secondary": 50,
                        "value": 15
                    },
                    {
                        "name": "Test3",
                        "secondary": 20,
                        "value": 8
                    }]
            }
        )

    # End of task execution, move on to next task in for loop

# Finish execution in current run

# Save timestamp again for next run
save(  latest_timestamp  ) # REPLACE SAVE() function WITH ACTUAL IMPLEMENTATION