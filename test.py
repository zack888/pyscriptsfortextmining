# -*- coding: utf-8 -*-


from difflib import SequenceMatcher
from collections import Counter
# def similar(a, b):
#     return SequenceMatcher(None, a, b).ratio()
#
# # print similar('字数|凑|字数| |凑|字数|凑|字数|凑|字数','字数| |凑|字数|凑|字数|凑|字数|凑|字数')
#
# # print Counter(['zzzzz','aaaaaaaaaaaaaaaaaaaa','aaa','aaaa'])
#
# import bisect
# n = int(input())
# contacts = []
#
# for _ in range(n):
#     op, contact = raw_input().strip().split(' ')
#
#     if op == 'add':
#         bisect.insort_left(contacts, contact)
#
#     if op == 'find':
#         left = bisect.bisect_left(contacts, contact)
#         prefix_next = contact[:-1] + chr(ord(contact[-1]) + 1)
#         right = bisect.bisect_left(contacts, prefix_next)
#         print(right - left)
#
# import re
# from collections import Counter
#
# def repetitions(s):
#    r = re.compile(r"(.+?)\1+")
#    for match in r.finditer(s):
#        yield (match.group(1), len(match.group(0))/len(match.group(1)))
#
# def checkRepetitions(l):
#     outputList = []
#     for eachString in l:
#         if list(repetitions(eachString))==[]:
#             outputList.append(eachString)
#             continue
#         if list(repetitions(eachString))[0][1]<3:
#             print list(repetitions(eachString))[0]
#             outputList.append(eachString)
#     return outputList
#
# # for each in checkRepetitions([u'红红火火恍恍惚惚红红火火恍恍惚惚红红火火恍恍惚惚叫姐姐 其实',u'山山水水哈哈哈哈哈哈山山水水哈哈哈哈哈哈啥哈哈哈哈哈哈啥时候']):
# #     print each
# # print checkRepetitions([u'还可以中还可以囤货'])
#
# def checkRepeatedSingleChar(l):
#     outputList = []
#     for eachString in l:
#         eachString1 = eachString.replace("|","")
#         eachCountString = Counter(eachString1).most_common(1)
#         if eachCountString[0][1]<10:
#             outputList.append(eachString)
#
#     return outputList
#
# output = checkRepeatedSingleChar([u'普通洗发水普通洗发水普通洗发水普通洗发水普通洗发水',u'山山水水哈哈哈哈哈哈山山水水哈哈哈哈哈哈啥哈哈哈哈哈哈啥时候'])

def checkAvg(l):
    outputList = []
    for eachString in l:
        avgStore=[]
        pattern=""
        accu = 0
        count = 0
        repeatedStrings = []

        for eachChar in eachString:
            count+=1
            accu += ord(eachChar)
            pattern+=eachChar
            avg = accu / count
            if avg in avgStore:
                repeatedStrings.append(pattern)
            avgStore.append(avg)

    for each in repeatedStrings:
        print each

# print checkAvg([u'红红火火恍恍惚惚红红火火恍恍惚惚红红火火恍恍惚惚叫姐姐 其实红红火火恍恍惚惚红红火火恍恍惚惚红红火火恍恍惚惚叫姐姐'])

def checkBackAvg(s):
    idict={}
    for i in range(len(s)):
        j=i
        jList=[]
        accul=0
        countback = 1
        while j>=0:
            accul += ord(s[j])
            avg = accul / countback
            jList.append(avg)
            j-=1
            countback+=1
        idict[i]=jList

    return idict

# print checkBackAvg(u'叫恍红恍红姐')
# {i:[j]}
# jList is moving average backwards for each i
# idict is a dict to store all j for each i

from difflib import SequenceMatcher
import pandas as pd
import re
from collections import Counter
import xlrd
from xlutils.copy import copy as xl_copy
from datetime import datetime
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy
import jieba

def read_column_in_excel_with_startRow(workbookName,sheetName,columnNumber,startRow):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=startRow
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def jiebaTokenizer(text):
    text=re.sub("\W+", "", text) # remove punctuation
    return list(jieba.cut(text, cut_all=False))

def similar(a, b, score):
    if SequenceMatcher(None, a, b).ratio() > score:
        return True
    else:
        return False

# def spamCheck(df):
#     tfVectorizer = CountVectorizer(tokenizer=jiebaTokenizer)
#     tfVector=tfVectorizer.fit_transform(df.iloc[:,0])
#     print tfVector
#     similarity_matrix = cosine_similarity(tfVector)

#     zz=[]
#     for i in range(0, len(df)):
#         xx=similarity_matrix[i,:]
#         xx[i]=0
#         #if numpy.any(xx>0.95):
#         if numpy.all(xx<0.95):
#             zz.append(df.iloc[i,0])
#     return zz

def spamCheck(inputList):
    tfVectorizer = CountVectorizer(tokenizer=jiebaTokenizer)
    tfVector=tfVectorizer.fit_transform(inputList)
    print tfVector
    similarity_matrix = cosine_similarity(tfVector)

    zz=[]
    for i in range(0, len(inputList)):
        xx=similarity_matrix[i,:]
        xx[i]=0
        #if numpy.any(xx>0.95):
        if numpy.all(xx<0.95):
            zz.append(inputList[i])
    return zz

# column = read_column_in_excel_with_startRow('natalia.xls','yourData',0,0)
# column = list(set(column))
# filteredList = []
# storeList = []
# for j in range(len(column)-1):
#     for i in range(len(column)):
#         if i not in storeList and j!=i and similar(column[j],column[i], 0.95):
#             # break
#             print j
#             print column[j]
#             print i
#             print column[i]
#             print "*************************************"


    # else:
    #     filteredList.append(column[j])

# filteredList = []
# import itertools
# for a, b in itertools.combinations(column, 2):
#     if similar(a, b,0.95):
#         continue
#     else:
#         if a not in filteredList:
#             filteredList.append(a)
#         if b not in filteredList:
#             filteredList.append(b)

filteredList = spamCheck(column)

# for each in filteredList:
#     print each


Edge = namedtuple('Edge', ['src', 'dest'])

tup1 = Edge(0,1)

print (tup1)