# -*- coding: utf-8 -*-

import pandas as pd
import re
from collections import Counter
import nltk
from nltk import ngrams
from nltk.stem import WordNetLemmatizer
from nltk import word_tokenize, pos_tag
from datetime import datetime
import xlrd
import string
from collections import defaultdict
import itertools
from difflib import SequenceMatcher



# beginning of script
startTime = datetime.now()

def read_column_in_excel_with_startRow(workbookName,sheetName,columnNumber,startRow):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=startRow
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def similar(a, b, score):
    if SequenceMatcher(None, a, b).ratio() > score:
        return True
    else:
        return False

def segSentence(paragraph):
        for sent in re.findall(u'[^!?。！，\.\!\?]+[!?。！，\.\!\?]?', paragraph, flags=re.U):
            yield sent

def remove_characters_before_tokenization(sentence, keep_apostrophes=False):
    sentence = sentence.strip()
    if keep_apostrophes:
        PATTERN = r'[?|$|&|*|%|@|(|)|~|.|!|,|"|\'|-|+]'  # add other characters here to remove them
        filtered_sentence = re.sub(PATTERN, r'', sentence)
    else:
        PATTERN = r'[^a-zA-Z0-9 ]'  # only extract alpha-numeric characters
        filtered_sentence = re.sub(PATTERN, r'', sentence)
    return filtered_sentence

def tokenizeBySpace(inputString):
    if inputString == '':
        return []
    else:
        return inputString.split(' ')

def job(filename):

    ################ OPENING FILE AS A STRING ###############################################
    with open(filename+'.txt','r') as file:
        content = file.read()
    file.close()
    content = content.lower()

    ###### cannot take out punctuation as we need to seg sentence later
    # translator = str.maketrans('', '', string.punctuation)
    # content = content.translate(translator)
    content = " ".join(content.split())

    #########################################################################################

    englishstopwords = [ line.strip('\n') for line in open('english_stop_wordsv2.txt',"r") ]

    stopwords = [ x.strip('\n') for x in open('stopwords.txt','r') ]
    
    posList = read_column_in_excel_with_startRow('postaglist.xlsx','workingList',0,0)

    adJVerb = ['JJ','JJR','JJS','RB','RBR','RBS','VB','VBD','VBG','VBN','VBP','VBZ','WRB']

    GAP_PATTERN = r'\s+'
    regex_wt = nltk.RegexpTokenizer(pattern=GAP_PATTERN, gaps=True)
    wnl = WordNetLemmatizer()

    # unigrams counter (lemmentized)
    unicounter = Counter()
    # dict to store sentence:adjverbs
    sendict = {}

    countsen=0
    # xx = list(segSentence(importedList[i]))
    xx = re.split("\\.",content)
    # xx is [u'Hmmmm.', u' He didnt get ousted BC he got a BJ.', u' He got ousted BC he lied under oath about it.', u' That is what you menat I am sure']

    print ("number of sentences is {}".format(len(xx)))

    for sen in xx:
        # sen = remove_characters_before_tokenization(sen, keep_apostrophes=False)
        translator = str.maketrans('', '', string.punctuation)
        sen = sen.translate(translator)
        # words = regex_wt.tokenize(sen.lower())
        
        words = tokenizeBySpace(sen.lower())
        # filtered_words = [v for v in words if v not in stopwords]
        filtered_words = [v for v in words if v!='']
        filtered_words = [wnl.lemmatize(token, 'v') for token in filtered_words]
        wordsPos = pos_tag(filtered_words)
        # wordsPos is [(u'he', u'PRON'), (u'didnt', u'VERB'), (u'get', u'NOUN'), (u'ousted', u'VERB'), (u'bc', u'ADP'), (u'he', u'PRON'), (u'got', u'VERB'), (u'a', u'DET'), (u'bj', u'NOUN')]
        
        sentenceadjverb = []

        for n,m in wordsPos:
            if m in posList: ##### this is a variable on why unigrams might have diff result from shalini.py. because for this case we segment the sentences first then do pos tagging
                if n in unicounter:
                    unicounter[n]+=1
                else: unicounter[n] = 1
            # identifying all verbs and adj in the sentence
            if m in adJVerb and n not in englishstopwords:
                sentenceadjverb.append(n)

        if sen in sendict:
            sendict[sen].extend(sentenceadjverb)
        else:
            sendict[sen]=sentenceadjverb

    unicounter = unicounter.most_common()

    ####### identifying top 40 before combining terms
    count = 0
    top40 = []
    for i in range(len(unicounter)):
        if unicounter[i][0] not in stopwords and count <40:
            ##### print this to check and make sure top 40 is ok
            print ("{}\t{}".format(unicounter[i][0],unicounter[i][1]))
            top40.append(unicounter[i][0])
            count +=1

    ####### identifying which terms could be combined in the top 40
    comdict = defaultdict(list)
    for i in range(len(top40)):
        for key in list(sendict.keys()):
            if top40[i] in key:
                comdict[top40[i]].extend(sendict[key]) # contains duplicates of adj verb, consider using counter here to rank the adj verbs

    ### checking to make sure number of keys in comdict is correct.
    # keys = list(comdict.keys())
    # print (len(keys))

    combined = open('combined.txt','w')
    for each in top40:
        combined.write("%s\t" % each) 
        avlist = list(set(comdict[each])) ### removing adjverb duplicates
        combined.write("%s\n" % avlist)

    combined.close()

    for a, b in itertools.combinations(top40, 2):
        if similar(list(set(comdict[a])), list(set(comdict[b])),0.5):
            print ("{} and {} have similar clusters".format(a,b))

    print (datetime.now() - startTime)

if __name__=='__main__':
    job('male') ### TOGGLE HERE