from functools import cmp_to_key
import operator


########### FEMALE ###########

femaleDict = {}
with open('femaleNormalized.txt') as f:
        twoColumns = f.readlines()
        # you may also want to remove whitespace characters like `\n` at the end of each line
f.close()

for i in range(len(twoColumns)):
    x, y = twoColumns[i].strip().split("\t") ### strip here is important to remove the \n from y
    femaleDict[x]=y

##############################

########### MALE ###########

maleDict = {}
with open('maleNormalized.txt') as f:
        twoColumns = f.readlines()
        # you may also want to remove whitespace characters like `\n` at the end of each line
f.close()

for i in range(len(twoColumns)):
    x, y = twoColumns[i].strip().split("\t") ### strip here is important to remove the \n from y
    maleDict[x]=y

##############################

# print those that are in one but not the other
diff = []
for each in femaleDict:      ######## TOGGLE HERE
    if each not in maleDict:       ######## TOGGLE HERE
        diff.append(each)
# print (diff)     ######## TOGGLE HERE

# store terms which appear in both dicts into an array
same = []
for each in femaleDict:
    if each in maleDict:
        same.append(each)

# create a dict of terms and score diff
termsscorediff = {}
for each in same:
    difference = round(float(femaleDict[each])-float(maleDict[each]),3)
    termsscorediff[each] = difference

# sorted_diff is a list of tuples sorted by score diff
sorted_diff = sorted(termsscorediff.items(), key=operator.itemgetter(1), reverse=True)

# print out female term and scores, sorted by diff
for each in sorted_diff:
    print("{}\t{}".format(each[0], femaleDict[each[0]]))

print ("**********************************************")

# print out male term and scores, sorted by diff
for each in sorted_diff:
    print("{}\t{}".format(each[0], "-"+maleDict[each[0]])) # added negative sign here

print ("**********************************************")

# class CommonTermsAndScores:
#     def __init__(self, term, diff):
#         self.term = term
#         self.diff = diff

# def comparator(a, b):
#     if a.diff!=b.diff:
#         return b.diff -  a.diff

####### TOGGLE HERE to print out overlaps
# termsscorediff = []
for each in same:
    difference = round(float(femaleDict[each])-float(maleDict[each]),2)
    print("{}\t{}".format(each, difference))
    # common = CommonTermsAndScores(each,difference)
    # print (type(common.diff))
    # termsscorediff.append(common)

# termsscorediff = sorted(termsscorediff, key=cmp_to_key(comparator)) 
# for i in termsscorediff:
#     print("{}\t{}".format(i.term, i.diff))


