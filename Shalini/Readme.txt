copy and paste the verbatim into either the male or female.txt

Edit the stop words if necessary

Run shalini.py -> see results in WGT -> stemmed.txt produced with terms which are not in stop words and are in the postal working list

Repeat for male or female, changing the input from male or female.txt

Paste into first and second tab of termscount.xlsx

Then paste ~ first 40 into the comparison tab of the termscount.xlsx

Normalize the data by dividing the count of each term to the number of recos

Copy and paste the normalised scores into maleNormalized.txt and femaleNormalized.txt

Run compareTwoDicts.py to find overlapping and non-overlapping terms, as well as difference in scores

Run phrase.py to find out 2,3,4 grams analysis and see if we can find common phrases. Output is gramsmale.xlsx or gramsfemale.xlsx

Run ngramengshalini.py to get the 11111grams.txt file, then run dataPrepPlotly2.py to get cl11111.json

Copy the cl11111.json into the html and then plot it ;) 

###########################

For running clustering on adj verbs, run shalini.py first to upload the data to wgt

Then run combineterms.py to determine which terms to combine together

Use wgt to combine the terms in the FOM and then copy the table into powerpoint