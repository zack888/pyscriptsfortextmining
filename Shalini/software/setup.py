from cx_Freeze import setup,Executable

includefiles = ['test.txt', 'names.txt', 'named_entity.py','setup.py','numpy']
includes = []
excludes = []
packages = []

setup(
    name = 'myapp',
    version = '0.1',
    description = 'A Name Remover App',
    author = 'zltan',
    author_email = 'tan.z.1@pg.com',
    options = {'build_exe': {'includes':includes,'excludes':excludes,'packages':packages,'include_files':includefiles}}, 
    executables = [Executable('replaceNames.py')]
)