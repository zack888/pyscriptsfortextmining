# -*- coding: utf-8 -*-

from nltk import ne_chunk, pos_tag, word_tokenize
from nltk.tree import Tree
import nltk
import re
from collections import Counter
import string
import sys
import sys
import time
import named_entity
import io
import unicodedata
from scipy import optimize
import numpy.core._methods

def read_text(fileName):
    with io.open(fileName+'.txt', 'r', encoding="utf-8") as content_file:
        content = content_file.read().replace(u'\n', u'')
    content_file.close()
    # content = content.replace(u"’s",u"")
    content = content.replace(u"’s", u"")

    return content

def get_continuous_chunks(text):
    chunked = ne_chunk(pos_tag(word_tokenize(text)))
    print (chunked)
    nameList = []
    for i in chunked:
        if type(i) == Tree:
            if i.label() == 'PERSON':
                nameList.extend([token for token, pos in i.leaves()])
    return nameList
 
##### filter out emamils addrs then combine all names into one long string
def read_names(fileName):
    with io.open(fileName+'.txt', 'r', encoding="utf-8") as names:
        nameString = names.read().replace('\n', '')
    names.close()

    nameString = nameString.lower()
    nameString = re.sub('<[^>]+>', '', nameString)
    nameString = nameString.replace(",","")
    nameList = nameString.split(";")

    for i in range(len(nameList)):
        nameList[i] = nameList[i].strip()

    nameList = list(set(nameList)) ### this is wrong, should remove duplicates based on email, not name. ###
    nameListTuple = [(a.split()[0],a.split()[1]) for a in nameList]
    firstNameList = [x.split()[1] for x in nameList]
    lastNameList = [x.split()[0] for x in nameList]

    allFirstLast = firstNameList + lastNameList
    allNameString = ''.join(allFirstLast)

    firstNameString = ''.join(firstNameList)

    cntAllFirstLast = Counter(allFirstLast)

    # return allNameString
    return allFirstLast

if __name__ == "__main__":

    # try:
        print("Name of text file to read:")
        content = read_text(input())

        ##### NLP method to filter away
        listFilter = get_continuous_chunks(content)

        print (listFilter)

        contentList = content.split(" ")
        newContentList = []

        for each in contentList:
            if each not in listFilter:
                newContentList.append(each)

        content = u" ".join(newContentList)

        # filter out punctuation at this stage only so that chunking can work
        translator = str.maketrans('', '', string.punctuation)
        content = content.translate(translator)
        # make it lowercase only now so that named entity analysis can work
        content = content.lower()

        ##### brute force method from the mailing list
        allNameList = read_names("names")
        # print (allNameString)
        contentList = content.split(" ")
        newContentList = []

        exceptionList = ['led']

        for each in contentList:
            for eachName in allNameList:
                if each in eachName:
                    if each in exceptionList:
                        continue
                    else:
                        break
            else:
                newContentList.append(each)

        text_file = open("filtered.txt", "w")
        text_file.write(u" ".join(newContentList))
        text_file.close()

        time.sleep(5)

    # except:
    #     print(Exception)
    #     print ("error!!! Contact zhi long.")
    #     time.sleep(10)
