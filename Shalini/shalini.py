# -*- coding: utf-8 -*-

from nltk import word_tokenize, pos_tag
from nltk.stem.porter import PorterStemmer
from collections import Counter
import xlrd
import string
import boto3
from nltk.stem import WordNetLemmatizer
import decimal

#############################
#############################
# tokenize -> POS Tag -> Unstemmed/Stemmed
#############################
#############################
#### Let's try video next time! ########

###### CREDENTIALS STUFF ###############
dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')

client = boto3.client(
    'dynamodb',
    aws_access_key_id='AKIAJQZCRP2UQ2GCPXVQ',
    aws_secret_access_key='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',
    region_name='ap-southeast-1'
)

table = dynamodb.Table('WGTTask')
analysis = dynamodb.Table('WGT_Analysis')
###### CREDENTIALS STUFF ###############

def putItem(username,timestamp,data,analysis):
    a_response = analysis.put_item(
        Item={
            'timestamp': timestamp,
            'type': 0,
            'username': username,
            'data': data
        }
    )

porter_stemmer = PorterStemmer()
wnl = WordNetLemmatizer()

def read_column_in_excel_with_startRow(workbookName,sheetName,columnNumber,startRow):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=startRow
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def tokenizeBySpace(inputString):
    if inputString == '':
        return []
    else:
        return inputString.split(' ')

def postagFilter(inputList,posList):
    return [x[0] for x in inputList if x[1] in posList]

def shalini(inputdoc,posList):
    
    wordListUnStem = []
    wordListStemmed = []
    posList = posList

    with open(inputdoc+'.txt','r') as file:
        content = file.read()
    file.close()

    content = content.lower()
    ###### REMOVING punctuation here before pos tagging, is questionable. TO COME BACK HERE AGAIN####
    translator = str.maketrans('', '', string.punctuation)
    content = content.translate(translator)
    ####################################################################
    content = " ".join(content.split())

    # wordListEachReco = word_tokenize(content)
    wordListEachReco = tokenizeBySpace(content)
    # print (wordListEachReco)
    postagged = pos_tag(wordListEachReco)
    wordListEachReco = postagFilter(postagged,posList)

    # unstemmed 
    wordListUnStem.extend(wordListEachReco)
    
    # stemmed
    # wordListStemmed.extend([porter_stemmer.stem(x) for x in wordListEachReco])
    wordListStemmed.extend([wnl.lemmatize(token, 'v') for token in wordListEachReco])
    
    cntUnstemmed = Counter(wordListUnStem).most_common()
    cntStemmed = Counter(wordListStemmed).most_common()

    #################### unstemmed ####################
    # data = [{
    #             "name": "",
    #             "secondary": 0,
    #             "value": 0
    #         } for x in range(0, len(cntUnstemmed), 1)]

    data = []
    for i in range(len(cntUnstemmed)):
        data.append({"name":cntUnstemmed[i][0],"value":cntUnstemmed[i][1]})

    putItem('6985e521-0bcf-4825-8488-b1bc9c8cde06',int(1526645302302),data,analysis)

    #################################################

    #################### stemmed ####################
    data = []
    thefile = open('stemmed.txt','w')

    with open('stopwords.txt') as f:
        stopwords = f.readlines()
        # you may also want to remove whitespace characters like `\n` at the end of each line
        stopwords = [x.strip() for x in stopwords] 
    f.close()

    for i in range(len(cntStemmed)):
        # data.append({"name":cntStemmed[i][0],"value":cntStemmed[i][1]})
        if cntStemmed[i][0] not in stopwords:
            data.append({"name":cntStemmed[i][0],"value":cntStemmed[i][1]})
            thefile.write("%s\t" % cntStemmed[i][0]) 
            thefile.write("%s\n" % cntStemmed[i][1])
        #### for word cloud scaling
        # data.append({"name":cntStemmed[i][0],"value":decim(float((cntStemmed[i][1])/top[0][1])*100)})
    thefile.close()

    putItem('6985e521-0bcf-4825-8488-b1bc9c8cde06',int(1526645302303),data,analysis)
    #################################################

posList = read_column_in_excel_with_startRow('postaglist.xlsx','workingList',0,0)
# verbatimList = read_column_in_excel_with_startRow('recos.xlsx','Sheet1',0,0)

shalini('male',posList) ### TOGGLE
