import pandas as pd
from collections import Counter
import xlrd


def read_column_in_excel_with_startRow(workbookName,sheetName,columnNumber,startRow):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=startRow
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList


#### import with xls or xlsx - result file from WGT
xl=pd.ExcelFile('carFebreeze_ngrams_results.xlsx')
# check sheet_names
#print(xl.sheet_names)
df = xl.parse('4grams')

gramsList = df.iloc[:,0] ############# CHINESE ###############
# gramsList = df.iloc[:,3] ############ ENGLISH ##################


countList = df.iloc[:,1]
# ratingList = read_column_in_excel_with_startRow('carFebreeze_ngrams_results.xlsx','4grams',2,1)
ratingList = df.iloc[:,2]

firstTwoList = []
secTwoList = []
ratingList2 = []
for i in range(len(gramsList)):
    splitTerms = gramsList[i].split("|")
    if len(splitTerms) ==4:
        for j in range(countList[i]):
            firstTwoList.append((splitTerms[0] + " " + splitTerms[1]))
            secTwoList.append((splitTerms[2] + "|" + splitTerms[3]) + "|")
            ratingList2.append(ratingList[i])

n2grams_Pair = pd.DataFrame(dtype='object')
ngramsVerAdj_per_ngram = pd.DataFrame(dtype='object')
rating = pd.DataFrame(dtype='object')

for i in range(len(firstTwoList)):
    n2grams_Pair = n2grams_Pair.append([firstTwoList[i]]) ### can only append lists
    ngramsVerAdj_per_ngram = ngramsVerAdj_per_ngram.append([secTwoList[i]])
    rating = rating.append([ratingList2[i]])

results = pd.concat([n2grams_Pair,ngramsVerAdj_per_ngram,rating], axis=1)
results.columns = ['Pair','AdjNVerb','Rating']
# dont need the line below since we are getting from a sorted excel sheet
# results = results.sort_values(by=['Pair'], axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
results.to_csv('test'+'grams.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")