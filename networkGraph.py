
import igraph as ig
import json
import codecs

data = json.load(codecs.open('testData.txt', 'r', 'utf-8-sig'))

print data.keys()
N=len(data['nodes'])
print 'there are ' +str(N)+ ' number of nodes'

L=len(data['links'])
print 'there are ' +str(L)+ ' number of links'

Edges=[(data['links'][k]['source'], data['links'][k]['target']) for k in range(L)]
# Edges=[(1, 0), (2, 0), (3, 0), (3, 2), (4, 0)]

G=ig.Graph(Edges, directed=True)
print type(G)
print G

labels=[]
group=[]
dotSize=[]
for node in data['nodes']:
    labels.append(node['name'])
    group.append(node['group'])
    dotSize.append(node["size"])

textSize = []
for eachDot in dotSize:
    if eachDot>15:
        textSize.append(eachDot)
    else:
        textSize.append(15)

lineThickness = []
for link in data['links']:
    lineThickness.append(link["value"])

# layt=G.layout('kk', dim=3)
layt=G.layout(None, dim=3)
for each in layt:
    print each
print layt

Xn=[layt[k][0] for k in range(N)]# x-coordinates of nodes
Yn=[layt[k][1] for k in range(N)]# y-coordinates
Zn=[layt[k][2] for k in range(N)]# z-coordinates
Xe=[]
Ye=[]
Ze=[]

# None is needed so that there would not be a consecutive line that joins all the points
for e in Edges:
    Xe+=[layt[e[0]][0],layt[e[1]][0], None]# x-coordinates of edge ends
    Ye+=[layt[e[0]][1],layt[e[1]][1], None]
    Ze+=[layt[e[0]][2],layt[e[1]][2], None]


print Xe
print Ye
print Ze

import plotly
from plotly.graph_objs import *

trace1=Scatter3d(x=Xe,
               y=Ye,
               z=Ze,
               mode='lines',
               line=Line(color='rgb(125,125,125)', width=lineThickness),
               hoverinfo='none'
               )
trace2=Scatter3d(x=Xn,
               y=Yn,
               z=Zn,
               mode='markers+text',
               name='actors',
                textfont=dict(
                        size=20
                    ),
               marker=Marker(symbol='dot',
                             # try array here for size
                             size=dotSize,
                             color=group,
                             colorscale='Viridis',
                             line=Line(color='rgb(50,50,50)', width=0.5)
                             ),
               text=labels,
               hoverinfo='text'
               )

axis=dict(showbackground=False,
          showline=False,
          zeroline=False,
          showgrid=False,
          showticklabels=False,
          title=''
          )

layout = Layout(
         title="nGrams Network Graph",
         width=2000,
         height=2000,
         showlegend=False,
         scene=Scene(
         xaxis=XAxis(axis),
         yaxis=YAxis(axis),
         zaxis=ZAxis(axis),
        ),
     margin=Margin(
        t=100
    ),
    hovermode='closest',
    annotations=Annotations([
           Annotation(
           showarrow=False,
            text="wheregottext.com",
            xref='paper',
            yref='paper',
            x=0,
            y=0.1,
            xanchor='left',
            yanchor='bottom',
            font=Font(
            size=14
            )
            )
        ]),    )

data=Data([trace1, trace2])
fig=Figure(data=data, layout=layout)

plot_url = plotly.offline.plot(fig, filename="clusterPlot",auto_open=True,config={'displayModeBar':False})