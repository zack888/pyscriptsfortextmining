#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  1 23:01:38 2018

@author: tzehau
"""

import pynlpir
import jieba
import jieba.posseg as pseg
import jieba.analyse
from nltk.tag.stanford import CoreNLPPOSTagger
from nltk.tag.stanford import CoreNLPNERTagger
import numpy as np
import pandas as pd
import re
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types


def segSentence(paragraph):
    for sent in re.findall(u'[^!?。！\.\!\?]+[!?。！\.\!\?]?', paragraph, flags=re.U):
        yield sent


### run pynlpir function
def pynlpirResults(text, stopwords):
    pynlpir.open(encoding="utf_8")
    commentIndex=1
    dfCommentNo = pd.DataFrame()
    dfSentence = pd.DataFrame(dtype="object")
    dfSentenceSW = pd.DataFrame(dtype="object")
    dfPOS = pd.DataFrame(dtype="object")
    dfKeyWords = pd.DataFrame(dtype="object")

    for comment in text:    
        xx=list(segSentence(comment))
        for sen in xx:
            pos = pynlpir.segment(sen)
            reStopWords_pos = [v for v in pos if v[0] not in stopwords]
            #pos = pynlpir.segment(sen, pos_names='all')
            KW = pynlpir.get_key_words(sen, weighted=False)
            dfCommentNo = dfCommentNo.append([commentIndex])
            dfSentence = dfSentence.append([sen])
            dfSentenceSW = dfSentenceSW.append([''.join(str(v[0]) for v in reStopWords_pos) + "。"]) 
            dfPOS = dfPOS.append([';'.join(str(v) for v in reStopWords_pos)]) 
            dfKeyWords = dfKeyWords.append([';'.join(str(v) for v in KW)])
        commentIndex +=1
    results=pd.concat([dfCommentNo, dfSentence, dfSentenceSW, dfPOS, dfKeyWords], axis=1)
    results=results.replace('',np.NaN)
    results.columns=['Comment_No', 'Sentence', 'RmSW_Sentence', 'POS', 'KeyWords']
    results.to_csv('pynlpir_SWresults.tsv', header=True, index=None, sep='\t', mode='w', na_rep="NaN")
    return results

################## jieba  ###########
def jiebaResults(text, stopwords):
    jieba.enable_parallel(4)
    commentIndex=1
    dfCommentNo = pd.DataFrame()
    dfSentence = pd.DataFrame(dtype="object")
    dfSentenceSW = pd.DataFrame(dtype="object")
    dfPOS = pd.DataFrame(dtype="object")
    dfKeyWords = pd.DataFrame(dtype="object")

    for comment in text:    
        xx=list(segSentence(comment))
        for sen in xx:
            pos = list(pseg.cut(sen))
            pos=[tuple(v) for v in pos]
            reStopWords_pos = [v for v in pos if v[0] not in stopwords]
            KW = list(jieba.analyse.extract_tags(sen, withWeight=False))
            #KW = jieba.analyse.textrank(sen, topK=20, withWeight=False, allowPOS=('ns', 'n', 'vn', 'v'))
            dfCommentNo = dfCommentNo.append([commentIndex])
            dfSentence = dfSentence.append([sen])
            dfSentenceSW = dfSentenceSW.append([''.join(str(v[0]) for v in reStopWords_pos) + "。"]) 
            dfPOS = dfPOS.append([';'.join(str(v) for v in pos)]) 
            dfKeyWords = dfKeyWords.append([';'.join(str(v) for v in KW)])
        commentIndex +=1
    results=pd.concat([dfCommentNo, dfSentence, dfSentenceSW, dfPOS, dfKeyWords], axis=1)
    results=results.replace('',np.NaN)
    results.columns=['Comment_No', 'Sentence', 'RmSW_Sentence', 'POS', 'KeyWords']
    results.to_csv('jieba_SWresults.tsv', header=True, index=None, sep='\t', mode='w', na_rep="NaN")
    return results

####### stanford coreNLP
def stanfordResults(text, stopwords):
    stf_tag=CoreNLPPOSTagger(url='http://localhost:9001')
    NERTag = CoreNLPNERTagger(url='http://localhost:9001')

    commentIndex=1
    dfCommentNo = pd.DataFrame()
    dfSentence = pd.DataFrame(dtype="object")
    dfSentenceSW = pd.DataFrame(dtype="object")
    dfPOS = pd.DataFrame(dtype="object")
    dfEntity = pd.DataFrame(dtype="object")

    for comment in text:    
        xx=list(segSentence(comment))
        for sen in xx:
            pos = stf_tag.tag(sen.split())
            NER = NERTag.tag(sen.split())
            reStopWords_pos = [v for v in pos if v[0] not in stopwords]
            dfCommentNo = dfCommentNo.append([commentIndex])
            dfSentence = dfSentence.append([sen])
            dfSentenceSW = dfSentenceSW.append([''.join(str(v[0]) for v in reStopWords_pos) + "。"]) 
            dfPOS = dfPOS.append([';'.join(str(v) for v in pos)]) 
            dfEntity = dfEntity.append([';'.join(str(v) for v in NER if v[1]!='O')])
        commentIndex +=1
    results=pd.concat([dfCommentNo, dfSentence, dfSentenceSW, dfPOS, dfEntity], axis=1)
    results=results.replace('',np.NaN)
    results.columns=['Comment_No', 'Sentence', 'RmSW_Sentence', 'POS', 'Entity']
    results.to_csv('stanford_SWresults.tsv', header=True, index=None, sep='\t', mode='w', na_rep="NaN")
    return results

def googleSentiment(textFile):
    """Run a sentiment analysis request on text within a passed filename."""
    client = language.LanguageServiceClient()

    with open(textFile, 'r') as review_file:
        # Instantiates a plain text document.
        content = review_file.read()

    document = types.Document(content=content,type=enums.Document.Type.PLAIN_TEXT)
    annotations = client.analyze_sentiment(document=document)
    return(annotations)


text = []
TestFile = open('/Users/tzehau/Projects/nlp_sentiments/test.txt', 'r', encoding='utf8', errors='ignore')
for line in TestFile:
    text.append(line.rstrip("\n"))
#text=text[45:50]
## remove stop words

stopwords = [ line.rstrip() for line in open('./Chinese_sentiment_analysis-master/data/chinese_stop_wordsV2.txt',"r") ]

pynlpirOutput=pynlpirResults(text, stopwords)

### write file for googleAPI sentiment analysis
pynlpirOutput.iloc[:,2].to_csv('INgoogleAPI.txt', header=False, index=None, mode='w')
### send for analysis
annotations=googleSentiment('/Users/tzehau/Projects/nlp_sentiments/INgoogleAPI.txt')
dfSenScore = pd.DataFrame()
for index, sentence in enumerate(annotations.sentences):
    sentence_sentiment = sentence.sentiment.score
    dfSenScore = dfSenScore.append([sentence_sentiment])
        
pynlpirOutput=pd.concat([pynlpirOutput, dfSenScore], axis=1)
pynlpirOutput.columns=['Comment_No', 'Sentence', 'RmSW_Sentence', 'POS', 'KeyWords', 'Sentiment_Score']
pynlpirOutput = pynlpirOutput.drop_duplicates(subset=['Sentence'], keep=False)
pynlpirOutput.to_csv('pynlpir_SWresults.tsv', header=True, index=None, sep='\t', mode='w', na_rep="NaN")

## jieba results
jiebaOutput=jiebaResults(text, stopwords)
jiebaOutput=pd.concat([jiebaOutput, dfSenScore], axis=1)
jiebaOutput.columns=['Comment_No', 'Sentence', 'RmSW_Sentence', 'POS', 'KeyWords', 'Sentiment_Score']
jiebaOutput = jiebaOutput.drop_duplicates(subset=['Sentence'], keep=False)
jiebaOutput.to_csv('jieba_SWresults.tsv', header=True, index=None, sep='\t', mode='w', na_rep="NaN")

## stanford results
stanfordOutput=stanfordResults(text, stopwords)
stanfordOutput=pd.concat([stanfordOutput, dfSenScore], axis=1)
stanfordOutput.columns=['Comment_No', 'Sentence', 'RmSW_Sentence', 'POS', 'Entity', 'Sentiment_Score']
stanfordOutput = stanfordOutput.drop_duplicates(subset=['Sentence'], keep=False)
stanfordOutput.to_csv('stanford_SWresults.tsv', header=True, index=None, sep='\t', mode='w', na_rep="NaN")


