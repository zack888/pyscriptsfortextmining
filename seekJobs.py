from bs4 import BeautifulSoup
import time
from selenium import webdriver
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import xlwt
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from time import sleep
import threading
from threading import Thread
from threading import Timer
from selenium.webdriver import ActionChains
import xlrd
from xlutils.copy import copy as xl_copy
import sys
import random
import re
import ast
import schedule
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

path_to_chromedriver = 'C:\Users\ZL\Desktop\chromedriver.exe'
browser = webdriver.Chrome(executable_path = path_to_chromedriver)
timeout_time=10
browser.set_page_load_timeout(timeout_time)
actions=ActionChains(browser)
# wait60 = WebDriverWait(browser, 60)
# wait10 = WebDriverWait(browser, 10)
# wait5 = WebDriverWait(browser, 5)

search_Url='https://www.seek.com.au/jobs-in-engineering/in-All-Melbourne-VIC?salaryrange=150000-999999&salarytype=annual'

seek_urls = [search_Url]

def get_page_source(url):
    def get_url_edit():
        print 'starting t1'
        browser.get(url)
        # actions.send_keys(Keys.END)
        # actions.perform()
        #time.sleep(10)
        browser.implicitly_wait(10)
        print 'ending t1'
    t1 = threading.Thread(target=get_url_edit)
    # t1 = Timer(timeout_time+10,get_url_edit)
    t1.setDaemon(True)
    t1.start()
    def return_page_source():
        print 'starting t2'
        print 'ending t2'
    t2 = Timer(timeout_time+10, return_page_source)
    t2.start()
    t2.join()
    #return browser.page_source

if __name__ == '__main__':

    jobsListCount = 0
    allLinksCount = 0
    allDescriptionsCount = 0
    lenJobList = 0
    # variables to add to output json
    runCount = 0
    falseTrueChecks = []
    listOfJobLists = []
    jsonForUpdate={"name":"seek","updated":""}

    def job():

        jobsList = []
        descriptionsList = []
        allLinks = []

        # variables to add to output json
        global lenJobList
        global listOfJobLists
        global runCount

        for each in seek_urls:
            get_page_source(each)
            for x in range(2,3,1):
                try:
                    print x
                    html = browser.page_source
                    soup = BeautifulSoup(unicode(html), "html.parser")
                    allJobs = soup.findAll('span', attrs={'class': '_3FrNV7v _3Eb3poo HfVIlOd E6m4BZb'})
                    for each in allJobs:
                        jobsList.append(each.get_text())
                        #print each.get_text()

                    print jobsList

                    # variables to add to output json
                    listOfJobLists.append(jobsList)

                    for div in allJobs:
                        #only want to find the first link, so using find instead of findall
                        link = div.find('a')
                        #for a in links:
                        allLinks.append('https://www.seek.com.au'+ link['href'])

                    print allLinks

                    # for elem in browser.find_elements_by_xpath('//*[@id="162d0ca3-4e77-49c0-99a6-3998d16b1968"]/div[1]/div[1]/div[2]/div[2]/p/span[2]'):
                    #     print elem.text
                    descriptions = soup.findAll('div', attrs={'class': '_1mzsMx5'})
                    for each in descriptions:
                        descriptionsList.append(each.get_text())

                    print descriptionsList

                    time.sleep(10+random.uniform(1.5,1.9))

                except:
                    e = sys.exc_info()[0]
                    print e
                    break
            time.sleep(0)

        # open existing workbook
        rb = xlrd.open_workbook('aussiejobs.xls', formatting_info=True)
        # make a copy of it
        wb = xl_copy(rb)
        # pull a sheet by name
        Sheet1 = wb.get_sheet(3)

        global jobsListCount
        global allDescriptionsCount
        global allLinksCount

        for each in jobsList:
            Sheet1.write(jobsListCount, 0, each)
            jobsListCount += 1

        for each in descriptionsList:
            Sheet1.write(allDescriptionsCount, 1, each)
            allDescriptionsCount += 1

        for each in allLinks:
            Sheet1.write(allLinksCount, 2, each)
            Sheet1.write(allLinksCount, 3, time.asctime(time.localtime(time.time())))
            allLinksCount += 1

        print "saving ausJobs into excel"
        wb.save("aussiejobs.xls")

        #chunk of code that checks for updates
        if runCount>0:
            print "checking if there has been any updates in jobs"
            if listOfJobLists[runCount]==listOfJobLists[runCount-1]:
                print "False"
                falseTrueChecks.append('False')
                jsonForUpdate["updated"]="False"
            else:
                print "True"
                falseTrueChecks.append('True')
                jsonForUpdate["updated"] = "True"
        print "The runcount is: " + str(runCount)
        runCount += 1
        print falseTrueChecks
        print "jsonForUpdate = " +jsonForUpdate["updated"]

    schedule.every(5).seconds.do(job)
    #schedule.every(8).hours.do(job)

    while True:
        schedule.run_pending()
        time.sleep(1)




