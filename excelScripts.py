def save_element_in_excel(fileName,sheetNumber,columnNumber,rowNumber,element):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    Sheet1.write(rowNumber, columnNumber, element)
    wb.save(fileName)

def create_new_workbook_random(fileName, sheetName):
    book = xlwt.Workbook()
    sh = book.add_sheet(sheetName)
    newExcelName=""
    ranGen = str(randint(1, 1000000000))
    print ranGen
    try:
        xlrd.open_workbook(fileName+ranGen+".xls")
        print "trying to save excel with a different name"
        create_new_workbook_random(fileName, sheetName)
    except:
        book.save(fileName + ranGen + ".xls")
        newExcelName = fileName + ranGen + ".xls"
        print "New Excel Workbook " + newExcelName + " created."
    return newExcelName
