import xlwt
import xlrd

ngrams = [line.strip('\n') for line in open('n2grams.txt', "r")]

firstColandadjverbs = {}
firstColandadjverbs['#']=[]
for i in range(1, len(ngrams)):
    a, b = ngrams[i].split('\t')
    if a[0] == '#':
        firstColandadjverbs['#'].append(a)
    else:
        firstColandadjverbs[a]=b.split('|')
