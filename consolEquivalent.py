
def genlistOfTenLists(wordList,verbatimList,sentiList):
    listOfTenLists=[]
    for eachWord in wordList:
        countVerbatim=0
        eachWordList=[]
        for eachVerbatim in verbatimList:
            if eachWord in eachVerbatim:
                eachWordList.append(sentiList(countVerbatim))
            else:
                eachWordList.append('')
            countVerbatim+=1
        listOfTenLists.append(eachWordList)

    return listOfTenLists

def findPercentageList(countList,verbatimList):
    percentageList=[]
    for eachcount in countList:
        eachPercentage=(eachcount/len(verbatimList))*100
        percentageList.append(eachPercentage)

    return percentageList

def acculList(percentageList):
    outputList = [''] * len(percentageList)
    finalElement = sum(percentageList)
    outputList[-1] = str(finalElement) + '%'

    return outputList

def listOfNegPercentage(listofTenLists):
    listOfNeg = []
    for eachList in listofTenLists:
        countNum=0.0
        countNeg=0.0
        for eachval in eachList:
            if eachval<0:
                countNum += 1
                countNeg+=1
            elif eachval!='':
                countNum += 1
        negPercent=round((countNeg/countNum)*100,0)
        listOfNeg.append(negPercent)

    return listOfNeg

def top3senti(verbatimList,listofTenLists):
    listOfTop3SentiLists=[]
    listofUsedPositions=[]
    for eachList in listofTenLists:
        # create a list variable to limit 3 appends to used positions
        limit3=[]
        # list to store top 3 verbatim for each list
        top3SentiList=[]
        # m is max value for eachList
        # seems lik max always picks out the '' blanks
        m=max([i for i in eachList if i!=''])
        # this is a list to store the positions where the maxvalue is found
        maxPositions=[i for i, j in enumerate(eachList) if j == m]
        print maxPositions
        # there is a chance that there are 3 or more 0.95 values
        if len(maxPositions)>2:
            for eachPos in maxPositions:
                if eachPos not in listofUsedPositions:
                    if len(limit3) <3:
                        top3SentiList.append(verbatimList[eachPos])
                        #filter to top 3 verbatim
                        top3SentiList=top3SentiList[:3]
                        listofUsedPositions.append(eachPos)
                        limit3.append('count')
        #what if there are less than 3 0.95 values
        else:
            # finding senti values betwen 0.7 and 0.95
            listOfmoreValues=[i for i in eachList if 0.7<i<0.95]
            # finding positions that have senti in that range
            maxPositionsExtend = [i for i, j in enumerate(eachList) if j in listOfmoreValues]
            maxPositions.extend(maxPositionsExtend)
            for eachPos in maxPositions:
                if eachPos not in listofUsedPositions:
                    if len(limit3) < 3:
                        top3SentiList.append(verbatimList[eachPos])
                        #filter to top 3 verbatim
                        top3SentiList=top3SentiList[:3]
                        listofUsedPositions.append(eachPos)
                        limit3.append('count')
        print listofUsedPositions
        listOfTop3SentiLists.append(top3SentiList)

    return listOfTop3SentiLists

def btm3senti(verbatimList,listofTenLists):
    listOfTop3SentiLists=[]
    listofUsedPositions=[]
    for eachList in listofTenLists:
        # create a list variable to limit 3 appends to used positions
        limit3=[]
        # list to store top 3 verbatim for each list
        top3SentiList=[]
        # m is max value for eachList
        # seems lik max always picks out the '' blanks
        m=min([i for i in eachList if i!=''])
        # this is a list to store the positions where the maxvalue is found
        maxPositions=[i for i, j in enumerate(eachList) if j == m]
        print maxPositions
        # there is a chance that there are 3 or more 0.95 values
        if len(maxPositions)>2:
            for eachPos in maxPositions:
                if eachPos not in listofUsedPositions:
                    if len(limit3) <3:
                        top3SentiList.append(verbatimList[eachPos])
                        #filter to top 3 verbatim
                        top3SentiList=top3SentiList[:3]
                        listofUsedPositions.append(eachPos)
                        limit3.append('count')
        #what if there are less than 3 0.95 values
        else:
            # finding senti values betwen 0.7 and 0.95
            listOfmoreValues=[i for i in eachList if -0.95<i<0]
            # finding positions that have senti in that range
            maxPositionsExtend = [i for i, j in enumerate(eachList) if j in listOfmoreValues]
            maxPositions.extend(maxPositionsExtend)
            for eachPos in maxPositions:
                if eachPos not in listofUsedPositions:
                    if len(limit3) < 3:
                        top3SentiList.append(verbatimList[eachPos])
                        #filter to top 3 verbatim
                        top3SentiList=top3SentiList[:3]
                        listofUsedPositions.append(eachPos)
                        limit3.append('count')
        print listofUsedPositions
        listOfTop3SentiLists.append(top3SentiList)

    return listOfTop3SentiLists


