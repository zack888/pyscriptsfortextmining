from subprocess import Popen, PIPE
import subprocess
import shlex
import argparse
import io
import os
import glob
from ankipan import Ankipan

def youtube(ankipan, url, name):
    prefilename = name + "_temp"
    filename = name + ".mp4"
    audioname = name + ".wav"

    print('\nDownloading video from Youtube to ' + prefilename)
    youtube_cmd = 'youtube-dl -o "' + prefilename + '" "' + url +'"'
    subprocess.call(youtube_cmd, shell=True)

    '''proc = Popen(shlex.split(youtube_cmd), stdout=PIPE, stderr=PIPE)
    out, err = proc.communicate()
    errcode = proc.returncode
    print('>>Status: Ouput')
    print(out)
    print('>>Status: Error')
    print(err)
    print('>>Status: Error Code')
    print(errcode)'''

    ext = 'avi'

    for file in glob.glob(prefilename + ".*"):
        parsed = file.split('.')
        ext = parsed[len(parsed)-1]

    print('\nCreating ' + audioname)
    extract_cmd = 'ffmpeg -i ' + prefilename + '.' + ext + ' -ab 160k -ac 1 -ar 44100 -vn ' + audioname
    subprocess.call(extract_cmd, shell=True)

    print('\nCreating ' + filename)
    convert_cmd = 'ffmpeg -i ' + prefilename + '.' + ext + ' -f mp4 -vcodec libx264 -s 1920x1080 -acodec aac ' + filename
    subprocess.call(convert_cmd, shell=True)

    #stream_cmd = 'streamlink -o "' + filename + '" ' + url + ' BEST'
    #subprocess.call(youtube_cmd, shell=True)
    #subprocess.call(extract_cmd, shell=True)
    #subprocess.call(convert_cmd, shell=True)

def instagram(ankipan, url, name):
    instagram_cmd = 'instaloader --only-if="is_video" ' + name
    subprocess.call(instagram_cmd, shell=True)