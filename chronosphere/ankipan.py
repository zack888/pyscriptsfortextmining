import time
import datetime
import codecs

class Ankipan:
    def __init__(self, name):
        #datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H-%M-%S')
        #self.text_file = open(name+".json", "w")
        self.text_file = codecs.open(name+".json", "w", "utf-8")

    def wink(self, item_string):
        self.eat(item_string)
        self.newline()
        print(item_string)
        print('\n')

    def eat(self, item_string):
        self.text_file.write(item_string)

    def gulp(self, item_string):
        self.eat(item_string)
        self.newline()

    def newline(self):
        self.text_file.write("\n")

    def sleep(self):
        self.text_file.close()