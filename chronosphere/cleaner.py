import os
import glob

def clean(name):
    for file in glob.glob(name + ".*"):
        os.remove(file)