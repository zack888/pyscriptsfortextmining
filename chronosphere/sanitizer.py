from urllib.parse import urlparse

def cleanParam(host, params):
    #delim = '&'
    #delime = '='
    #return params.split(delim)[0].split(delime)[1]

    #Youtube only
    if host == 'youtube':
        t1 = params.split('&')
        for item in t1:
            t2 = item.split('=')
            if t2[0] == 'v':
                return t2[1]

    return ''

def cleanHost(host):
    delim = '.'
    count = host.count(delim)
    if count > 1:
        return host.split(delim)[1]
    else:
        return host.split(delim)[0]

def sanitize(url):
    out = urlparse(url)
    host = cleanHost(out.hostname).lower()
    query = cleanParam(host, out.query)
    sanitized = url

    #Youtube only
    if host == 'youtube':
        sanitized = 'https://www.youtube.com/watch?v=' + query

    print('\nSanitization ret out')
    return host, query, sanitized
