import lxml
from lxml import etree
import urllib.request

def getyoutube(url):
    print('Retrieve title LXML')
    youtube = etree.HTML(urllib.request.urlopen(url).read())
    video_title = youtube.xpath("//span[@id='eow-title']/@title")
    print(''.join(video_title))
    return ''.join(video_title)