# -*- coding: utf-8 -*-

import pandas as pd
import json
from collections import Counter
import re


def job(ngramsList,timestamp):
    stopwords = [line.strip('\n') for line in open('english_stop_wordsv2.txt', "r")]
    # open the txt file
    ngrams = [line.strip('\n') for line in open(ngramsList, "r")]
    firstColandadjverbs = {}
    firstColandadjverbs['#']=[]
    firstCol = []
    hashTagCount = 0
    linkDict = {'#':Counter({})}
    for i in range(1, len(ngrams)):
        a, b = ngrams[i].split('\t')
        aList = [a]
        acnt = Counter(aList)
        bList = [x for x in b.split('|') if len(x)!=0 and x not in a.split(' ')]
        bcnt = Counter(bList)
        # if a not in linkDict:
        #     linkDict[a]=bcnt
        # # needs to be elif here to ensure it does not run the first time for each a
        # elif a in linkDict:
        #     linkDict[a]+=bcnt
        if a[0] == '#':
            linkDict['#']+=acnt
            # append all the first column to know all the hashtags
            firstColandadjverbs['#'].append(a)
            hashTagCount+=1
        # get rid of all those with a[0] in stopwords
        elif a.split(' ')[0] not in stopwords:
            if a not in linkDict:
                linkDict[a] = bcnt
            else:
                linkDict[a] += bcnt
            firstCol.append(a)

    cnt = Counter(firstCol)
    top_keys = ['#']
    size =[hashTagCount]
    # previously did this:
    # assocTerms = firstColandadjverbs["#"]
    # turns out assocTerms became a pointer to that
    assocTerms =[]
    for each in [(k, v) for k, v in cnt.most_common(10)]:
        top_keys.append(each[0])
        size.append(each[1])
        eachAssocTermList = [x for x,y in linkDict[each[0]].most_common(50)]
        firstColandadjverbs[each[0]] = eachAssocTermList
        assocTerms.extend(eachAssocTermList)

    nameList = top_keys +firstColandadjverbs["#"]+ assocTerms

    while len(size)!= len(nameList):
        size.append(1)

    sourceList = []
    sourceLink = 0
    for eachKey in top_keys:
        for eachTerm in firstColandadjverbs[eachKey]:
            sourceList.append(sourceLink)
        sourceLink+=1

    targetList = []
    assocIndex = len(top_keys)
    combinedList = firstColandadjverbs["#"]+ assocTerms
    for eachAssoc in combinedList:
        targetList.append(assocIndex)
        assocIndex+=1

    data = {
            "nodes":
            [
                {"name":"","group":1,"size":1,'fontsize':1} for x in range(len(nameList))
            ],

            "links":
            [
                {"source":0,"target":0,"value":1} for x in range(len(sourceList))
            ]
            }

    count = 0
    for each in data["nodes"]:
        each["name"] = nameList[count]
        each["size"] = size[count]
        each["fontsize"] = size[count]+10
        each["group"] = count
        count+=1

    count = 0
    for each in data["links"]:
        each["source"] = sourceList[count]
        each["target"] = targetList[count]
        if nameList[sourceList[count]][0]=='#':
            each["value"] = linkDict['#'][nameList[targetList[count]]]
        else:
            each["value"] = linkDict[nameList[sourceList[count]]][nameList[targetList[count]]]
        count+=1

    with open('cl'+timestamp+'.json', 'w') as outfile:
        json.dump(data, outfile)

    return 'cl'+timestamp+'.json'

if __name__=="__main__":
    job('11111grams.txt','11111')