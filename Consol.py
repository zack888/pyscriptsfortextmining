# coding: utf-8

from openpyxl import load_workbook
from xlutils.copy import copy as xl_copy
import xlwt
import xlrd

def read_column_in_excel(workbookName,sheetName,columnNumber):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=0
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList
def read_column_in_excel_limited(workbookName,sheetName,columnNumber,rowMax):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = rowMax
    row_read=0
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

wordList=read_column_in_excel_limited('-L1IhCjteSit-9gwqIxK.xls','frequencyList',0,10)
countList=read_column_in_excel_limited('-L1IhCjteSit-9gwqIxK.xls','frequencyList',1,10)
sentiList=read_column_in_excel('-L1IhCjteSit-9gwqIxK.xls','sentiScores',0)
verbatimList=read_column_in_excel('-L1IhCjteSit-9gwqIxK.xls','yourData',0)

def genlistOfTenLists(wordList,verbatimList,sentiList):
    listOfTenLists=[]
    for eachWord in wordList:
        countVerbatim=0
        eachWordList=[]
        for eachVerbatim in verbatimList:
            if eachWord in eachVerbatim:
                eachWordList.append(sentiList[countVerbatim])
            else:
                eachWordList.append('')
            countVerbatim+=1
        listOfTenLists.append(eachWordList)

    return listOfTenLists

listofTenLists=genlistOfTenLists(wordList,verbatimList,sentiList)

def findPercentageList(countList,verbatimList):
    percentageList=[]
    for eachcount in countList:
        eachPercentage=round((float(eachcount)/len(verbatimList))*100,2)
        percentageList.append(eachPercentage)

    return percentageList

def acculList(percentageList):
    outputList = [''] * len(percentageList)
    finalElement = sum(percentageList)
    outputList[-1] = str(finalElement) + '%'

    return outputList

def listOfNegPercentage(listofTenLists):
    listOfNeg = []
    for eachList in listofTenLists:
        countNum=0.0
        countNeg=0.0
        for eachval in eachList:
            if eachval!='':
                countNum+=1
                eachvalconvert=float(eachval)
                if eachvalconvert<0.0:
                    countNeg+=1
        negPercent=round((countNeg/countNum)*100,0)
        listOfNeg.append(negPercent)

    return listOfNeg

def top3senti(verbatimList,listofTenLists):
    listOfTop3SentiLists=[]
    listofUsedPositions=[]
    for eachList in listofTenLists:
        # making sure all numerical chars are float in eachList
        newEachList=[]
        for eachVal in eachList:
            if eachVal!='':
                newEachList.append(float(eachVal))
            else:
                newEachList.append('')
        # create a list variable to limit 3 appends to used positions
        limit3=[]
        # list to store top 3 verbatim for each list
        top3SentiList=[]
        # m is max value for eachList
        # seems lik max always picks out the '' blanks
        m=max([i for i in newEachList if i!=''])
        # this is a list to store the positions where the maxvalue is found
        maxPositions=[i for i, j in enumerate(newEachList) if j == m]
        print 'maxPos '+ str(maxPositions)
        # there is a chance that there are 3 or more 0.95 values
        if len(maxPositions)>2:
            for eachPos in maxPositions:
                if eachPos not in listofUsedPositions:
                    if len(limit3) <3:
                        top3SentiList.append(verbatimList[eachPos])
                        #filter to top 3 verbatim
                        top3SentiList=top3SentiList[:3]
                        listofUsedPositions.append(eachPos)
                        limit3.append('count')
            #second chance to add more into top3sentilist
            if len(limit3) < 3:
                listOfmoreValues = [i for i in newEachList if 0.1<i<0.95]
                maxPositionsExtend = [i for i, j in enumerate(newEachList) if j in listOfmoreValues]
                print 'extended maxPos '+str(maxPositionsExtend)
                for eachPos in maxPositionsExtend:
                    if eachPos not in listofUsedPositions:
                        top3SentiList.append(verbatimList[eachPos])
                        # filter to top 3 verbatim
                        top3SentiList = top3SentiList[:3]
                        listofUsedPositions.append(eachPos)
                        limit3.append('count')
                    if len(limit3) == 3:
                        break
            #now fill up the blanks with 'check others for overlap'
            while len(limit3)<3:
                top3SentiList.append('check other terms for overlap')
                listofUsedPositions.append('**')
                limit3.append('count')


        #what if there are less than 3 0.95 values
        else:
            # finding senti values betwen 0.7 and 1
            listOfmoreValues=[i for i in newEachList if 0.1 < i < 1]
            # finding positions that have senti in that range
            maxPositionsExtend = [i for i, j in enumerate(newEachList) if j in listOfmoreValues]
            print 'extended maxPos ' + str(maxPositionsExtend)
            for eachPos in maxPositionsExtend:
                if eachPos not in listofUsedPositions:
                    if len(limit3) < 3:
                        top3SentiList.append(verbatimList[eachPos])
                        #filter to top 3 verbatim
                        top3SentiList=top3SentiList[:3]
                        listofUsedPositions.append(eachPos)
                        limit3.append('count')
            while len(limit3)<3:
                top3SentiList.append('check other terms for overlap')
                listofUsedPositions.append('**')
                limit3.append('count')
        print 'usedPos '+str(listofUsedPositions)
        listOfTop3SentiLists.append(top3SentiList)
        print 'len of listoftop3sentilist '+str(len(listOfTop3SentiLists))
    return listOfTop3SentiLists

#top3senti(verbatimList,listofTenLists)

def btm3senti(verbatimList,listofTenLists):
    listOfTop3SentiLists=[]
    listofUsedPositions=[]
    for eachList in listofTenLists:
        # making sure all numerical chars are float in eachList
        newEachList=[]
        for eachVal in eachList:
            if eachVal!='':
                newEachList.append(float(eachVal))
            else:
                newEachList.append('')
        # create a list variable to limit 3 appends to used positions
        limit3=[]
        # list to store top 3 verbatim for each list
        top3SentiList=[]
        # m is max value for eachList
        # seems lik max always picks out the '' blanks
        m=min([i for i in newEachList if i!=''])
        # this is a list to store the positions where the maxvalue is found
        maxPositions=[i for i, j in enumerate(newEachList) if j == m]
        print 'minPos '+ str(maxPositions)
        # there is a chance that there are 3 or more 0.95 values
        if len(maxPositions)>2:
            for eachPos in maxPositions:
                if eachPos not in listofUsedPositions:
                    if len(limit3) <3:
                        top3SentiList.append(verbatimList[eachPos])
                        #filter to top 3 verbatim
                        top3SentiList=top3SentiList[:3]
                        listofUsedPositions.append(eachPos)
                        limit3.append('count')
            #second chance to add more into top3sentilist
            if len(limit3) < 3:
                listOfmoreValues = [i for i in newEachList if -0.95<i<0]
                maxPositionsExtend = [i for i, j in enumerate(newEachList) if j in listOfmoreValues]
                print 'extended minPos '+str(maxPositionsExtend)
                for eachPos in maxPositionsExtend:
                    if eachPos not in listofUsedPositions:
                        top3SentiList.append(verbatimList[eachPos])
                        # filter to top 3 verbatim
                        top3SentiList = top3SentiList[:3]
                        listofUsedPositions.append(eachPos)
                        limit3.append('count')
                    if len(limit3) == 3:
                        break
            #now fill up the blanks with 'check others for overlap'
            while len(limit3)<3:
                top3SentiList.append('check other terms for overlap/ no positives')
                listofUsedPositions.append('**')
                limit3.append('count')


        #what if there are less than 3 0.95 values
        else:
            # finding senti values betwen 0.7 and 1
            listOfmoreValues=[i for i in newEachList if -1.1 < i < 0]
            # finding positions that have senti in that range
            maxPositionsExtend = [i for i, j in enumerate(newEachList) if j in listOfmoreValues]
            print 'extended minPos ' + str(maxPositionsExtend)
            for eachPos in maxPositionsExtend:
                if eachPos not in listofUsedPositions:
                    if len(limit3) < 3:
                        top3SentiList.append(verbatimList[eachPos])
                        #filter to top 3 verbatim
                        top3SentiList=top3SentiList[:3]
                        listofUsedPositions.append(eachPos)
                        limit3.append('count')
            while len(limit3)<3:
                top3SentiList.append('check other terms for overlap/ no negatives')
                listofUsedPositions.append('**')
                limit3.append('count')
        print 'usedPos '+str(listofUsedPositions)
        listOfTop3SentiLists.append(top3SentiList)
        print 'len of listoftop3sentilist '+str(len(listOfTop3SentiLists))
    return listOfTop3SentiLists

btm3senti(verbatimList,listofTenLists)