#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import re
from collections import Counter
import nltk
from nltk import ngrams
from nltk.stem import WordNetLemmatizer
# from nltk import word_tokenize, pos_tag
from datetime import datetime

# beginning of script
startTime = datetime.now()

# import sys

# reload(sys)
# sys.setdefaultencoding("utf8")

#### import with xls or xlsx - result file from WGT
# xl=pd.ExcelFile('./DataSets/Antioxidant_Data.xls')
# check sheet_names
# print(xl.sheet_names)
# df = xl.parse('yourData')

def job(commentsList,timestamp):
    # df = pd.read_csv(commentsList, sep="\t", header=None, encoding='utf8')
    df = [ line.strip('\n') for line in open(commentsList,"r") ]

    # try not filtering any stop words out
    stopwords = [ line.strip('\n') for line in open('english_stop_wordsv2.txt',"r") ]
    # sentence="Really loving this multi-purpose thermal spring water spray.  first of all I like the size of the bottle...it will definitely last me longer than most aqua sprays, appreciate the simple design and the easy squeeze pump spray. It didn't comes out in a gentle mist."
    # sentence="it seems just water, so it didn't do any thing, and the nozzle broken after a few days."
    def segSentence(paragraph):
        for sent in re.findall(u'[^!?。！，\.\!\?]+[!?。！，\.\!\?]?', paragraph, flags=re.U):
            yield sent


    def remove_characters_before_tokenization(sentence, keep_apostrophes=False):
        sentence = sentence.strip()
        if keep_apostrophes:
            PATTERN = r'[?|$|&|*|%|@|(|)|~|.|!|,|"|\'|-|+]'  # add other characters here to remove them
            filtered_sentence = re.sub(PATTERN, r'', sentence)
        else:
            PATTERN = r'[^a-zA-Z0-9 ]'  # only extract alpha-numeric characters
            filtered_sentence = re.sub(PATTERN, r'', sentence)
        return filtered_sentence


    def countAdaptedPos(grams, posTag):
        gramsPos = []
        for x, y in grams:
            gramsPos.append(y)

        gramsPos = Counter(gramsPos)
        # Counter({u'DET': 1, u'ADP': 1})

        count = 0
        for x, y in gramsPos.items():
            if x in posTag:
                count = count + y
        return count

    n2grams_Pair = pd.DataFrame(dtype='object')
    ngramsVerAdj_per_ngram = pd.DataFrame(dtype='object')

    # posTag = ['NOUN', 'ADP', 'ADV', 'CC', 'DT', 'EX', 'FW']
    adJVerb = ['ADJ','VERB']

    GAP_PATTERN = r'\s+'
    regex_wt = nltk.RegexpTokenizer(pattern=GAP_PATTERN, gaps=True)
    wnl = WordNetLemmatizer()

    countsen=0
    for i in range(0, len(df)):
        xx = list(segSentence(df[i]))
        # xx is [u'Hmmmm.', u' He didnt get ousted BC he got a BJ.', u' He got ousted BC he lied under oath about it.', u' That is what you menat I am sure']
        for sen in xx:
            countsen += 1
            sen = remove_characters_before_tokenization(sen, keep_apostrophes=True)
            words = regex_wt.tokenize(sen.lower())
            # filtered_words = [v for v in words if v not in stopwords]
            filtered_words = [v for v in words]
            # filtered_words = [wnl.lemmatize(token, 'v') for token in filtered_words]
            wordsPos = nltk.pos_tag(filtered_words, tagset='universal')
            # wordsPos is [(u'he', u'PRON'), (u'didnt', u'VERB'), (u'get', u'NOUN'), (u'ousted', u'VERB'), (u'bc', u'ADP'), (u'he', u'PRON'), (u'got', u'VERB'), (u'a', u'DET'), (u'bj', u'NOUN')]
            # identifying all verbs and adj in the sentence
            verbadjpersenString = u""
            for n,m in wordsPos:
                if m in adJVerb and n not in stopwords:
                    verbadjpersenString=verbadjpersenString+n+u'|'
            # cheered|culminated|blue|

            ### 2grams
            twograms = list(ngrams(wordsPos, 2))
            # twograms is [((u'they', u'PRON'), (u'make', u'VERB')), ((u'make', u'VERB'), (u'me', u'PRON')), ((u'me', u'PRON'), (u'so', u'ADP')), ((u'so', u'ADP'), (u'madddd', u'ADJ'))]
            for grams in twograms:
                #  grams is ((u'perjury', u'NOUN'), (u'trap', u'NOUN'))
                # count = countAdaptedPos(grams, posTag)
                # finding pairs with pos in posTag list
                # if count >= 2:
                # n2grams_FirstTerm = n2grams_FirstTerm.append([grams[0][0]])
                # n2grams_SecondTerm = n2grams_SecondTerm.append([grams[1][0]])
                n2grams_Pair = n2grams_Pair.append([grams[0][0]+u' '+grams[1][0]])
                ngramsVerAdj_per_ngram = ngramsVerAdj_per_ngram.append([verbadjpersenString])

    results = pd.concat([n2grams_Pair,ngramsVerAdj_per_ngram], axis=1)
    results.columns = ['Pair','AdjNVerb']
    results = results.sort_values(by=['Pair'], axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
    print (results)

    results.to_csv(timestamp+'grams.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")

    #### write results to excel
    # writer = pd.ExcelWriter('Trump.xlsx', engine='xlsxwriter')
    # n2grams_results.to_excel(writer, '2grams', header=True, index=False)
    # writer.save()

    # end of script
    print (datetime.now() - startTime)

    return timestamp+'grams.txt'

if __name__=='__main__':
    job('11111text.txt','11111')