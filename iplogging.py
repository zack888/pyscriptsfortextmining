# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import time
from selenium import webdriver
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
import threading
from threading import Thread
from threading import Timer
from selenium.webdriver import ActionChains
import time


path_to_chromedriver = 'C:\\Users\\admin\\Desktop\\chromedriver.exe'
browser = webdriver.Chrome(executable_path = path_to_chromedriver)

PROXY = "23.23.23.23:3128" # IP:PORT or HOST:PORT

# chrome_options = webdriver.ChromeOptions()
# chrome_options.add_argument('--proxy-server=%s' % PROXY)
timeout_time=30
browser.set_page_load_timeout(timeout_time)

def job(url):
    def t1():
        browser.get(url)
        time.sleep(5)
    t1 = threading.Thread(target=t1)
    t1.setDaemon(True)
    t1.start()
    def t2():
        try:
            html = browser.page_source
        except:
            time.sleep(5)
            html=browser.page_source
        soup = BeautifulSoup(unicode(html), "html.parser")
        ipAddr = browser.find_element_by_xpath('//*[@id="section_left"]/div[2]/a').text
        print ipAddr
        ts = time.time()
        print ts
        text_file = open("IPLOGGING.txt", "a")
        text_file.write("IP Addr: %s    TIME: %s" % (ipAddr, ts))
        text_file.write("\n")
        text_file.close()
    t2 = Timer(timeout_time, t2)
    t2.start()
    t2.join()
    #return browser.page_source

while True:
    job('https://whatismyipaddress.com/')
    time.sleep(1800)