import sys
from time import sleep
from apscheduler.scheduler import Scheduler
import logging
logging.basicConfig()
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import textMining
import time

# Fetch the service account key JSON file contents
cred = credentials.Certificate('C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\serviceAccountKey.json')

# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://text-mining-458e9.firebaseio.com/'
})

# As an admin, the app has access to read and write all data, regardless of Security Rules

sched = Scheduler()
sched.start()  # start the scheduler

collectionOfEach=[]

def my_job():
    ref = db.reference('messages')
    entireJson=ref.get()
    print entireJson
    print time.asctime(time.localtime(time.time()))
    if entireJson is not None:
        for each in entireJson:
            if each not in collectionOfEach:
                print each
                collectionOfEach.append(each)
                tmall_urls = []
                eachJson = entireJson[str(each)]
                tmall_urls.extend(eachJson['message'].split(','))
                print tmall_urls
                if eachJson['checked'] == 'true':
                    textMining.get_pictures(tmall_urls,each,eachJson['email'])
                textMining.job(tmall_urls,each,eachJson['email'])
    else:
        print "emtpy JSON"


def main():
    my_job()
    sched.add_interval_job(my_job, seconds=10)
    while True:
        sleep(1)
        # sys.stdout.write('.');
        sys.stdout.flush()

##############################################################

if __name__ == "__main__":
    main()