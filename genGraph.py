import ngramchv5
import dataPrepPlotly2ch
from difflib import SequenceMatcher
import pandas as pd
import re
from collections import Counter
import xlrd
from xlutils.copy import copy as xl_copy
from datetime import datetime
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy
import jieba
import io
import shutil
import random

def similar(a, b, score):
    if SequenceMatcher(None, a, b).ratio() > score:
        return True
    else:
        return False

def read_column_in_excel_with_startRow(workbookName,sheetName,columnNumber,startRow):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=startRow
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def filterSpam(column):
    filteredList = []
    for j in range(len(column) - 1):
        for i in range(len(column)):
            if j != i and similar(column[j], column[i], 0.95):
                break
        else:
            filteredList.append(column[j])

    return filteredList

tabsList = ['freyapersf','freyaperds','freyallsf','freyallds','adolfper','adolfll','freyacondsf','freyacondds','freyafoam','adolffoam','abbfoam']
# tabsList = ['adolffoam','abbfoam']

for each in range(2):
    sheetName = tabsList[each]

    column = read_column_in_excel_with_startRow('fen.xls',sheetName,0,0)

    if len(column)>1000:
        column = random.sample(column, 1000)

    column = filterSpam(column)

    thefile = io.open(sheetName+'.txt', 'w',encoding='utf-8')

    for item in column:
        thefile.write("%s\n" % item)

    ngramchv5.job(sheetName+'.txt','11111')
    dataPrepPlotly2ch.job('11111grams.txt',sheetName)