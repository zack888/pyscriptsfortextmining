import pyautogui, time

def job():

    pyautogui.click()    # click to put drawing program in focus

    time.sleep(1)

    pyautogui.moveTo(995, 500, duration=1)

    time.sleep(1)

    pyautogui.dragRel(445, 5, 0.897, pyautogui.easeInQuad)  # move right

def job1():

    pyautogui.click()  # click to put drawing program in focus

    time.sleep(1)

    pyautogui.moveTo(995, 500, duration=1)

    time.sleep(1)

    pyautogui.dragRel(450, 5, 0.75, pyautogui.easeInOutQuad)  # move right

# job()
# job1()

def usernameClick():

    pyautogui.click()    # click to put drawing program in focus

    time.sleep(1)

    pyautogui.moveTo(1197, 357, duration=1)

    time.sleep(1)

    pyautogui.click()

def passwordClick():

    pyautogui.click()    # click to put drawing program in focus

    time.sleep(1)

    pyautogui.moveTo(1202, 424, duration=1)

    time.sleep(1)

    pyautogui.click()

def reviewButtonClick():

    pyautogui.click()    # click to put drawing program in focus

    time.sleep(1)

    # click review tab
    pyautogui.moveTo(500, 900, duration=1)
    pyautogui.click()

    time.sleep(3)

    # scroll down
    pyautogui.scroll(-5)

    time.sleep(3)

    # show pictures
    pyautogui.moveTo(500, 387, duration=1)
    pyautogui.click()