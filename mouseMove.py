import pyautogui
import time

pyautogui.PAUSE = 1
pyautogui.FAILSAFE = True

print 'size of screen: (x,y)'
print pyautogui.size()

width, height = pyautogui.size()

while True:
    pyautogui.moveRel(100, 0, duration=0.25)
    pyautogui.moveRel(0, 100, duration=0.25)
    pyautogui.moveRel(-100, 0, duration=0.25)
    pyautogui.moveRel(0, -100, duration=0.25)
    print 'cycle'
    time.sleep(10)