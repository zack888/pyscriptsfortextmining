#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 15:30:50 2018

@author: tzehau
"""
import pynlpir
from nltk import ngrams
#import numpy as np
import pandas as pd
import re
import sys
import xlrd

reload(sys)
sys.setdefaultencoding('utf8')

def read_column_in_excel(workbookName,sheetName,columnNumber):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=0
    while row_read < row_count:
        # this is to prevent excel from auto adding decimals to numbers
        try:
            each=str(int(sheet.cell(row_read,columnNumber).value))
        except:
            each = str(sheet.cell(row_read, columnNumber).value)
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList


pynlpir.open(encoding="utf_8")
df = pd.read_csv('./review_sample.txt', sep="\t", header=None)
dfFirstTerm = pd.DataFrame(dtype='object')
dfSecondTerm = pd.DataFrame(dtype='object')
dfnounAdjPair = pd.DataFrame(dtype='object')
# stopwords = [unicode(line.rstrip()) for line in open('chinese_stop_wordsv2.txt',"r")]
stopwords=read_column_in_excel('stopwords.xlsx','chinese',0)
print stopwords
print type(stopwords)

def segSentence(paragraph):
    for sent in re.findall(u'[^!?。！，\.\!\?]+[!?。！，\.\!\?]?', paragraph, flags=re.U):
        yield sent

for i in range(0, len(df)):
    xx=list(segSentence(df.iloc[i,0]))
    for sen in xx:
        pos = pynlpir.segment(sen)
        reStopWords_pos = [v for v in pos if v[0] not in stopwords]
        twograms = list(ngrams(reStopWords_pos, 2))
        for grams in twograms:
            if any(any(substr in s for s in grams) for substr in ['noun', 'adjective']) :
                dfFirstTerm = dfFirstTerm.append([grams[0][0]])
                dfSecondTerm = dfSecondTerm.append([grams[1][0]])
                dfnounAdjPair =  dfnounAdjPair.append([grams[0][0] + '|' + grams[1][0]])

results=pd.concat([dfFirstTerm, dfSecondTerm, dfnounAdjPair], axis=1)
results.columns=['First_Term', 'Second_Term', 'nounAdjPair']
            
wordFreq = results.groupby('nounAdjPair').count()

dfFirstTerm = pd.DataFrame(dtype='object')
dfSecondTerm = pd.DataFrame(dtype='object')
dfCount = pd.DataFrame()

for i in range(0, len(wordFreq)):
    xx=wordFreq.index[i].split('|')
    dfFirstTerm = dfFirstTerm.append([xx[0]])
    dfSecondTerm = dfSecondTerm.append([xx[1]])
    dfCount = dfCount.append([wordFreq.iloc[i,1]])
    
dfnounAdjPairCount = pd.concat([dfFirstTerm, dfSecondTerm, dfCount], axis=1)
dfnounAdjPairCount.columns=['First_Term', 'Second_Term', 'nounAdjPair_Count']

dfnounAdjPairCount.to_csv('nounAdjPairCountV2.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")