# coding: utf-8

from openpyxl import load_workbook
from xlutils.copy import copy as xl_copy
import xlwt
import xlrd

def read_column_in_excel_with_startRow(workbookName,sheetName,columnNumber,startRow):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=startRow
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def read_column_in_excel_limited(workbookName,sheetName,columnNumber,rowMax):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = rowMax
    row_read=0
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def save_column_in_excel(fileName,sheetNumber,columnNumber,columnList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=25
    for each in columnList:
        Sheet1.write(link_comments, columnNumber, each)
        link_comments += 1
    wb.save(fileName)

def save_row_in_excel(fileName,sheetNumber,rowNumber,rowList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=3
    for each in rowList:
        Sheet1.write(rowNumber, link_comments, each)
        link_comments += 1
    wb.save(fileName)

def save_row_in_xlsx(loadExcel,inputList,rowNum,columnNum):
    #Open an xlsx for reading
    wb = load_workbook(filename = loadExcel)
    #Get the current Active Sheet
    #ws = wb.get_active_sheet()
    #You can also select a particular sheet
    #based on sheet name
    ws = wb.get_sheet_by_name("steve")
    #save the csb file
    for col, val in enumerate(inputList, start=columnNum):
        ws.cell(row=rowNum, column=col).value = val
    wb.save(loadExcel)

def save_col_in_xlsx(loadExcel,inputList,rowNum,columnNum):
    #Open an xlsx for reading
    wb = load_workbook(filename = loadExcel)
    #Get the current Active Sheet
    #ws = wb.get_active_sheet()
    #You can also select a particular sheet
    #based on sheet name
    ws = wb.get_sheet_by_name("steve")
    #save the csb file
    for row, val in enumerate(inputList, start=rowNum):
        ws.cell(row=row, column=columnNum).value = val
    wb.save(loadExcel)

def moveFreqList(excelName,sheetName):
    wordList=read_column_in_excel_limited(excelName,sheetName,0,10)
    countList=read_column_in_excel_limited(excelName,sheetName,1,10)

    save_row_in_xlsx('textAnalysistestsaved.xlsx', wordList, 1, 4)
    save_row_in_xlsx('textAnalysistestsaved.xlsx', countList, 2, 4)

def checkCellValue(workbookName,sheetName,rowNum,colNum):
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    cellval = sheet.cell(rowNum,colNum).value

    if cellval!=int or cellval!=float or cellval!=long:
        cellval=0

    print type(cellval)
    print int(cellval)
    print type(cellval)
    return cellval

def clear_cols_in_xlsx(loadExcel,startRowNum,rowMax):
    #Open an xlsx for reading
    wb = load_workbook(filename = loadExcel)
    #wb_read = load_workbook(filename = loadExcel,data_only=True)
    #Get the current Active Sheet
    #ws = wb.get_active_sheet()
    #You can also select a particular sheet
    #based on sheet name
    ws = wb.get_sheet_by_name("steve")
    #ws_read = wb_read.get_sheet_by_name("steve")
    #save the csb file
    # row_count = ws.max_row
    #max_row=ws_read.cell(row=2, column=3).value
    max_row = rowMax+30
    print max_row
    #max_row = int(0 if max_row is None else max_row)+30
    count = startRowNum
    while count< max_row:
        ws.cell(row=count, column=1).value = ''
        count+=1
    count = startRowNum
    while count< max_row:
        ws.cell(row=count, column=2).value = ''
        count+=1
    print 'replace complete'
    wb.save(loadExcel)

def moveSentiScores(excelName,sheetName):
    scoreList = read_column_in_excel_with_startRow(excelName, sheetName, 0,0)

    save_col_in_xlsx('textAnalysistestsaved.xlsx', scoreList, 26, 2)

def moveVerbatim(excelName,sheetName):
    verbatimList = read_column_in_excel_with_startRow(excelName, sheetName, 0,0)

    save_col_in_xlsx('textAnalysistestsaved.xlsx', verbatimList, 26, 1)

def refresh_excel(path):
    from win32com.client import Dispatch
    xl = Dispatch('Excel.Application')
    wb = xl.Workbooks.Open(path)
    wb.RefreshAll()
    wb.Close(True)

def job(excelName):
    moveFreqList(excelName,'frequencyList')
    print 'freqlist moved'
    refresh_excel('C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\textMiningForMoney\\textAnalysistestsaved.xlsx')
    clear_cols_in_xlsx('textAnalysistestsaved.xlsx',26,10000)
    print 'cols cleared'
    refresh_excel('C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\textMiningForMoney\\textAnalysistestsaved.xlsx')
    moveSentiScores(excelName,'sentiScores')
    print 'senti moved'
    refresh_excel('C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\textMiningForMoney\\textAnalysistestsaved.xlsx')
    moveVerbatim(excelName,'yourData')
    print 'verbatim moved'
    refresh_excel('C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\textMiningForMoney\\textAnalysistestsaved.xlsx')

