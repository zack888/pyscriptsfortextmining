#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 15:30:50 2018

@author: tzehau

edited by zhi long
"""
import jieba.posseg as pseg
import jieba.analyse
from nltk import ngrams
# import numpy as np
import pandas as pd
import re
from collections import Counter
from difflib import SequenceMatcher
import updateAnalysis
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy
import jieba
from datetime import datetime
startTime = datetime.now()

def similar(a, b, score):
    if SequenceMatcher(None, a, b).ratio() > score:
        return True
    else:
        return False

def jiebaTokenizer(text):
    text=re.sub("\W+", "", text) # remove punctuation
    return list(jieba.cut(text, cut_all=False))

def spamCheck(inputList,score):
    tfVectorizer = CountVectorizer(tokenizer=jiebaTokenizer)
    tfVector=tfVectorizer.fit_transform(inputList)
    similarity_matrix = cosine_similarity(tfVector)

    zz=[]
    for i in range(0, len(inputList)):
        xx=similarity_matrix[i,:]
        xx[i]=0
        #if numpy.any(xx>0.95):
        if numpy.all(xx<score):
            zz.append(inputList[i])
    return zz

def repetitions(s):
   r = re.compile(r"(.+?)\1+")
   for match in r.finditer(s):
       yield (match.group(1), len(match.group(0))/len(match.group(1)))

def checkRepetitions(l):
    outputList = []
    for eachString in l:
        eachString1 = eachString.replace(u" ", u"")
        eachString2 = eachString1.replace(u"|",u"")
        if list(repetitions(eachString2))==[]:
            outputList.append(eachString)
            continue
        if list(repetitions(eachString2))[0][1]<3:
            outputList.append(eachString)
    return outputList

def checkRepeatedSingleChar(l):
    outputList = []
    for eachString in l:
        eachString1 = eachString.replace("|","")
        eachCountString = Counter(eachString1).most_common(1)
        if eachCountString[0][1]<10:
            outputList.append(eachString)

    return outputList

def job(bookName,sheetName,ngramsNum,timestamp):
    #### import with xls or xlsx - result file from WGT
    xl = pd.ExcelFile(bookName)
    # check sheet_names
    # print(xl.sheet_names)
    df = xl.parse(sheetName)

    importedList = []
    for i in range(len(df)):
        if df.iloc[i, 0]:
            importedList.append(df.iloc[i, 0])

    importedList = checkRepetitions(importedList)

    ## load stopwords file
    stopwords = [line.strip('\n') for line in open('chinese_stop_wordsV3.txt', "r")]

    n7grams_com = pd.DataFrame(dtype='object')

    # jieba.enable_parallel(4)

    def segSentence(paragraph):
        for sent in re.findall(u'[^!?。！，\.\!\?]+[!?。！，\.\!\?]?', paragraph, flags=re.U):
            yield sent

    def countAdaptedPos(grams, posTag):
        gramsPos = []
        for x, y in grams:
            gramsPos.append(y)

        gramsPos = Counter(gramsPos)
        count = 0
        for x, y in gramsPos.items():
            if x in posTag:
                count = count + y
        return count

    posTag = ['Ag', 'a', 'an', 'ad', 'Ng', 'n', 'nr', 'ns', 'nt', 'nz', 's', 't', 'z', 'Vg', 'v', 'vd', 'vn']

    for i in range(0, len(importedList)):
        xx = list(segSentence(importedList[i]))
        for sen in xx:
            pos = list(pseg.cut(sen))
            pos = [tuple(v) for v in pos]
            # reStopWords_pos = [v for v in pos if v[0] not in stopwords]
            reStopWords_pos = [v for v in pos]

            sevengrams = list(ngrams(reStopWords_pos, ngramsNum))
            for grams in sevengrams:
                count = countAdaptedPos(grams, posTag)
                if count >= 2 and grams[0][0] not in stopwords and grams[0][0]!= ' ':
                    n7grams_com = n7grams_com.append(
                        [grams[0][0] + '|' + grams[1][0] + '|' + grams[2][0] + '|' + grams[3][0]+ '|' + grams[4][0]+ '|' + grams[5][0]+ '|' + grams[6][0]+ '|' + grams[7][0] + '|' + grams[8][0] + '|' + grams[9][0]]
                    )
    dfList=[]
    for i in range(len(n7grams_com)):
        dfList.append((n7grams_com.iloc[i, 0]))

    # remove phrases made up of repeated terms
    filteredList = []
    for eachString in dfList:
        eachList = eachString.split('|')
        if not all(eachList[0] == item for item in eachList):
            filteredList.append(eachString)

    phraseList =[]
    processingList = filteredList
    processingList = checkRepeatedSingleChar(processingList)
    processingList = checkRepetitions(processingList)

    for i in range(10):
        cnt = Counter(processingList)
        each = cnt.most_common(1) # each is a list
        print each
        phrase = each[0][0]
        count = 0
        phraseCount = []
        indexes_to_remove =[]

        for j in range(len(processingList)):
            if similar(phrase, processingList[j], 0.6):
                count+=1
                indexes_to_remove.append(j)
        phraseCount.append(phrase)
        phraseCount.append(count)
        phraseList.append(phraseCount)

        newList=[]
        for k in range(len(processingList)):
            if k not in indexes_to_remove:
                newList.append(processingList[k])

        processingList = newList

    dataPhrase = []
    dataCount = []

    for each in phraseList:
        dataPhrase.append(each[0].replace(u"|",""))
        phrase = each[0].replace(u"|","")
        count = 0
        for i in range(len(df)):
            count+=df.iloc[i, 0].count(phrase)
        dataCount.append(count)

    for each in dataPhrase:
        print each

    for each in dataCount:
        print each

    data = {
            'timestamp': timestamp,
            'type': 9,
            'data': [
                {
                    "count": 0,
                    "phrase": ""
                }for l in range(10)
            ]
        }

    for m in range(10):
        data['data'][m]["count"] = dataCount[m]
        data['data'][m]["phrase"] = dataPhrase[m]

    print data

    updateAnalysis.putItem(timestamp,data)
    print datetime.now() - startTime

if __name__=="__main__":
    job("Data.xls",'removedSpam',10,int(1524061714487))