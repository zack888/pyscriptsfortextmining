#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 15:30:50 2018

@author: tzehau
"""
import jieba
import jieba.posseg as pseg
import jieba.analyse
from nltk import ngrams
import numpy
import pandas as pd
import re
from collections import Counter
from collections import defaultdict
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
#from googletrans import Translator


#### import with xls or xlsx - result file from WGT
xl=pd.ExcelFile('Data.xls')
# check sheet_names
#print(xl.sheet_names)
df = xl.parse('removedSpam')
df=df[~df.iloc[:,0].duplicated(keep='first')]
#### import data from tab delimited file
#df = pd.read_csv('./DataSets/HotelNeg.txt', sep="\t", header=None)

## load stopwords file
stopwords = [ line.strip('\n') for line in open('./chinese_stop_wordsV3.txt',"r") ]

def segSentence(paragraph):
    for sent in re.findall(u'[^!?。！，\.\!\?]+[!?。！，\.\!\?]?', paragraph, flags=re.U):
        yield sent
        
def countAdaptedPos(grams, posTag):
    gramsPos=[]
    for x,y in grams:
        gramsPos.append(y)
    
    gramsPos = Counter(gramsPos)
    count = 0
    for x,y in gramsPos.items():
        if x in posTag:
            count=count + y
    return count

def jiebaTokenizer(text):
    text=re.sub("\W+", "", text) # remove punctuation
    return list(jieba.cut(text, cut_all=False))

def spamCheck(df):
    tfVectorizer = CountVectorizer(tokenizer=jiebaTokenizer)
    tfVector=tfVectorizer.fit_transform(df.iloc[:,0])
    similarity_matrix = cosine_similarity(tfVector)

    zz=[]
    for i in range(0, len(df)):
        xx=similarity_matrix[i,:]
        xx[i]=0
        #if numpy.any(xx>0.95):
        if numpy.all(xx<0.95):
            zz.append(df.iloc[i,0])
    return zz

n2grams_df = pd.DataFrame(dtype='object')
n2grams_Rating = pd.DataFrame(dtype='object')

n3grams_df = pd.DataFrame(dtype='object')
n3grams_Rating = pd.DataFrame(dtype='object')

n4grams_df = pd.DataFrame(dtype='object')
n4grams_Rating = pd.DataFrame(dtype='object')

jieba.enable_parallel(4)
spamRemoved=spamCheck(df)
posTag=['Ag', 'a', 'an', 'ad', 'Ng', 'n', 'nr', 'ns', 'nt', 'nz', 's', 't', 'z', 'Vg', 'v', 'vd', 'vn', 'BG', 'b', 'zg']
wordCount = defaultdict(int)
wordPos = defaultdict()
for i in range(0, len(spamRemoved)):
    
    xx=list(segSentence(spamRemoved[i]))    
    for sen in xx:
        pos = list(pseg.cut(sen))
        pos=[tuple(v) for v in pos]
        reStopWords_pos = [v for v in pos if v[0] not in stopwords]
        
        ### word count, rating & POS
        for word, pos in reStopWords_pos:
            wordCount[word] += 1
            wordPos[word] = pos
        
        ### 2grams
        twograms = list(ngrams(reStopWords_pos, 2))
        for grams in twograms:            
            count=countAdaptedPos(grams, posTag)             
            if count>=2:
                n2grams_df =  n2grams_df.append([grams[0][0] + '|' + grams[1][0]])
        
        ### 3grams
        threegrams = list(ngrams(reStopWords_pos, 3))
        for grams in threegrams:            
            count=countAdaptedPos(grams, posTag)
            if count>=2:               
                n3grams_df =  n3grams_df.append([grams[0][0] + '|' + grams[1][0] + '|' + grams[2][0]])
                
        ### 4grams
        fourgrams = list(ngrams(reStopWords_pos, 4))
        for grams in fourgrams:
            count=countAdaptedPos(grams, posTag)                
            if count>=2: 
                n4grams_df =  n4grams_df.append([grams[0][0] + '|' + grams[1][0] + '|' + grams[2][0] + '|' + grams[3][0]])
                
wordCount=pd.DataFrame([wordCount]).transpose()
wordPos=pd.DataFrame([wordPos]).transpose()
FOM = pd.concat([wordCount, wordPos], axis=1)
FOM.columns=['Count', 'POS']

#translator = Translator() # note include translator will be slow
n2grams_df.columns=['n2grams']
n2grams_wordFreq = n2grams_df['n2grams'].value_counts()
# translation
#n2grams_en = pd.DataFrame(dtype='object') # for translation
#for i in range(0, len(n2grams_wordFreq.index)):
#    xx=translator.translate(n2grams_wordFreq.index[i]).text
#    n2grams_en = n2grams_en.append([xx])
#n2grams_en.index=n2grams_wordFreq.index

n3grams_df.columns=['n3grams']
n3grams_wordFreq = n3grams_df['n3grams'].value_counts()
# translation
#n3grams_en = pd.DataFrame(dtype='object') # for translation
#for i in range(0, len(n3grams_wordFreq.index)):
#    n3grams_en = n3grams_en.append([xx])
#n3grams_en.index=n3grams_wordFreq.index

n4grams_df.columns=['n4grams']
n4grams_wordFreq = n4grams_df['n4grams'].value_counts()
# translation
#n4grams_en = pd.DataFrame(dtype='object') # for translation
#for i in range(0, len(n4grams_wordFreq.index)):
#    n4grams_en = n4grams_en.append([xx])
#n4grams_en.index=n4grams_wordFreq.index    


#### write results to excel
writer = pd.ExcelWriter('loreal.xlsx', engine='xlsxwriter')
FOM.to_excel(writer, 'Unigram', header=True, index=True)
n2grams_wordFreq.to_excel(writer, '2grams', header=True, index=True)
n3grams_wordFreq.to_excel(writer, '3grams', header=True, index=True)
n4grams_wordFreq.to_excel(writer, '4grams', header=True, index=True)
writer.save()


