# -*- coding: utf-8 -*-

import boto3
import json
import decimal
import boto3
from boto3.dynamodb.conditions import Key, Attr
import textMiningAWS
from openpyxl import load_workbook
import xlwt
import xlrd
import sys
import time

def putItem(username,timestamp,data,analysis):
    a_response = analysis.put_item(
        Item={
            'timestamp': timestamp,
            'type': 0,
            'username': username,
            'data': data
        }
    )

def read_column_in_excel(workbookName, sheetName, columnNumber):
    columnList = []
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read = 0
    while row_read < row_count:
        each = sheet.cell(row_read, columnNumber).value
        columnList.append(each)
        row_read += 1
    # return list of elements in column
    return columnList

def read_column_in_excel_limited(workbookName, sheetName, columnNumber, rowMax):
    columnList = []
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = rowMax
    row_read = 0
    while row_read < row_count:
        each = sheet.cell(row_read, columnNumber).value
        columnList.append(each)
        row_read += 1
    # return list of elements in column
    return columnList

def genlistOfTenLists(wordList, verbatimList, sentiList):
        listOfTenLists = []
        for eachWord in wordList:
            countVerbatim = 0
            eachWordList = []
            for eachVerbatim in verbatimList:
                if eachWord in eachVerbatim:
                    eachWordList.append(sentiList[countVerbatim])
                else:
                    eachWordList.append('')
                countVerbatim += 1
            listOfTenLists.append(eachWordList)

        return listOfTenLists

def findPercentageList(countList, verbatimList):
        percentageList = []
        for eachcount in countList:
            eachPercentage = round((float(eachcount) / len(verbatimList)) * 100, 2)
            percentageList.append(eachPercentage)

        return percentageList

def listOfNegPercentage(listofTenLists):
        listOfNeg = []
        for eachList in listofTenLists:
            countNum = 0.0
            countNeg = 0.0
            for eachval in eachList:
                if eachval != '':
                    countNum += 1
                    eachvalconvert = float(eachval)
                    if eachvalconvert < 0.0:
                        countNeg += 1
            if countNum == 0.0 or countNeg ==0.0:
                negPercent=0
            else:
                negPercent = round((countNeg / countNum) * 100, 0)
            listOfNeg.append(negPercent)

        return listOfNeg

def progressbar_update_aws(username,timestamp,progress,table):
    response = table.update_item(
        Key={
            'username' : username,
            'timestamp': timestamp
        },
        ExpressionAttributeNames={"#s": "status"},
        UpdateExpression="set #s = :r",
        ExpressionAttributeValues={
            ':r': progress
        },
        ReturnValues="UPDATED_NEW"
    )

def job(latest_timestamp,table,analysis):
    #filters out the items which are greater than latest timestamp
    response = table.scan(
        FilterExpression=Key('timestamp').gt(latest_timestamp)
        )

    if response['Items']==[]:
        print "no new items"

    else:
        for eachItem in response['Items']:

            ## Just Printin out the details##
            print eachItem['timestamp']
            useremail=eachItem['email']
            print eachItem['email']
            # split turns the url into a list type, textmining.py to handle it
            urlList=eachItem['url'].split(",")
            print urlList
            print eachItem['status']
            #some past entries have no titles
            try:
                print eachItem['title']
            except:
                print 'no title'
            #################################

            if eachItem['status']==0:
                try:
                    # getting pictures
                    if eachItem['picture'] == 't':
                        print 'getting pictures!'
                        textMiningAWS.get_pictures(urlList, str(eachItem['timestamp']), eachItem['email'],unicode(eachItem['title']))
                        imgList = read_column_in_excel(str(eachItem['timestamp']) + "pictures.xls", 'pictureLinks', 0)
                        commentList = read_column_in_excel(str(eachItem['timestamp']) + "pictures.xls", 'pictureLinks',
                                                           1)
                        imgJson = [{
                                       "id": 0,
                                       "label": "",
                                       "img": ""
                                   } for x in range(0, len(imgList), 1)]
                        count = 0
                        for each in imgJson:
                            each['id'] = count
                            each['label'] = commentList[count]
                            each['img'] = imgList[count]
                            count += 1
                        from codecs import open
                        import json
                        with open(str(eachItem['timestamp']) + '.json', 'w', encoding='utf-8') as fp:
                            json.dump(imgJson, fp, ensure_ascii=False)
                        import uploadS3
                        uploadS3.upload_pics(str(eachItem['timestamp']))
                    else:
                        print 'get pictures false'
                except:
                    e = sys.exc_info()[0]
                    print e
                checkfacebook = textMiningAWS.job(urlList, str(eachItem['timestamp']), eachItem['email'],eachItem['username'],table,unicode(eachItem['title']))
                # status has been updated to 100 in textMiningAWS

                if checkfacebook != 'onlyFacebook':
                    # read all segmented words
                    wordList = read_column_in_excel(str(eachItem['timestamp'])+".xls", 'frequencyList', 0)
                    print wordList
                    countList = read_column_in_excel(str(eachItem['timestamp'])+".xls", 'frequencyList', 1)
                    # sentiList = read_column_in_excel(str(eachItem['timestamp'])+".xls", 'sentiScores', 0)
                    verbatimList = read_column_in_excel(str(eachItem['timestamp'])+".xls", 'removedSpam', 0)
                    # listofTenLists = genlistOfTenLists(wordList, verbatimList, sentiList)

                    percentageList = findPercentageList(countList, verbatimList)

                    # negPercentage = listOfNegPercentage(listofTenLists)

                    # intialise list called data for plotting in frontend
                    data = [{
                                "name": "",
                                "secondary": 0,
                                "value": 0
                            } for x in range(0, len(wordList), 1)]

                    count=0
                    # store values for plotting in frontend
                    for each in data:
                        #chinese terms
                        if wordList[count]!='':
                            each['name'] = wordList[count]
                        else:
                            each['name'] = 'SomeWeirdCharacter'
                        #percentage of mentions
                        each['value']=int(percentageList[count])
                        #negative percentage
                        # each['secondary']=int(negPercentage[count])
                        count+=1
                        if count==len(wordList):
                            break

                    putItem(eachItem['username'],eachItem['timestamp'],data,analysis)

                    # storing vjson into S3 for  verbatim table for freq of mentions chart
                    # this is the structure of the vjson:
                    # vjson = [{
                    #             term: listOfVerbatim,
                    #         },...]


                    def genlistOfTermAndVerbatimList(wordList, verbatimList):
                        vjson = []
                        for eachWord in wordList:
                            listOfVerbatim = []
                            for eachVerbatim in verbatimList:
                                if eachWord in eachVerbatim:
                                    listOfVerbatim.append(eachVerbatim)
                            vjson.append({eachWord:listOfVerbatim})

                        return vjson

                    vjson=genlistOfTermAndVerbatimList(wordList, verbatimList)

                    from codecs import open
                    import json
                    with open('v'+str(eachItem['timestamp']) + '.json', 'w', encoding='utf-8') as fp:
                        json.dump(vjson, fp, ensure_ascii=False)

                    import uploadS3
                    uploadS3.upload_verbatim(str(eachItem['timestamp']))

            if eachItem['timestamp'] > latest_timestamp:
                latest_timestamp = eachItem['timestamp']

            progressbar_update_aws(eachItem['username'], int(eachItem['timestamp']), 100, table)

        return latest_timestamp


# Helper class to convert a DynamoDB item to JSON.
# class DecimalEncoder(json.JSONEncoder):
#     def default(self, o):
#         if isinstance(o, decimal.Decimal):
#             if o % 1 > 0:
#                 return float(o)
#             else:
#                 return int(o)
#         return super(DecimalEncoder, self).default(o)

# the code below turns response into a nice json format.
# for i in response['Items']:
#     print(json.dumps(i, cls=DecimalEncoder))
