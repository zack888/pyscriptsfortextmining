
########### remember to run ngrok http 5000 in cmd prompt first

from flask import Flask, Response, request
from twilio import twiml


app = Flask(__name__)


# @app.route("/")
# def check_app():
#     # returns a simple string stating the app is working
#     return Response("It works!"), 200
def write_to_csv(filename,inputEle):
    fd = open(filename+'.txt', 'a')
    fd.write(inputEle+'\n')
    fd.close()

@app.route("/", methods=["POST"])
def inbound_sms():
    print "Running!"
    # we get the SMS message from the request. we could also get the
    # "To" and the "From" phone number as well
    inbound_message = request.form.get("Body")
    print inbound_message

    write_to_csv("sms", inbound_message)

    return inbound_message

if __name__ == "__main__":
    app.run(debug=True)