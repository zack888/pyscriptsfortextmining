# coding: utf-8

import xlwt
import xlrd
from decimal import Decimal
import base64

def plotDoubleBar(excelName):
    import plotly
    import plotly.graph_objs as go

    image_filename = 'C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\logo_link.png'  # replace with your own image
    encoded_image = base64.b64encode(open(image_filename, 'rb').read())

    def read_row_in_excel(workbookName, sheetName, rowNumber, colMax):
        rowList = []
        rb = xlrd.open_workbook(workbookName)
        sheet = rb.sheet_by_name(sheetName)
        # row_count = len(sheet.col_values(0))
        col_count = colMax
        column_read = 3
        while column_read < col_count:
            each = sheet.cell(rowNumber, column_read).value
            print type(each)
            if isinstance(each, float):
                rowList.append(int(round(Decimal(each),0)))
            else:
                rowList.append(each)
            column_read += 1
        # return list of elements in row
        return rowList

    def getCumulative(inputList):
        outputList = []
        processingList = []
        for each in inputList:
            processingList.append(float(each))
        count = 1
        for each in processingList:
            outputList.append(int(round(Decimal(sum(processingList[0:count]),0))))
            count += 1
        return outputList

    def getScatterText(inputList):
        outputList = [''] * len(inputList)
        outputList[-1] =str(inputList[-1]) + '%'
        return outputList

    xList = read_row_in_excel(excelName, 'steve', 0, 13)
    print xList
    yList1 = read_row_in_excel(excelName, 'steve', 2, 13)
    print yList1
    yList2 = read_row_in_excel(excelName, 'steve', 6, 13)
    culmulativePercent = getCumulative(yList1)
    print culmulativePercent
    scatterText = getScatterText(culmulativePercent)

    yList1Width=[0.5] * len(yList1)
    ylist2Width=[0.1] * len(yList2)

    trace1 = go.Bar(
        x=xList,
        y=yList1,
        text=yList1,
        textposition='auto',
        width = yList1Width,
        name='Percentage of Mentions'
    )
    trace2 = go.Scatter(
        x=xList,
        y=culmulativePercent,
        mode='lines+markers+text',
        name='Cumulative Percentage',
        text=scatterText,
        textposition='bottom'
    )
    trace3 = go.Bar(
        x=xList,
        y=yList2,
        text=yList2,
        textposition='outside',
        width = ylist2Width,
        name='Percentage of Negative',
        # yaxis='y2',
        marker=dict(color='rgb(255, 0, 0)',)
    )

    data = [trace1, trace2,trace3]
    layout = go.Layout(
        images=[dict(
            source='data:image/png;base64,{}'.format(encoded_image),
            # source="https://wheregottext.com/img/logo_link.png",
            # source="https://images.plot.ly/language-icons/api-home/python-logo.png",
            xref="x",
            yref="y",
            x=4.5,
            y=80,
            sizex=10,
            sizey=60,
            sizing="contain",
            opacity=0.1,
            xanchor="center",
            yanchor="center",
            layer="above")],
        # autosize=True,
        barmode='grouped',
        legend=dict(orientation="h"),
        title='Top 10 Terms',
        font=dict(family='Helvetica Neue, Helvetica', size=20, color='#000000'),
        yaxis=dict(
            title='%',
            autorange=True,
        )
    )
    fig = go.Figure(data=data, layout=layout)
    plot_url = plotly.offline.plot(fig, filename='multiple-axes-double')

plotDoubleBar('testAnalysis1.xlsx')