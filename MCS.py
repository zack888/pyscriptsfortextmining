# -- coding: utf-8 --

import xlwt
import xlrd
from xlutils.copy import copy as xl_copy
from string import punctuation
from random import *
import os
from pathlib import Path
import csv
import operator
import itertools
from csvsort import csvsort
import sys
from datetime import datetime


reload(sys)
sys.setdefaultencoding('utf8')

def create_new_csv(fileName):
    ranGen = str(randint(1, 1000000000))
    print ranGen
    newCsvName = fileName + ranGen
    script_dir = os.path.dirname(__file__)  # <-- absolute dir the script is in
    rel_path = newCsvName + ".csv"
    abs_file_path = os.path.join(script_dir, rel_path)
    my_file = Path(abs_file_path)
    if my_file.exists():
        print "file exists"
        print "trying to save csv with a different name"
        create_new_csv(fileName)
    else:
        with open(newCsvName+'.csv', 'wb') as file:
            print "New csv file " + newCsvName + " created."
    return newCsvName

def write_to_csv(filename,inputEle):
    fd = open(filename+'.csv', 'a')
    fd.write(inputEle+'\n')
    fd.close()

def read_line_csv(inputCsv,lineNum):
    with open(inputCsv+'.csv') as f:
        reader = csv.reader(f)
        line= next(itertools.islice(reader, lineNum, None))
        eachLine=unicode(line[0])
        # print eachLine
        # print eachLine[0][0]
        return eachLine

def number_of_rows_csv(inputCsv):
    with open(inputCsv+'.csv') as f:
        reader = csv.reader(f)
        row_count = sum(1 for row in reader)
        return row_count

def read_column_in_excel(workbookName,sheetName,columnNumber,numOfRows=None):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    if numOfRows is None:
        row_count = len(sheet.col_values(0))
    else:
        row_count = numOfRows
    row_read=0
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def compare_strings_csv(inputCsv):
    repeatedSubstringList = []
    maxRows=number_of_rows_csv(inputCsv)
    count=0
    while count<maxRows-1:
        # might be over excessive to read the whole lines
        # would limit each line in csv to a fixed number of characters
        line1=read_line_csv(inputCsv,count)
        line2=read_line_csv(inputCsv,count+1)
        count+=1
        repeatedSubstring=""
        countChar=0
        if line1==line2:
            repeatedSubstring += line1[:-1]
        else:
            while line1[countChar]==line2[countChar]:
                repeatedSubstring+=line1[countChar]
                countChar+=1
        # filter out all null and single characters
        if repeatedSubstring!="" and len(repeatedSubstring)>3:
            # print repeatedSubstring
            repeatedSubstringList.append(repeatedSubstring)
    if repeatedSubstringList!=[]:
        # remove duplicates
        # repeatedSubstringList=list(set(repeatedSubstringList))
        return repeatedSubstringList
    else:
        return "No repeated substrings"

def longestSubStringFinderZL(inputStr):
    count=1
    #need a new suffix list, to append $ at the back, which is an uncommon symbol
    newSuffixList=[]

    #creating a new excel workbook with newExcelName as the filename
    csvFileName=create_new_csv('suffixList')

    #for each character in str
    for each in inputStr:
        # generating a suffix string for each character
        ele=inputStr[-count:]
        # print "number of suffix elements: " + str(count)
        #limit each ele to a certain number of characters for efficiency
        if len(ele) < 50:
            newSuffixList.append(ele+u'$')
        else:
            newSuffixList.append(ele[:200] + u'$')
        count+=1
    # remove duplicates
    # newSuffixList=list(set(newSuffixList))
    # print 'saving suffixlist to csv'
    for each in newSuffixList:
        write_to_csv(csvFileName, each)

    # print "Sorting csv file now"
    # csvsort('large545341275.csv', [0], output_filename='sorted.csv', has_header=False)
    csvsort(csvFileName+'.csv', [0], has_header=False)
    # compare strings to find lcs
    return compare_strings_csv(csvFileName)

def longest_Phrase(superLongString):
    # superLongString = u"EEEEEEEEABCDEABCDEABCDEFGHEFGH"
    #print time.asctime(time.localtime(time.time()))
    result=longestSubStringFinderZL(superLongString)
    longestPhraseList=[]
    if result!="No repeated substrings":
        for each in result:
            if superLongString.count(each)>1:
                longestPhraseList.append(each)
                # print each
        return max(longestPhraseList, key=len)
    else:
        return ""

def filter_all_comments(all_comments):
    #chars_to_remove=['`','~','!','@','#','$','%','^','&','*','(',')','_','-','+','=','{','[','}','}','|','\',':',';','"',''','<',',','>','.','?','/']
    # input is a list of strings
    outputList=[]
    sc = set(punctuation)
    count=1
    for each in all_comments:
        newEach=each.replace(u',',u'')
        #print 'row number: '+str(count)
        #newEach=''.join([c for c in each if c not in sc])
        newEach = u"".join(c for c in each if c not in [u'!','.',':','`','~','!','@','#','$','%','^','&','*','(',')','_','-','+','=','{','[','}','}','|',u',','\\','/',u'，',u'！',u'。',u' ',' ',';',u';',u'?','?','h','e','l','l','i','p','？',u'？',u'1'])
        outputList.append(newEach)
        count+=1

    # output is a list of filtered strings
    return outputList

def job(workbookName,sheetName,columnNumber):
    startTime = datetime.now()

    allComments=[x.lower() for x in read_column_in_excel(workbookName,sheetName,columnNumber,numOfRows=10)]
    allComments =[x.replace(',', '') for x in allComments]
    # filteredList=filter_all_comments(allComments)
    duplicatesRemoved = list(set(allComments))
    superLongString = u"".join(elem for elem in duplicatesRemoved)
    removeString=""
    for i in range(1,30):
        superLongString=superLongString.replace(removeString,"")
        removeString = longest_Phrase(superLongString)
        print 'lcs is: ' + removeString
        print i
    print datetime.now() - startTime




job('CA2.xlsx','Sheet1',0)