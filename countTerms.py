#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 15:35:08 2018

@author: tzehau
"""

### process and count freq

import numpy as np
import pandas as pd
import re

df = pd.read_csv('pynlpir_SWresults.tsv', sep="\t")
df.Comment_No = pd.to_numeric(df.Comment_No, errors="coerce")

## select only positive reviews

df=df.loc[df.Comment_No <=105,]
dfWord = pd.DataFrame(dtype='object')
dfSentimentScore = pd.DataFrame()

df=df.dropna()
for i in range(0, len(df)):
    xx=df.iloc[i]['POS'].split(';')
    for v in xx:
        yy=re.sub("[()'']", "", v).split(',')
        if yy[1].strip() == 'noun':
            dfWord = dfWord.append([yy[0]])
            dfSentimentScore = dfSentimentScore.append([df.iloc[i]['Sentiment_Score']])
                
results=pd.concat([dfWord, dfSentimentScore], axis=1)
results.columns=['Word', 'Score']
            
wordFreq = results.groupby('Word').count()
scoreMean = results.groupby('Word').aggregate(np.mean)
output = pd.concat([wordFreq, scoreMean], axis=1)
output.columns=['Freq', 'MeanScore']
output.to_csv('pynlpir_WordFreq.tsv', header=True, index=True, sep='\t', mode='w', na_rep="NaN")

## ngrams
#import nltk
#from nltk.util import ngrams
#from collections import Counter
#bigrams = list(ngrams(final, 2))
#Counter(bigrams)

