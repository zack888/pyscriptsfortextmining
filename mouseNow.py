from __future__ import print_function

import pyautogui
import time
import os

print ('Press Ctrl-C to quit,')

try:
    while True:
        # Get and Print the mouse coordinates
        x, y = pyautogui.position()
        positionStr = 'X: ' + str(x).rjust(4) + ' Y: ' + str(y).rjust(4)
        print(positionStr, end='')
        os.system('cls')
        # print('\b' * len(positionStr), end='')

except KeyboardInterrupt:
    print ('\nDone.')