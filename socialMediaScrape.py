# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import time
from selenium import webdriver
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import xlwt
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from time import sleep
import threading
from threading import Thread
from threading import Timer
from selenium.webdriver import ActionChains
import xlrd
from xlutils.copy import copy as xl_copy
import sys as sys
import xlwt
import random
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import smtplib
from email import encoders
import chineseSegmenter
from selenium.common.exceptions import NoSuchElementException
from openpyxl import load_workbook
from decimal import Decimal
import base64
import testEmail
import testEmailPictures
import testEmailNoPics
import time
import pyautogui
import json
import os

import sys
reload(sys)
sys.setdefaultencoding("utf8")

# path_to_chromedriver = 'C:\Users\ZL\Desktop\geckodriver.exe'
# browser = webdriver.Firefox(executable_path = path_to_chromedriver)
# browser.set_page_load_timeout(10)
# timeout_time=10

path_to_chromedriver = 'C:\\Users\\admin\\Desktop\\chromedriver.exe'
chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option("prefs",prefs)
browser = webdriver.Chrome(executable_path = path_to_chromedriver,chrome_options=chrome_options)


# browser.get('chrome://settings/advanced')
# browser.find_element_by_id('privacyContentSettingsButton').click()
# browser.find_element_by_name('popups').click()
timeout_time=5
browser.set_page_load_timeout(timeout_time)
actions=ActionChains(browser)
wait60 = WebDriverWait(browser, 60)
wait10 = WebDriverWait(browser, 10)
wait5 = WebDriverWait(browser, 5)

def read_column_in_excel_with_startRow(workbookName,sheetName,columnNumber,startRow):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=startRow
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def read_column_in_excel_limited(workbookName,sheetName,columnNumber,rowMax):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = rowMax
    row_read=0
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def save_column_in_excel(fileName,sheetNumber,columnNumber,columnList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=25
    for each in columnList:
        Sheet1.write(link_comments, columnNumber, each)
        link_comments += 1
    wb.save(fileName)

def save_row_in_excel(fileName,sheetNumber,rowNumber,rowList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=3
    for each in rowList:
        Sheet1.write(rowNumber, link_comments, each)
        link_comments += 1
    wb.save(fileName)

def save_row_in_xlsx(loadExcel,inputList,rowNum,columnNum):
    #Open an xlsx for reading
    wb = load_workbook(filename = loadExcel)
    #Get the current Active Sheet
    #ws = wb.get_active_sheet()
    #You can also select a particular sheet
    #based on sheet name
    ws = wb.get_sheet_by_name("steve")
    #save the csb file
    for col, val in enumerate(inputList, start=columnNum):
        ws.cell(row=rowNum, column=col).value = val
    wb.save(loadExcel)

def save_col_in_xlsx(loadExcel,inputList,rowNum,columnNum):
    #Open an xlsx for reading
    wb = load_workbook(filename = loadExcel)
    #Get the current Active Sheet
    #ws = wb.get_active_sheet()
    #You can also select a particular sheet
    #based on sheet name
    ws = wb.get_sheet_by_name("steve")
    #save the csb file
    for row, val in enumerate(inputList, start=rowNum):
        ws.cell(row=row, column=columnNum).value = val
    wb.save(loadExcel)

def checkCellValue(workbookName,sheetName,rowNum,colNum):
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    cellval = sheet.cell(rowNum,colNum).value

    if cellval!=int or cellval!=float or cellval!=long:
        cellval=0

    print type(cellval)
    print int(cellval)
    print type(cellval)
    return cellval

def check_exists_by_class(classname):
    try:
        browser.find_element_by_class_name(classname)
    except NoSuchElementException:
        return False
    return True

def check_exists_by_id(id):
    try:
        browser.find_element_by_id(id)
    except NoSuchElementException:
        return False
    return True

def check_exists_by_xpath(xpath):
    try:
        browser.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True

def close_popup_clickable():
    try:
        browser.find_element_by_class_name('sufei-tb-dialog-close').click()
        return True
    except:
        print "Element is not clickable"
        return False

def check_exists_by_partial_link_text(text):
    try:
        browser.find_element_by_partial_link_text(text)
    except NoSuchElementException:
        return False
    return True

def check_exists_by_link_text(text):
    try:
        browser.find_element_by_link_text(text)
    except NoSuchElementException:
        return False
    return True

def get_page_source(url):
    def get_url_edit():
        print 'starting t1'
        print 'getting tmall url'
        browser.get(url)
        print 'ending t1'
    t1 = threading.Thread(target=get_url_edit)
    t1.setDaemon(True)
    t1.start()
    def return_page_source():
        print 'starting t2'
        # first_review = browser.find_element_by_class_name('tm-rate-fulltxt').text
        # print first_review
        print 'ending t2'
    t2 = Timer(timeout_time+5, return_page_source)
    t2.start()
    t2.join()
    #return browser.page_source

def create_new_workbook(fileName, sheetName):
    book = xlwt.Workbook()
    sh = book.add_sheet(sheetName)
    book.save(fileName)

def signInFB(inputUserName,inputPassword):

    usernameField = browser.find_element_by_xpath("//*[@id=\"email\"]")
    usernameField.click()
    usernameField.send_keys(inputUserName)

    time.sleep(1)

    passwordField = browser.find_element_by_xpath("//*[@id=\"pass\"]")
    passwordField.click()
    passwordField.send_keys(inputPassword)

    time.sleep(1)

    try:
        login = browser.find_element_by_id('loginbutton')
        login.click()
    except:
        pass

    time.sleep(2)

    browser.switch_to.default_content()

def open_posts(inputSearch):
    searchField = browser.find_element_by_css_selector('._1frb')
    searchField.send_keys(inputSearch)
    time.sleep(2)
    actions = ActionChains(browser)
    actions.send_keys(Keys.ENTER)
    actions.perform()
    time.sleep(2)
    attempts = 5
    while not check_exists_by_class('_5f9g') and attempts>0:
        browser.find_element_by_link_text('Posts').click()
        time.sleep(2)
        actions.send_keys(Keys.END)
        actions.perform()
        time.sleep(5)
        actions.send_keys(Keys.END)
        actions.perform()
        time.sleep(2)
        attempts-=1
    # browser.find_element_by_link_text('See all').click()
    seeAll = browser.find_element_by_class_name('_5f9g')
    actions = ActionChains(browser)
    actions.move_to_element(seeAll).perform()
    try:
        seeAll.click()
    except:
        time.sleep(5)
        browser.find_element_by_link_text('See all')
    time.sleep(10)

def load_articles_facebook(numTimes):
    for i in range(numTimes):
        try:
            articles = browser.find_elements_by_partial_link_text('Comments')
            actions = ActionChains(browser)
            actions.move_to_element(articles[-1]).perform()
            time.sleep(5)
        except:
            print 'Failed to move to comment '+str(articles[-1])
            print sys.exc_info()[0]

def get_pictures(tmall_urls,excelName,recipientEmail,title):

    def get_page_source_pictures_tmall(url):
        def get_url_edit():
            print 'starting t1'
            browser.get(url)
            browser.execute_script("window.scrollTo(0, 500);")
            #actions.send_keys(Keys.END)
            #actions.perform()
            time.sleep(5)
            if check_exists_by_id("login-banner-wrap"):
                try:
                    print "directed to login page"
                    time.sleep(1)

                    signIn()
                    # relook at this portion again

                    # if check_exists_by_xpath("//*[@id=\"nc_1_n1z\"]"):
                    #     print "slider detected"
                    #     slider = browser.find_element_by_xpath("//*[@id=\"nc_1_n1z\"]")
                    #     # hoverandclick = ActionChains(browser).move_to_element(slider).click()
                    #     # hoverandclick.perform()
                    #     # print 'clicked slider'
                    #     # ActionChains(browser).move_to_element_with_offset(slider,6,10).perform()
                    #     # print 'moved to element'
                    #     # ActionChains(browser).move_to_element_with_offset(slider,6,10).click_and_hold().move_by_offset(300, 0).pause(2).release().perform()
                    #     # time.sleep(1.1)
                    #
                    #     print 'trying py autogui'
                    #
                    #     SlideBar.job()
                    #
                    #     time.sleep(5)
                    #
                    #     print 'tried py auto gui'
                    #
                    #     print 'tried to move slider 1st time'
                    #     time.sleep(2)
                    #
                    #     while check_exists_by_xpath('//*[@id=\"TPL_password_1\"]'):
                    #         passwordField = browser.find_element_by_xpath("//*[@id=\"TPL_password_1\"]")
                    #         passwordField.clear()
                    #         passwordField.click()
                    #         passwordField.send_keys("sosurely8")
                    #         time.sleep(3)
                    #         try:
                    #             getBar =  browser.find_element_by_xpath('/html/body/div[1]/div/div/div[2]/div[3]/form/div[4]/div/span/a')
                    #             getBar.click()
                    #         except:
                    #             pass
                    #         time.sleep(3)
                    #
                    #         print "moving subsequent times"
                    #         print 'trying py autogui'
                    #
                    #         SlideBar.job1()
                    #
                    #         time.sleep(3)
                    #
                    #         print 'tried py auto gui'
                    #         print 'tried subsequent slide'
                    #         time.sleep(3)
                    #
                    # time.sleep(3)

                    # another iframe loads
                    iframe = browser.find_element_by_xpath("/html/body/div/div/div[2]/div[1]/iframe")
                    browser.switch_to.frame(iframe)

                    sendSMS = browser.find_element_by_xpath("//*[@id=\"J_GetCode\"]")
                    sendSMS.click()

                    time.sleep(3)

                    key = browser.find_element_by_xpath("//*[@id=\"J_Checkcode\"]")
                    key.clear()
                    key.click()
                    # key.send_keys("81617818")

                    # over here, read the txt file that the twilio code sends to ngrok and localhost. then extract the number.

                    # then click submit

                    #############  IMPT
                    browser.switch_to.default_content()

                    time.sleep(20)

                    print "going back to the product page url"
                except:
                    pause = raw_input("Error detected")
                    print 'went to exception'
                    print "didnt need to get handphone.. apparently"
            print 'ending t1'
        t1 = threading.Thread(target=get_url_edit)
        t1.setDaemon(True)
        t1.start()
        def return_page_source():
            print 'starting t2'
            #############  IMPT
            browser.switch_to.default_content()
            time.sleep(5)
            browser.execute_script("window.scrollTo(0, 500);")

            # wait10.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="J_TabBar"]/li[2]')))
            try:
                # review_button = browser.find_element_by_xpath('//*[@id="J_TabBar"]/li[3]')
                review_button = browser.find_element_by_partial_link_text("累计评价 ")
                review_button.click()
                time.sleep(5)
                # wait10.until(EC.visibility_of_element_located((By.CLASS_NAME, 'tm-rate-fulltxt')))
                element_to_hover_over = browser.find_element_by_css_selector('.rate-filter > label:nth-child(6)')
                hover = ActionChains(browser).move_to_element(element_to_hover_over)
                hover.perform()
                time.sleep(1)
                # show all pictures section in tmall
                show_all_pics = browser.find_element_by_css_selector('.rate-filter > label:nth-child(6)')
                show_all_pics.click()
            except:
                # nested try except again to catch if its not a tmall url, and send email to say no pictures will be sent
                try:
                    print '2nd try to click reviews button'
                    # review_button = browser.find_element_by_xpath('//*[@id="J_TabBar"]/li[2]')
                    review_button = browser.find_element_by_xpath("/html/body/div[2]/div[3]/div/div[6]/div/div[1]/div/div[1]/ul/li[2]/a")
                    review_button.click()
                    wait10.until(EC.visibility_of_element_located((By.CLASS_NAME, 'tm-rate-fulltxt')))
                    element_to_hover_over = browser.find_element_by_css_selector('.rate-list-picture.rate-radio-group')
                    hover = ActionChains(browser).move_to_element(element_to_hover_over)
                    hover.perform()
                    time.sleep(1)
                    # show all pictures section in tmall
                    show_all_pics = browser.find_element_by_css_selector('.rate-list-picture.rate-radio-group')
                    show_all_pics.click()
                    browser.execute_script("window.scrollTo(0, 500);")
                except:
                    try:
                        print 'third time, trying autogui'
                        # SlideBar.reviewButtonClick()
                        time.sleep(5)
                    except:
                        testEmailNoPics.job(recipientEmail,title)
                        e = sys.exc_info()[0]
            time.sleep(5)
            print 'ending t2'
        t2 = Timer(timeout_time+10, return_page_source)
        t2.start()
        t2.join()

    def get_page_source_pictures_jd(url):
        def get_url_edit():
            print 'starting t1'
            browser.get(url)
            browser.execute_script("window.scrollTo(0, 800);")
            #actions.send_keys(Keys.END)
            #actions.perform()
            time.sleep(5)
            print 'ending t1'
        t1 = threading.Thread(target=get_url_edit)
        t1.setDaemon(True)
        t1.start()
        def return_page_source():
            print 'starting t2'
            wait10.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="detail"]/div[1]/ul/li[5]')))
            try:
                # review_button = browser.find_element_by_xpath('//*[@id="J_TabBar"]/li[3]')
                review_button = browser.find_element_by_css_selector("li.current:nth-child(5)")
                review_button.click()
                # wait10.until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, '晒图')))
                # element_to_hover_over = browser.find_element_by_partial_link_text('晒图')
                # hover = ActionChains(browser).move_to_element(element_to_hover_over)
                # hover.perform()
                time.sleep(10)
                # show all pictures section in tmall
                show_all_pics = browser.find_element_by_css_selector('.filter-list > li:nth-child(2)')
                show_all_pics.click()
            except:
                # nested try except again to catch if its not a tmall url, and send email to say no pictures will be sent
                try:
                    print '2nd try to click reviews button'
                    wait10.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="detail"]/div[1]/ul/li[5]')))
                    review_button = browser.find_element_by_xpath('//*[@id="detail"]/div[1]/ul/li[5]')
                    review_button.click()
                    time.sleep(5)
                    # show all pictures section in tmall
                    show_all_pics = browser.find_element_by_partial_link_text('晒图')
                    show_all_pics.click()
                    browser.execute_script("window.scrollTo(0, 800);")

                except:
                    testEmailNoPics.job(recipientEmail,title)
                    e = sys.exc_info()[0]
            time.sleep(5)
            print 'ending t2'
        t2 = Timer(timeout_time+10, return_page_source)
        t2.start()
        t2.join()

    dictToAWS={}

    for each in tmall_urls:
        if "tmall" in each or each[14:17] == 'com':
            get_page_source_pictures_tmall(each)
            for x in range(2,110,1):
                try:
                    time.sleep(5)
                    print x
                    html = browser.page_source
                    soup = BeautifulSoup(unicode(html), "html.parser")
                    #grab all entry classes
                    all = soup.findAll('td', attrs={'class': 'tm-col-master'})

                    # for each entry
                    for each in all:
                        # comm is the comments for each entry and can be len 1 or 2
                        comm = each.findAll('div', attrs={'class': 'tm-rate-fulltxt'})
                        # get text for each comment
                        comm_text=[x.get_text() for x in comm]
                        # join all comments
                        commJoint='***'.join(comm_text)
                        print commJoint
                        # all the picLinks in each entry
                        picLinks=each.findAll('li')
                        for each in picLinks:
                            # strip off outerhtml
                            picLinksStripped=strip_string(str(each), '<li data-src="', '"')
                            # store each piclink into dict with corresponding comment
                            dictToAWS[picLinksStripped]=commJoint

                    for each in dictToAWS:
                        print each
                        print dictToAWS[each]
                    print "number of pics: "+str(len(dictToAWS))

                    scrollAttempts=0
                    scrollHeight = 500
                    while not scroll_element_into_view('下一页>>'):
                        if scrollAttempts>10:
                            break
                        print 'trying to scroll manually down by pixels'
                        browser.execute_script("window.scrollTo(0, " + str(scrollHeight) + ");")
                        print 'scrolling'
                        scrollAttempts += 1
                        scrollHeight += 500
                        time.sleep(2)

                    attempts = 0
                    while(attempts < 3):
                        try:
                            addAbitScrollHeightTmall(scrollHeight)
                            time.sleep(3)
                            break
                        except:
                            while check_exists_by_class('sufei-tb-dialog-close') and close_popup_clickable():
                                print "pop up class detected after clicking next!!!!!!!!!!!!"
                                nextPage = browser.find_element_by_link_text(u'下一页>>')
                                actions = ActionChains(browser)
                                actions.move_to_element(nextPage).perform()
                                nextPage.click()
                                time.sleep(2 + random.uniform(3.5, 1.9))
                            print "next attempt"
                            time.sleep(8)
                            e = sys.exc_info()[0]
                        attempts+=1
                    if attempts==3:
                        print "ran out of attempts"
                        break
                    time.sleep(5+random.uniform(1.5,1.9))
                except:
                    if check_exists_by_class('sufei-tb-dialog-close') and close_popup_clickable():
                        print "pop up class detected after clicking next!!!!!!!!!!!!"

                    else:
                        contd = raw_input("Press Enter to Continue")
                        e = sys.exc_info()[0]
                        print e
                        break

        elif "jd" in each:
            get_page_source_pictures_jd(each)
            for x in range(2,500,1):
                try:
                    time.sleep(3)
                    print x
                    html = browser.page_source
                    soup = BeautifulSoup(unicode(html), "html.parser")
                    # comm is the comments for each entry and can be len 1 or 2
                    comm = soup.find('div', attrs={'class': 'p-comment'})
                    # get text for each comment
                    comm_text = comm.get_text()
                    print 'comm test is: ' + comm_text
                    # all the picLinks in each entry
                    link = soup.find('img', attrs={'class': 'J-photo-img'})
                    # strip off outerhtml
                    picLinksStripped=strip_string(str(link), '<img class="J-photo-img" src="', '"/>')
                    # store each piclink into dict with corresponding comment
                    dictToAWS[picLinksStripped]=comm_text
                    print dictToAWS
                    print "number of pics: "+str(len(dictToAWS))

                    attempts = 0
                    while(attempts < 3):
                        try:
                            nextPage = browser.find_element_by_css_selector('.J-cursor-right')
                            print 'right click found'
                            # IMPT previously used to have a actionchains hover command here which was causing a bug with this element
                            nextPage.click()
                            time.sleep(1)
                            break
                        except:
                            print "next attempt"
                            time.sleep(3)
                            e = sys.exc_info()[0]
                        attempts+=1
                    if attempts==3:
                        print "ran out of attempts"
                        time.sleep(3)
                        # click to go back to list of comments (only for jd)
                        goBack = browser.find_element_by_xpath('/html/body/div[10]/div[2]/div[4]/div[2]/div[2]/div[1]/ul/li[1]/a')
                        print 'going back to comments'
                        # IMPT previously used to have a actionchains hover command here which was causing a bug with this element
                        goBack.click()
                        sys.exit()
                    time.sleep(5+random.uniform(1.5,1.9))
                except:
                    e = sys.exc_info()[0]
                    contd = raw_input("Press Enter to Continue")
                    print e
                    break


    import xlwt

    create_new_workbook(excelName+"pictures.xls","BlankSheet")

    # open existing workbook
    rb = xlrd.open_workbook(excelName+"pictures.xls", formatting_info=True)
    # make a copy of it
    wb = xl_copy(rb)
    # add sheet to workbook with existing sheets
    Sheet1 = wb.add_sheet('pictureLinks')

    link_comments=0
    for key in dictToAWS:
        Sheet1.write(link_comments, 0, key)
        Sheet1.write(link_comments, 1, dictToAWS[key])
        link_comments += 1

    wb.save(excelName+"pictures.xls")

    print excelName
    print recipientEmail

    emailSuccessPictures = testEmailPictures.job(excelName + "pictures.xls", recipientEmail, title)

    if emailSuccessPictures == False:
        sys.exit()

def scroll_element_into_view(linkTextInput):
    try:
        print 'checking if item is in view'
        scrollTo = browser.find_element_by_link_text(linkTextInput)
        actions = ActionChains(browser)
        actions.move_to_element(scrollTo).perform()
        return True
    except:
        return False

class StoreBef:
    def __init__(self,StoreBef):
        self.b = StoreBef

class StoreAft:
    def __init__(self,StoreAft):
        self.a = StoreAft

def addAbitScrollHeight(className,scrollHeight):
    try:
        nextPage = browser.find_element_by_class_name(className)
        actions = ActionChains(browser)
        actions.move_to_element(nextPage).perform()
        nextPage.click()
    except:
        print 'trying to add abit to scroll height'
        scrollHeight += 10
        browser.execute_script("window.scrollTo(0, " + str(scrollHeight) + ");")
        time.sleep(2)
        nextPage = browser.find_element_by_class_name(className)
        actions = ActionChains(browser)
        actions.move_to_element(nextPage).perform()
        nextPage.click()

def view_more_comments():
    view_more_comments = browser.find_elements_by_class_name('UFIPagerLink')
    actions = ActionChains(browser)
    actions.move_to_element(view_more_comments[-1]).perform()
    print "Clicking view more comments"
    view_more_comments[-1].click()
    time.sleep(10)
    # open up all the replies
    replies = browser.find_elements_by_class_name('UFIReplySocialSentenceVerified')
    for each in replies:
        try:
            print 'opening/ closing reply'
            each.click()
        except:
            print 'can\'t open/close reply'
        time.sleep(5)

def facebook_job(searchTermsList):
    total_comments = []
    articles_to_comments = {}

    get_page_source('https://www.facebook.com')
    signInFB('zltan12340@gmail.com', 'jimpoland')
    time.sleep(1)

    for eachTerm in searchTermsList:
        open_posts(eachTerm)
        load_articles_facebook(3) # can't be too high else browser will lag

        # get each main article before opening comments
        html = browser.page_source
        soup = BeautifulSoup(html, "html.parser")
        allArticles = soup.findAll('div', attrs={'class': '_5pcr userContentWrapper'})

        # comment_buttons = browser.find_elements_by_partial_link_text('Comments')
        comment_buttons = browser.find_elements_by_class_name('_-56')
        # click on view comments for each post first
        for i in range(len(comment_buttons)):
            try:
                print comment_buttons[i].text
                print "Clicking comment button"
                actions = ActionChains(browser)
                actions.move_to_element(comment_buttons[i]).perform()
                comment_buttons[i].click()
                time.sleep(5)
                try:
                    view_more_comments()
                    # try again
                    view_more_comments()
                    # try again
                    view_more_comments()
                    # try again
                    view_more_comments()
                    # try again
                    view_more_comments()
                except:
                    print "Seems like no more comments to open"
                print "saving article and comments into dict"
                html = browser.page_source
                soup = BeautifulSoup(html, "html.parser")
                allComments = soup.findAll('span', attrs={'class': 'UFICommentBody'})
                articles_to_comments[allArticles[i].get_text()] = [eachComment.get_text() for eachComment in allComments]
                time.sleep(10)
                # print "Clicking on comment button to close comments"
                # # seems like when you click on the comments button again, the comments disappear from html
                # actions = ActionChains(browser)
                # actions.move_to_element(comment_buttons[i]).perform()
                # time.sleep(10)
                # comment_buttons[i].click()
                # print comment_buttons[i].text + "Comments closed"
            except:
                print "**********EXCEPTION************"

                print sys.exc_info()[0]

    # just get all comments
    html = browser.page_source
    soup = BeautifulSoup(html, "html.parser")
    allComments = soup.findAll('span', attrs={'class': 'UFICommentBody'})
    for each in allComments:
        total_comments.append(each.get_text())

    for eachComment in total_comments:
        if total_comments != []:
            print eachComment.encode("utf-8")
            print '****************************'
            filename = searchTermsList[0] + 'Comments.txt'
            if os.path.exists(filename):
                append_write = 'a'  # append if already exists
            else:
                append_write = 'w'  # make a new file if not
            file = open(filename, append_write)
            file.write(eachComment.encode("utf-8") + '\n')
            file.close()
        else:
            print "total_comments list is empty"

    with open(searchTermsList[0]+'ArtiNComm.json', 'w') as outfile:
        json.dump(articles_to_comments, outfile)

facebook_job(['macbook'])

    # chineseSegmenter.job(excelName+".xls",'yourData')

    # addSheet(excelName+".xls","sentiScores")

    # sentiGen.job(excelName+".xls",'yourData',get_page_source_repustate,browser)

    # googleNL.job(excelName+".xls",'yourData')

    # print "Generating graphs start"

    # genGraphandTable.job(excelName+".xls")

    # emailSuccess = testEmail.job(excelName + ".xls", recipientEmail, title)
    #
    # if emailSuccess == False:
    #     import sys
    #     sys.exit()