
# -*- coding: utf-8 -*-

import pandas as pd
import re
from collections import Counter
from nltk import ngrams
from datetime import datetime
import jieba.posseg as pseg
import jieba.analyse
# beginning of script
startTime = datetime.now()

import sys

# reload(sys)
# sys.setdefaultencoding("utf8")

#### import with xls or xlsx - result file from WGT
# xl=pd.ExcelFile('./DataSets/Antioxidant_Data.xls')
# check sheet_names
# print(xl.sheet_names)
# df = xl.parse('yourData')

def job(commentsList,timestamp):
    df = pd.read_csv(commentsList, sep="\t", header=None,encoding='utf-8',engine='python')


    # try not filtering any stop words out
    stopwords = [ line.strip('\n') for line in open('chinese_stop_wordsV3.txt',"r") ]
    # sentence="Really loving this multi-purpose thermal spring water spray.  first of all I like the size of the bottle...it will definitely last me longer than most aqua sprays, appreciate the simple design and the easy squeeze pump spray. It didn't comes out in a gentle mist."
    # sentence="it seems just water, so it didn't do any thing, and the nozzle broken after a few days."
    def segSentence(paragraph):
        for sent in re.findall(u'[^!?。！，\.\!\?]+[!?。！，\.\!\?]?', paragraph, flags=re.U):
            yield sent


    def remove_characters_before_tokenization(sentence, keep_apostrophes=False):
        sentence = sentence.strip()
        if keep_apostrophes:
            PATTERN = r'[?|$|&|*|%|@|(|)|~|.|!|,|"|\'|-|+|;|:| |~|，|～]'  # add other characters here to remove them
            filtered_sentence = re.sub(PATTERN, r'', sentence)
        else:
            PATTERN = r'[^a-zA-Z0-9 ]'  # only extract alpha-numeric characters
            filtered_sentence = re.sub(PATTERN, r'', sentence)
        return filtered_sentence


    def countAdaptedPos(grams, posTag):
        gramsPos = []
        for x, y in grams:
            gramsPos.append(y)

        gramsPos = Counter(gramsPos)
        # Counter({u'DET': 1, u'ADP': 1})

        count = 0
        for x, y in gramsPos.items():
            if x in posTag:
                count = count + y
        return count

    n2grams_Pair = pd.DataFrame(dtype='object')
    ngramsVerAdj_per_ngram = pd.DataFrame(dtype='object')

    # posTag = ['NOUN', 'ADP', 'ADV', 'CC', 'DT', 'EX', 'FW']
    adJVerb = ['Ag', 'a', 'an', 'ad', 'Ng', 'n', 'nr', 'ns', 'nt', 'nz', 's', 't', 'z', 'Vg', 'v', 'vd', 'vn','Bg','b','nr','nx','o','Rg','r','s','u']

    countsen=0
    for i in range(0, len(df)):
        xx = list(segSentence(df.iloc[i, 0]))
        # xx is [u'Hmmmm.', u' He didnt get ousted BC he got a BJ.', u' He got ousted BC he lied under oath about it.', u' That is what you menat I am sure']
        for sen in xx:
            countsen += 1
            sen = remove_characters_before_tokenization(sen, keep_apostrophes=True)
            # sen = sen.replace(u"，".encode('utf-8'),u"")
            # sen = sen.replace(u"。".encode('utf-8'), u"")
            # sen = sen.replace(u"hellip".encode('utf-8'), u"")
            pos = list(pseg.cut(sen))
            pos = [tuple(v) for v in pos]
            # identifying all verbs and adj in the sentence
            verbadjpersenString = u""
            # for n,m in pos:
            for i in range((len(pos)-1)):
                n,m=pos[i]
                if m in adJVerb and n not in stopwords:
                    verbadjpersenString=verbadjpersenString+n+pos[i+1][0]+u'|'
            # cheered|culminated|blue|

            ### 2grams
            twograms = list(ngrams(pos, 2))
            # twograms is [((u'they', u'PRON'), (u'make', u'VERB')), ((u'make', u'VERB'), (u'me', u'PRON')), ((u'me', u'PRON'), (u'so', u'ADP')), ((u'so', u'ADP'), (u'madddd', u'ADJ'))]
            for grams in twograms:
                n2grams_Pair = n2grams_Pair.append([grams[0][0]+u' '+grams[1][0]])
                # verbadjpersenString=verbadjpersenString.replace(grams[0][0].encode('utf-8')+grams[1][0].encode('utf-8'),u"")
                verbadjpersenString=verbadjpersenString.replace(grams[0][0]+grams[1][0],u"")
                ngramsVerAdj_per_ngram = ngramsVerAdj_per_ngram.append([verbadjpersenString])

    results = pd.concat([n2grams_Pair,ngramsVerAdj_per_ngram], axis=1)
    results.columns = ['Pair','AdjNVerb']
    results = results.sort_values(by=['Pair'], axis=0, ascending=True, inplace=False, kind='quicksort', na_position='last')
    results.to_csv(timestamp+'grams.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")

    #### write results to excel
    # writer = pd.ExcelWriter('Trump.xlsx', engine='xlsxwriter')
    # n2grams_results.to_excel(writer, '2grams', header=True, index=False)
    # writer.save()

    # end of script
    print (datetime.now() - startTime)

    return timestamp+'grams.txt'

if __name__=="__main__":
    job('11111textch.txt','11111')