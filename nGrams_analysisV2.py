#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 11 15:30:50 2018

@author: tzehau
"""
import pynlpir
from nltk import ngrams
#import numpy as np
import pandas as pd
import re
import jieba

pynlpir.open(encoding="utf_8")

#### import with xls or xlsx - result file from WGT
xl=pd.ExcelFile('Data.xls')
# check sheet_names
#print(xl.sheet_names)
df = xl.parse('yourData')

#### import data from tab delimited file
#df = pd.read_csv('./DataSets/HotelNeg.txt', sep="\t", header=None)

## load stopwords file
stopwords = [ line.strip('\n') for line in open('./chinese_stop_wordsV3.txt',"r") ]


n2grams_FirstTerm = pd.DataFrame(dtype='object')
n2grams_SecondTerm = pd.DataFrame(dtype='object')
n2grams_Pair = pd.DataFrame(dtype='object')

n3grams_FirstTerm = pd.DataFrame(dtype='object')
n3grams_SecondTerm = pd.DataFrame(dtype='object')
n3grams_ThirdTerm = pd.DataFrame(dtype='object')
n3grams_Triplet = pd.DataFrame(dtype='object')

n4grams_FirstTerm = pd.DataFrame(dtype='object')
n4grams_SecondTerm = pd.DataFrame(dtype='object')
n4grams_ThirdTerm = pd.DataFrame(dtype='object')
n4grams_FourthTerm = pd.DataFrame(dtype='object')
n4grams_Quartet = pd.DataFrame(dtype='object')

def jieba_segment_all_comments(all_comments_list):
    super_long_string = ""
    for each in all_comments_list:
        super_long_string = super_long_string + each
    output_list = jieba.lcut(super_long_string)
    # outputs list of segmented characters
    return output_list

def segSentence(paragraph):
    for sent in re.findall(u'[^!?。！，\.\!\?]+[!?。！，\.\!\?]?', paragraph, flags=re.U):
        yield sent

for i in range(0, len(df)):
    xx=list(segSentence(df.iloc[i,0]))
    for sen in xx:
        print type(sen)
        pos = pynlpir.segment(sen)
        print pos
        reStopWords_pos = [v for v in pos if v[0] not in stopwords and v[0]!=""]
        
        ### 2grams
        twograms = list(ngrams(reStopWords_pos, 2))
        for grams in twograms:
            if all(any(substr in s for s in grams) for substr in ['noun', 'adjective']) :
                n2grams_FirstTerm = n2grams_FirstTerm.append([grams[0][0]])
                n2grams_SecondTerm = n2grams_SecondTerm.append([grams[1][0]])
                n2grams_Pair =  n2grams_Pair.append([grams[0][0] + '|' + grams[1][0]])
        
        ### 3grams
        threegrams = list(ngrams(reStopWords_pos, 3))
        for grams in threegrams:
            if all(any(substr in s for s in grams) for substr in ['noun', 'adjective']) :
                n3grams_FirstTerm = n3grams_FirstTerm.append([grams[0][0]])
                n3grams_SecondTerm = n3grams_SecondTerm.append([grams[1][0]])
                n3grams_ThirdTerm = n3grams_ThirdTerm.append([grams[2][0]])                
                n3grams_Triplet =  n3grams_Triplet.append([grams[0][0] + '|' + grams[1][0] + '|' + grams[2][0]])
                
        ### 4grams
        fourgrams = list(ngrams(reStopWords_pos, 4))
        for grams in fourgrams:
            if all(any(substr in s for s in grams) for substr in ['noun', 'adjective']) :
                n4grams_FirstTerm = n4grams_FirstTerm.append([grams[0][0]])
                n4grams_SecondTerm = n4grams_SecondTerm.append([grams[1][0]])
                n4grams_ThirdTerm = n4grams_ThirdTerm.append([grams[2][0]])
                n4grams_FourthTerm = n4grams_FourthTerm.append([grams[3][0]]) 
                n4grams_Quartet =  n4grams_Quartet.append([grams[0][0] + '|' + grams[1][0] + '|' + grams[2][0] + '|' + grams[3][0]])
                
n2grams_results=pd.concat([n2grams_FirstTerm, n2grams_SecondTerm, n2grams_Pair], axis=1)
n2grams_results.columns=['First_Term', 'Second_Term', 'n2grams']
n2grams_wordFreq = n2grams_results.groupby('n2grams').count()

n3grams_results=pd.concat([n3grams_FirstTerm, n3grams_SecondTerm, n3grams_ThirdTerm, n3grams_Triplet], axis=1)
n3grams_results.columns=['First_Term', 'Second_Term', 'Third_Term', 'n3grams']
n3grams_wordFreq = n3grams_results.groupby('n3grams').count()

n4grams_results=pd.concat([n4grams_FirstTerm, n4grams_SecondTerm, n4grams_ThirdTerm, n4grams_FourthTerm, n4grams_Quartet], axis=1)
n4grams_results.columns=['First_Term', 'Second_Term', 'Third_Term', 'Fourth_Term', 'n4grams']
n4grams_wordFreq = n4grams_results.groupby('n4grams').count()


##############################################################################################
n2grams_FirstTerm = pd.DataFrame(dtype='object')
n2grams_SecondTerm = pd.DataFrame(dtype='object')
n2grams_Count = pd.DataFrame(dtype='object')

for i in range(0, len(n2grams_wordFreq)):
    xx=n2grams_wordFreq.index[i].split('|')
    n2grams_FirstTerm = n2grams_FirstTerm.append([xx[0]])
    n2grams_SecondTerm = n2grams_SecondTerm.append([xx[1]])
    n2grams_Count = n2grams_Count.append([n2grams_wordFreq.iloc[i,1]])
    
n2gramsProcessed = pd.concat([n2grams_FirstTerm, n2grams_SecondTerm, n2grams_Count], axis=1)
n2gramsProcessed.columns=['First_Term', 'Second_Term', 'ngram_Count']
#n2gramsProcessed.to_csv('./ngrams/n2gramsV1.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")

#################################################################################################
n3grams_FirstTerm = pd.DataFrame(dtype='object')
n3grams_SecondTerm = pd.DataFrame(dtype='object')
n3grams_ThirdTerm = pd.DataFrame(dtype='object')
n3grams_Count = pd.DataFrame(dtype='object')

for i in range(0, len(n3grams_wordFreq)):
    xx=n3grams_wordFreq.index[i].split('|')
    n3grams_FirstTerm = n3grams_FirstTerm.append([xx[0]])
    n3grams_SecondTerm = n3grams_SecondTerm.append([xx[1]])
    n3grams_ThirdTerm = n3grams_ThirdTerm.append([xx[2]])
    n3grams_Count = n3grams_Count.append([n3grams_wordFreq.iloc[i,1]])
    
n3gramsProcessed = pd.concat([n3grams_FirstTerm, n3grams_SecondTerm, n3grams_ThirdTerm, n3grams_Count], axis=1)
n3gramsProcessed.columns=['First_Term', 'Second_Term', 'Third_Term', 'ngram_Count']
#n3gramsProcessed.to_csv('./ngrams/n3gramsV1.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")

#################################################################################################
n4grams_FirstTerm = pd.DataFrame(dtype='object')
n4grams_SecondTerm = pd.DataFrame(dtype='object')
n4grams_ThirdTerm = pd.DataFrame(dtype='object')
n4grams_FourthTerm = pd.DataFrame(dtype='object')
n4grams_Count = pd.DataFrame(dtype='object')

for i in range(0, len(n4grams_wordFreq)):
    xx=n4grams_wordFreq.index[i].split('|')
    n4grams_FirstTerm = n4grams_FirstTerm.append([xx[0]])
    n4grams_SecondTerm = n4grams_SecondTerm.append([xx[1]])
    n4grams_ThirdTerm = n4grams_ThirdTerm.append([xx[2]])
    n4grams_FourthTerm = n4grams_FourthTerm.append([xx[3]])
    n4grams_Count = n4grams_Count.append([n4grams_wordFreq.iloc[i,1]])
    
n4gramsProcessed = pd.concat([n4grams_FirstTerm, n4grams_SecondTerm, n4grams_ThirdTerm, n4grams_FourthTerm, n4grams_Count], axis=1)
n4gramsProcessed.columns=['First_Term', 'Second_Term', 'Third_Term', 'Fourth_Term', 'ngram_Count']
#n4gramsProcessed.to_csv('./ngrams/n4gramsV1.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")

#### write results to excel
writer = pd.ExcelWriter('ngrams_results.xlsx', engine='xlsxwriter')
n2gramsProcessed.to_excel(writer, '2grams', header=True, index=False)
n3gramsProcessed.to_excel(writer, '3grams', header=True, index=False)
n4gramsProcessed.to_excel(writer, '4grams', header=True, index=False)
writer.save()

