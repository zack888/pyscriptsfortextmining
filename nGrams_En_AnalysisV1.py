#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 31 16:24:07 2018

@author: tzehau
"""


import pandas as pd
import re
from collections import Counter
import nltk
from nltk import ngrams
from nltk.stem import WordNetLemmatizer
#from nltk import word_tokenize, pos_tag

import sys
reload(sys)
sys.setdefaultencoding("utf8")

#### import with xls or xlsx - result file from WGT
#xl=pd.ExcelFile('./DataSets/Antioxidant_Data.xls')
# check sheet_names
#print(xl.sheet_names)
#df = xl.parse('yourData')

df = pd.read_csv('TrumpArtiNCommtext.txt', sep="\t", header=None, encoding='utf8')

print df

# try not filtering any stop words out
# stopwords = [ line.strip('\n') for line in open('english_stop_wordsv2.txt',"r") ]
#sentence="Really loving this multi-purpose thermal spring water spray.  first of all I like the size of the bottle...it will definitely last me longer than most aqua sprays, appreciate the simple design and the easy squeeze pump spray. It didn't comes out in a gentle mist."
#sentence="it seems just water, so it didn't do any thing, and the nozzle broken after a few days."
def segSentence(paragraph):
    for sent in re.findall(u'[^!?。！，\.\!\?]+[!?。！，\.\!\?]?', paragraph, flags=re.U):
        yield sent

def remove_characters_before_tokenization(sentence, keep_apostrophes=False):
    sentence = sentence.strip()
    if keep_apostrophes:
        PATTERN = r'[?|$|&|*|%|@|(|)|~|.|!|,]' # add other characters here to remove them
        filtered_sentence = re.sub(PATTERN, r'', sentence)
    else:
        PATTERN = r'[^a-zA-Z0-9 ]' # only extract alpha-numeric characters
        filtered_sentence = re.sub(PATTERN, r'', sentence)
    return filtered_sentence

def countAdaptedPos(grams, posTag):
    gramsPos=[]
    for x,y in grams:
        gramsPos.append(y)
    
    gramsPos = Counter(gramsPos)
    count = 0
    for x,y in gramsPos.items():
        if x in posTag:
            count=count + y
    return count


n2grams_FirstTerm = pd.DataFrame(dtype='object')
n2grams_SecondTerm = pd.DataFrame(dtype='object')
n2grams_Pair = pd.DataFrame(dtype='object')

n3grams_FirstTerm = pd.DataFrame(dtype='object')
n3grams_SecondTerm = pd.DataFrame(dtype='object')
n3grams_ThirdTerm = pd.DataFrame(dtype='object')
n3grams_Triplet = pd.DataFrame(dtype='object')

n4grams_FirstTerm = pd.DataFrame(dtype='object')
n4grams_SecondTerm = pd.DataFrame(dtype='object')
n4grams_ThirdTerm = pd.DataFrame(dtype='object')
n4grams_FourthTerm = pd.DataFrame(dtype='object')
n4grams_Quartet = pd.DataFrame(dtype='object')


posTag=['ADJ', 'NOUN', 'ADP', 'ADV', 'VERB']
GAP_PATTERN = r'\s+'
regex_wt = nltk.RegexpTokenizer(pattern=GAP_PATTERN, gaps=True)
wnl = WordNetLemmatizer()

for i in range(0, len(df)):
    xx=list(segSentence(df.iloc[i,0]))
    for sen in xx:
        sen=remove_characters_before_tokenization(sen, keep_apostrophes=True)
        words = regex_wt.tokenize(sen.lower())
        # filtered_words = [v for v in words if v not in stopwords]
        filtered_words = [v for v in words]
        # filtered_words = [wnl.lemmatize(token, 'v') for token in filtered_words]
        wordsPos = nltk.pos_tag(filtered_words, tagset='universal')


        ### 2grams
        twograms = list(ngrams(wordsPos, 2))
        for grams in twograms:
            count=countAdaptedPos(grams, posTag)
            if count>=2:
                n2grams_FirstTerm = n2grams_FirstTerm.append([grams[0][0]])
                n2grams_SecondTerm = n2grams_SecondTerm.append([grams[1][0]])
                n2grams_Pair =  n2grams_Pair.append([grams[0][0] + '|' + grams[1][0]])
        
        ### 3grams
        threegrams = list(ngrams(wordsPos, 3))
        for grams in threegrams:            
            count=countAdaptedPos(grams, posTag)
            if count>=2:
                n3grams_FirstTerm = n3grams_FirstTerm.append([grams[0][0]])
                n3grams_SecondTerm = n3grams_SecondTerm.append([grams[1][0]])
                n3grams_ThirdTerm = n3grams_ThirdTerm.append([grams[2][0]])                
                n3grams_Triplet =  n3grams_Triplet.append([grams[0][0] + '|' + grams[1][0] + '|' + grams[2][0]])
        
        ### 4grams
        fourgrams = list(ngrams(wordsPos, 4))
        for grams in fourgrams:
            count=countAdaptedPos(grams, posTag)   
            if count>=2:
                n4grams_FirstTerm = n4grams_FirstTerm.append([grams[0][0]])
                n4grams_SecondTerm = n4grams_SecondTerm.append([grams[1][0]])
                n4grams_ThirdTerm = n4grams_ThirdTerm.append([grams[2][0]])
                n4grams_FourthTerm = n4grams_FourthTerm.append([grams[3][0]]) 
                n4grams_Quartet =  n4grams_Quartet.append([grams[0][0] + '|' + grams[1][0] + '|' + grams[2][0] + '|' + grams[3][0]])


n2grams_results=pd.concat([n2grams_FirstTerm, n2grams_SecondTerm, n2grams_Pair], axis=1)
n2grams_results.columns=['First_Term', 'Second_Term', 'n2grams']
n2grams_wordFreq = n2grams_results.groupby('n2grams').count()                
                
n3grams_results=pd.concat([n3grams_FirstTerm, n3grams_SecondTerm, n3grams_ThirdTerm, n3grams_Triplet], axis=1)
n3grams_results.columns=['First_Term', 'Second_Term', 'Third_Term', 'n3grams']
n3grams_wordFreq = n3grams_results.groupby('n3grams').count()

n4grams_results=pd.concat([n4grams_FirstTerm, n4grams_SecondTerm, n4grams_ThirdTerm, n4grams_FourthTerm, n4grams_Quartet], axis=1)
n4grams_results.columns=['First_Term', 'Second_Term', 'Third_Term', 'Fourth_Term', 'n4grams']
n4grams_wordFreq = n4grams_results.groupby('n4grams').count()

##############################################################################################
n2grams_FirstTerm = pd.DataFrame(dtype='object')
n2grams_SecondTerm = pd.DataFrame(dtype='object')
n2grams_Count = pd.DataFrame(dtype='object')

for i in range(0, len(n2grams_wordFreq)):
    xx=n2grams_wordFreq.index[i].split('|')
    n2grams_FirstTerm = n2grams_FirstTerm.append([xx[0]])
    n2grams_SecondTerm = n2grams_SecondTerm.append([xx[1]])
    n2grams_Count = n2grams_Count.append([n2grams_wordFreq.iloc[i,1]])
    
n2gramsProcessed = pd.concat([n2grams_FirstTerm, n2grams_SecondTerm, n2grams_Count], axis=1)
n2gramsProcessed.columns=['First_Term', 'Second_Term', 'ngram_Count']
#n2gramsProcessed.to_csv('./ngrams/n2gramsV1.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")

#################################################################################################
n3grams_FirstTerm = pd.DataFrame(dtype='object')
n3grams_SecondTerm = pd.DataFrame(dtype='object')
n3grams_ThirdTerm = pd.DataFrame(dtype='object')
n3grams_Count = pd.DataFrame(dtype='object')

for i in range(0, len(n3grams_wordFreq)):
    xx=n3grams_wordFreq.index[i].split('|')
    n3grams_FirstTerm = n3grams_FirstTerm.append([xx[0]])
    n3grams_SecondTerm = n3grams_SecondTerm.append([xx[1]])
    n3grams_ThirdTerm = n3grams_ThirdTerm.append([xx[2]])
    n3grams_Count = n3grams_Count.append([n3grams_wordFreq.iloc[i,1]])
    
n3gramsProcessed = pd.concat([n3grams_FirstTerm, n3grams_SecondTerm, n3grams_ThirdTerm, n3grams_Count], axis=1)
n3gramsProcessed.columns=['First_Term', 'Second_Term', 'Third_Term', 'ngram_Count']
#n3gramsProcessed.to_csv('./ngrams/n3gramsV1.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")

#################################################################################################
n4grams_FirstTerm = pd.DataFrame(dtype='object')
n4grams_SecondTerm = pd.DataFrame(dtype='object')
n4grams_ThirdTerm = pd.DataFrame(dtype='object')
n4grams_FourthTerm = pd.DataFrame(dtype='object')
n4grams_Count = pd.DataFrame(dtype='object')

for i in range(0, len(n4grams_wordFreq)):
    xx=n4grams_wordFreq.index[i].split('|')
    n4grams_FirstTerm = n4grams_FirstTerm.append([xx[0]])
    n4grams_SecondTerm = n4grams_SecondTerm.append([xx[1]])
    n4grams_ThirdTerm = n4grams_ThirdTerm.append([xx[2]])
    n4grams_FourthTerm = n4grams_FourthTerm.append([xx[3]])
    n4grams_Count = n4grams_Count.append([n4grams_wordFreq.iloc[i,1]])
    
n4gramsProcessed = pd.concat([n4grams_FirstTerm, n4grams_SecondTerm, n4grams_ThirdTerm, n4grams_FourthTerm, n4grams_Count], axis=1)
n4gramsProcessed.columns=['First_Term', 'Second_Term', 'Third_Term', 'Fourth_Term', 'ngram_Count']
#n4gramsProcessed.to_csv('./ngrams/n4gramsV1.txt', header=True, index=False, sep='\t', mode='w', na_rep="NaN")


#### write results to excel
writer = pd.ExcelWriter('Trump.xlsx', engine='xlsxwriter')
n2gramsProcessed.to_excel(writer, '2grams', header=True, index=False)
n3gramsProcessed.to_excel(writer, '3grams', header=True, index=False)
n4gramsProcessed.to_excel(writer, '4grams', header=True, index=False)
writer.save()


###### spacy
## do not tokonize hyphens
#import spacy
#from spacy.tokenizer import Tokenizer
#
#prefix_re = re.compile(r'''^[\[\("']''')
#suffix_re = re.compile(r'\s+')
#infix_re = re.compile(r'\s+')
#simple_url_re = re.compile(r'''^https?://''')
#
#def custom_tokenizer(nlp):
#    return Tokenizer(nlp.vocab, prefix_search=prefix_re.search,
#                                suffix_search=suffix_re.search,
#                                infix_finditer=infix_re.finditer)
#
#nlp = spacy.load('en')
#nlp.tokenizer = custom_tokenizer(nlp)
#
#doc=nlp.tokenizer(sentence)
#for token in doc:
#    print(token.text + '\t' + token.p