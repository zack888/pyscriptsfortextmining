from bs4 import BeautifulSoup
import time
from selenium import webdriver
import xlrd
from xlutils.copy import copy as xl_copy
import sys
import random

dhl_sg='https://www.dpdhl.jobs/search-jobs/Singapore/1886/2/1880251/1x36667/103x8/0/2'

seek_urls = [dhl_sg]

jobsListCount = 0
allLinksCount = 0
allDescriptionsCount = 0
# variables to add to output json
runCount = 0
falseTrueChecks = []
listOfJobLists = []

def job(ref,queueNum,jobCheckRow,sgJobSheetNum,get_page_source,browser):
    jsonForUpdate = {"url": dhl_sg, "title": "DHL", "updated": True,
                     "lastChecked": time.asctime(time.localtime(time.time())), "selfField": ""}
    allList =[]
    jobsList = []
    descriptionsList = []
    allLinks = []
    # variables to add to output json
    global lenJobList
    global listOfJobLists
    global runCount
    for each in seek_urls:
        get_page_source(each)
        for x in range(2,3,1):
            try:
                print x
                #job_page = browser.find_element_by_xpath('//*[@id="app"]/div/div/div[1]/div/div[2]/span/div/section/div[2]/div[2]/div[2]/div[2]/div/div[1]').text
                html = browser.page_source
                soup = BeautifulSoup(unicode(html), "html.parser")

                allJobs = soup.findAll('h2', attrs={'class': 'job-title icon'})
                for each in allJobs:
                    jobsList.append(each.get_text())

                listOfJobLists.append(jobsList)

                print listOfJobLists

                allCon = soup.findAll('li', attrs={'class': 'job-entry'})

                for div in allCon:
                    # only want to find the first link, so using find instead of findall
                    link = div.find('a')
                    # for a in links:
                    allLinks.append('https://www.dpdhl.jobs'+link['href'])

                print allLinks

                descriptions = soup.findAll('span', attrs={'class': 'icon job-date-posted'})
                for each in descriptions:
                    descriptionsList.append('date posted: ' + each.get_text())

                print descriptionsList

                time.sleep(10+random.uniform(1.5,1.9))

            except:
                e = sys.exc_info()[0]
                print e
                break
        time.sleep(0)

    # open existing workbook
    rb = xlrd.open_workbook('SGJobs.xls', formatting_info=True)
    # make a copy of it
    wb = xl_copy(rb)
    # pull a sheet by name
    Sheet1 = wb.get_sheet(sgJobSheetNum)

    global jobsListCount
    global allDescriptionsCount
    global allLinksCount

    for each in jobsList:
        Sheet1.write(jobsListCount, 0, each)
        jobsListCount += 1
    #
    for each in descriptionsList:
        Sheet1.write(allDescriptionsCount, 1, each)
        allDescriptionsCount += 1
    #
    for each in allLinks:
        Sheet1.write(allLinksCount, 2, each)
        Sheet1.write(allLinksCount, 3, time.asctime(time.localtime(time.time())))
        allLinksCount += 1

    wb.save("SGJobs.xls")
    # chunk of code that checks for updates
    if runCount > 0:
        print "checking if there has been any updates in jobs"
        if listOfJobLists[runCount] == listOfJobLists[runCount - 1]:
            print "False"
            falseTrueChecks.append('False')
            jsonForUpdate["updated"] = False
        else:
            print "True"
            falseTrueChecks.append('True')
            jsonForUpdate["updated"] = True
    print "The runcount is: " + str(runCount)
    runCount += 1
    print falseTrueChecks
    print "jsonForUpdate = " + str(jsonForUpdate["updated"])

    # open existing workbook
    rb = xlrd.open_workbook('JobsCheck.xls', formatting_info=True)
    # make a copy of it
    wb = xl_copy(rb)
    # pull a sheet by name
    Sheet1 = wb.get_sheet(0)

    Sheet1.write(jobCheckRow, 0, jsonForUpdate['title'])
    Sheet1.write(jobCheckRow, 1, jsonForUpdate['updated'])
    Sheet1.write(jobCheckRow, 2, time.asctime(time.localtime(time.time())))
    jsonForUpdate["lastChecked"] = time.asctime(time.localtime(time.time()))

    wb.save("JobsCheck.xls")

    # EDITED#

    users_ref = ref.child('items').child(str(queueNum)).set(jsonForUpdate)
    print 'sent to backend'

    # END EDITED#


# job()
# # schedule.every(5).seconds.do(job)
# schedule.every(6).hours.do(job)
#
# while True:
#     schedule.run_pending()
#     time.sleep(1)