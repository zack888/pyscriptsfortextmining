# -*- coding: utf-8 -*-

import time
from selenium import webdriver
import threading
from threading import Timer
import testEmailOPM
import sys
from time import sleep
import logging
logging.basicConfig()
import schedule
import time
import testEmailOPM


path_to_chromedriver = 'C:\\Users\\admin\\Desktop\\chromedriver.exe'
browser = webdriver.Chrome(executable_path = path_to_chromedriver)
browser.set_page_load_timeout(10)
timeout_time=10

def get_page_source(url):
    def get_url_edit():
        print 'starting t1'
        browser.get(url)
        time.sleep(5)
        print 'ending t1'
    t1 = threading.Thread(target=get_url_edit)
    t1.setDaemon(True)
    t1.start()
    def return_page_source():
        print 'starting t2'
        print 'ending t2'
    t2 = Timer(timeout_time+10, return_page_source)
    t2.start()
    t2.join()
    #return browser.page_source


def job():
    get_page_source('https://www.mangapanda.com/onepunch-man')

    checkChap = browser.find_element_by_xpath('//*[@id="latestchapters"]/ul/li[1]/a')

    file = open('trackOPM.txt', 'a')
    file.write(checkChap.text+'\n')
    file.close()

def checkTxt():
    with open('trackOPM.txt', 'r') as f:
        chaps = f.readlines()
    print chaps
    if chaps[-1]!=chaps[-2]:
        testEmailOPM.job(chaps[-1], 'tzl_ngs@hotmail.com', 'OPM NEW CHAP')
        testEmailOPM.job(chaps[-1], 'kgzx1989@gmail.com', 'OPM NEW CHAP')
        testEmailOPM.job(chaps[-1], 'lam.t@pg.com', 'OPM NEW CHAP')

schedule.every().day.at("06:00").do(job)

while True:
    schedule.run_pending()
    time.sleep(1)