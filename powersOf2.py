def powersOf2(n):
    if n<1:
        return 0
    elif n==1:
        print 1
        return 1
    else:
        prev = powersOf2(n/2)
        curr = prev*2
        print curr
        return curr

# powersOf2(3)


def testCase(n):
    if n<1:
        return 0
    elif n==1:
        return 1
    else:
        testCase(n-1)
        print n

def fib(n):
    if n<=0:
        return 0
    elif n==1:
        return 1
    else:
        return fib(n-1)+fib(n-2)


# print fib(4)

numChars = 26

def printSortedStrings(remaining):
    printSortedStrings(remaining,"")

def printSortedStrings(remaining, prefix):
    if remaining == 0:
        if isInOrder(prefix):
            print prefix

    else:
        i=0
        while i < numChars:
            c = ithLetter()