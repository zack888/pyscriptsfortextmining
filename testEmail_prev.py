# -*- coding: utf-8 -*-

import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import sys
import time

def send_email(fileName,toEmail,title):
    fromaddr = "info@wheregottext.com"
    toaddr = toEmail
    bcc = ['zltan12340@gmail.com']
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = title

    body = "Thank you for using wheregottext.com\n\nPlease note that Where Got Text is currently made primarily for the chinese market. However, if there is demand/need for it to be used for English or other languages, those capabilities could be boosted very quickly. Please advise and send feedback.\n\nBringing data to the masses since 2017:)\n\nIf you clicked the option to get pictures, text will be sent in a separate email!\n\nPlease Log in to www.wheregottext.com dashboard to see the analysis."
    msg.attach(MIMEText(body, 'plain'))

    attachment = "C:\\Users\\admin\\PycharmProjects\\host\\" + fileName
    # attachment2 = "C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\textMiningForMoney\\freqOfMentions.html"
    # attachment3 = "C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\textMiningForMoney\\Positives.html"
    # attachment4 ="C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\textMiningForMoney\\Negatives.html"

    filenames = [attachment]
    attachmentNames = ['Data.xls']

    count = 0
    for each in filenames:
        print each
        part = MIMEBase('application', 'octet-stream')
        # part.set_payload((attachment).read())
        part.set_payload(open(each, 'rb').read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "attachment; filename= %s" % attachmentNames[count])
        msg.attach(part)
        count += 1

    print'ok'

    toaddrs = [toaddr] + bcc
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login(fromaddr, "Randomness8")
    text = msg.as_string()
    server.sendmail(fromaddr, toaddrs, text)
    server.quit()

    print 'sending email ok'


def job(fileName,toEmail,title):
    attempt = 0
    while attempt < 1000:
        try:
            send_email(fileName,toEmail,title)
            break
        except:
            print "Error: unable to send email"
            # maybe ip addr changing, try one more time
            time.sleep(10)
            attempt += 1
    return True

# job('1518068914039.xls','wong.j.11@pg.com','SeeYoung Ginger')