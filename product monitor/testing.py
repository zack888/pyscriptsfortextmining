# -*- coding: utf-8 -*-

import boto3
import json
import decimal
from boto3.dynamodb.conditions import Key, Attr
import GetURLUpdateResult
from openpyxl import load_workbook
import xlwt
import xlrd
import sys
import GetURLUpdateResult
import time

###### CREDENTIALS STUFF ###############
dynamodb = boto3.resource('dynamodb', region_name='ap-southeast-1')

client = boto3.client(
    'dynamodb',
    aws_access_key_id='AKIAJQZCRP2UQ2GCPXVQ',
    aws_secret_access_key='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'
)

table = dynamodb.Table('WGTTask')
analysis = dynamodb.Table('WGT_Analysis')
monitor = dynamodb.Table('WGT_Monitor')
###### CREDENTIALS STUFF ###############

def read_column_in_excel(workbookName, sheetName, columnNumber):
    columnList = []
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read = 0
    while row_read < row_count:
        each = sheet.cell(row_read, columnNumber).value
        columnList.append(each)
        row_read += 1
    # return list of elements in column
    return columnList

def genlistOfTenLists(wordList, verbatimList, sentiList):
    listOfTenLists = []
    for eachWord in wordList:
        countVerbatim = 0
        eachWordList = []
        for eachVerbatim in verbatimList:
            if eachWord in eachVerbatim:
                eachWordList.append(sentiList[countVerbatim])
            else:
                eachWordList.append('')
            countVerbatim += 1
        listOfTenLists.append(eachWordList)

    return listOfTenLists

def findPercentageList(countList, verbatimList):
        percentageList = []
        for eachcount in countList:
            eachPercentage = round((float(eachcount) / len(verbatimList)) * 100, 2)
            percentageList.append(eachPercentage)

        return percentageList


def listOfNegPercentage(listofTenLists):
    listOfNeg = []
    for eachList in listofTenLists:
        countNum = 0.0
        countNeg = 0.0
        for eachval in eachList:
            if eachval != '':
                countNum += 1
                eachvalconvert = float(eachval)
                if eachvalconvert < 0.0:
                    countNeg += 1
        # for some terms, there are no comments. WHY?
        # maybe due to some weird characters
        if countNum == 0.0 or countNeg == 0.0:
            negPercent = 0
        else:
            negPercent = round((countNeg / countNum) * 100, 0)
        listOfNeg.append(negPercent)

    return listOfNeg

wordList = read_column_in_excel(str(1)+".xls", 'frequencyList', 0)
print wordList
countList = read_column_in_excel(str(1)+".xls", 'frequencyList', 1)
sentiList = read_column_in_excel(str(1)+".xls", 'sentiScores', 0)
verbatimList = read_column_in_excel(str(1)+".xls", 'yourData', 0)
listofTenLists = genlistOfTenLists(wordList, verbatimList, sentiList)

percentageList = findPercentageList(countList, verbatimList)

negPercentage = listOfNegPercentage(listofTenLists)

# intialise list called data for plotting in frontend
data = [{
            "name": "",
            "secondary": 0,
            "value": 0
        } for x in range(0, len(wordList), 1)]

count=0

# store values for plotting in frontend
for each in data:
    #chinese terms
    each['name']=wordList[count]
    #percentage of mentions
    each['value']=int(percentageList[count])
    #negative percentage
    each['secondary']=int(negPercentage[count])
    count+=1
    if count==len(wordList):
        break

GetURLUpdateResult.update_aws_analysis(1,data)


def genlistOfTermAndVerbatimList(wordList, verbatimList):
    vjson = []
    for eachWord in wordList:
        listOfVerbatim = []
        for eachVerbatim in verbatimList:
            if eachWord in eachVerbatim:
                listOfVerbatim.append(eachVerbatim)
        vjson.append({eachWord: listOfVerbatim})

    return vjson


vjson = genlistOfTermAndVerbatimList(wordList, verbatimList)

from codecs import open
import json

with open('v' + str(1) + '.json', 'w', encoding='utf-8') as fp:
    json.dump(vjson, fp, ensure_ascii=False)

import uploadS3

uploadS3.upload_verbatim(str(1))