# -- coding: utf-8 --

import xlwt
import xlrd
from xlutils.copy import copy as xl_copy
from suffix_trees import STree
import unicodedata
import re
import time
from string import punctuation
from random import *
import sys
import os
from pathlib import Path
import csv
import operator
import itertools
from csvsort import csvsort
import sys

reload(sys)
sys.setdefaultencoding('utf8')

def save_element_in_excel(fileName,sheetNumber,columnNumber,rowNumber,element):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    Sheet1.write(rowNumber, columnNumber, element)
    wb.save(fileName)

def create_new_workbook_random(fileName, sheetName):
    book = xlwt.Workbook()
    sh = book.add_sheet(sheetName)
    newExcelName=""
    ranGen = str(randint(1, 1000000000))
    print ranGen
    try:
        xlrd.open_workbook(fileName+ranGen+".xls")
        print "trying to save excel with a different name"
        create_new_workbook_random(fileName, sheetName)
    except:
        book.save(fileName + ranGen + ".xls")
        newExcelName = fileName + ranGen + ".xls"
        print "New Excel Workbook " + newExcelName + " created."
    return newExcelName

def counter_all(inputList):
    from collections import Counter
    cnt=Counter()
    for each in inputList:
        cnt[each]+=1
    #returns a dictionary of counted words
    return cnt

def create_new_csv(fileName):
    ranGen = str(randint(1, 1000000000))
    print ranGen
    newCsvName = fileName + ranGen
    script_dir = os.path.dirname(__file__)  # <-- absolute dir the script is in
    rel_path = newCsvName + ".csv"
    abs_file_path = os.path.join(script_dir, rel_path)
    my_file = Path(abs_file_path)
    if my_file.exists():
        print "file exists"
        print "trying to save csv with a different name"
        create_new_csv(fileName)
    else:
        with open(newCsvName+'.csv', 'wb') as file:
            print "New csv file " + newCsvName + " created."
    return newCsvName

def write_to_csv(filename,inputEle):
    fd = open(filename+'.csv', 'a')
    fd.write(inputEle+'\n')
    fd.close()

def read_line_csv(inputCsv,lineNum):
    with open(inputCsv+'.csv') as f:
        reader = csv.reader(f)
        line= next(itertools.islice(reader, lineNum, None))
        eachLine=unicode(line[0])
        # print eachLine
        # print eachLine[0][0]
        return eachLine

def number_of_rows_csv(inputCsv):
    with open(inputCsv+'.csv') as f:
        reader = csv.reader(f)
        row_count = sum(1 for row in reader)
        return row_count

def compare_strings_csv(inputCsv):
    repeatedSubstringList = []
    maxRows=number_of_rows_csv(inputCsv)
    count=0
    while count<maxRows-1:
        # might be over excessive to read the whole lines
        # would limit each line in csv to a fixed number of characters
        line1=read_line_csv(inputCsv,count)
        line2=read_line_csv(inputCsv,count+1)
        count+=1
        repeatedSubstring=""
        countChar=0
        if line1==line2:
            repeatedSubstring += line1[:-1]
        else:
            while line1[countChar]==line2[countChar]:
                repeatedSubstring+=line1[countChar]
                countChar+=1
        if repeatedSubstring!="" and len(repeatedSubstring)>1:
            print repeatedSubstring
            repeatedSubstringList.append(repeatedSubstring)
    if repeatedSubstringList!=[]:
        return repeatedSubstringList
    else:
        return "No repeated substrings"

def read_column_in_excel(workbookName,sheetName,columnNumber,numOfRows=None):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    if numOfRows is None:
        row_count = len(sheet.col_values(0))
    else:
        row_count = numOfRows
    row_read=0
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def filter_all_comments(all_comments):
    #chars_to_remove=['`','~','!','@','#','$','%','^','&','*','(',')','_','-','+','=','{','[','}','}','|','\',':',';','"',''','<',',','>','.','?','/']
    outputList=[]
    sc = set(punctuation)
    count=1
    for each in all_comments:
        newEach=each.replace(u',',u'')
        #print 'row number: '+str(count)
        #newEach=''.join([c for c in each if c not in sc])
        newEach = u"".join(c for c in each if c not in (u'!','.',':','`','~','!','@','#','$','%','^','&','*','(',')','_','-','+','=','{','[','}','}','|',u',','\\','/',u'，',u'！',u'。',u' ',' ',';',u';',u'?','?','h','e','l','l','i','p','？',u'？'))
        outputList.append(newEach)
        count+=1

    return outputList

def longestSubStringFinderZL(inputStr):
    suffixList=[]
    count=1
    eleRowCount = 0
    #need a new suffix list, to append $ at the back, which is an uncommon symbol
    newSuffixList=[]

    #creating a new excel workbook with newExcelName as the filename
    csvFileName=create_new_csv('suffixList')

    #for each character in str
    for each in inputStr:
        # generating a suffix string for each character
        ele=inputStr[-count:]
        print "number of suffix elements: " + str(count)
        #def save_element_in_excel(fileName,sheetNumber,columnNumber,rowNumber,element)
        #limit each ele to a certain number of characters for efficiency
        if len(ele) < 10:
            write_to_csv(csvFileName,ele+u'$')
        else:
            write_to_csv(csvFileName,ele[:10]+u'$')
        count+=1
        eleRowCount+=1

    print "Sorting csv file now"
    # csvsort('large545341275.csv', [0], output_filename='sorted.csv', has_header=False)
    csvsort(csvFileName+'.csv', [0], has_header=False)

    return compare_strings_csv(csvFileName)

def top_trending_words(excelName,sheetName,runNum):
    all_comments = read_column_in_excel(excelName, sheetName, 0, runNum)
    filteredList = filter_all_comments(all_comments)
    superLongString = u"".join(elem for elem in filteredList)
    #print time.asctime(time.localtime(time.time()))
    result=longestSubStringFinderZL(superLongString)
    print result
    if result!="No repeated substrings":
        repeatedSubStringName=create_new_csv('repeatedSubstringList')
        for each in result:
            write_to_csv(repeatedSubStringName,each)
    return repeatedSubStringName
    #print time.asctime(time.localtime(time.time()))

def job(excelName,sheetName):
    csvName=top_trending_words(excelName,sheetName,100)
    rowNum=number_of_rows_csv(csvName)
    allRepeatedSubStringsList=[]
    for each in range(0,rowNum,1):
        eachLine=read_line_csv(csvName,each)
        allRepeatedSubStringsList.append(eachLine)
    print "Longest common substring:"
    print max(allRepeatedSubStringsList, key=len)
    counted_terms = counter_all(allRepeatedSubStringsList)
    sorted_terms = counted_terms.most_common()
    print sorted_terms
    print "Most common substring:"
    print sorted_terms[0][0]
    print "Possible emerging substring"
    print sorted_terms[-1][0]

    sortedfilename=create_new_workbook_random('sortedTerms','sorted')

    countsorted=0
    for each in sorted_terms:
        save_element_in_excel(sortedfilename, 0, 0, countsorted, each[0])
        save_element_in_excel(sortedfilename, 0, 1, countsorted, each[1])
        countsorted += 1

job("pantene.xls","yourData")