# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import time
from selenium import webdriver
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import xlwt
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from time import sleep
import threading
from threading import Thread
from threading import Timer
from selenium.webdriver import ActionChains
import xlrd
from xlutils.copy import copy as xl_copy
import sys
import xlwt
import random
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEBase import MIMEBase
from email import encoders
import chineseSegmenter
from selenium.common.exceptions import NoSuchElementException
from openpyxl import load_workbook
from decimal import Decimal
import base64
# import sentiGen
# import genGraphandTable
# import googleNL
# import sentiGen

path_to_chromedriver = 'C:\Users\ZL\Desktop\chromedriver.exe'
browser = webdriver.Chrome(executable_path = path_to_chromedriver)
# browser.get('chrome://settings/advanced')
# browser.find_element_by_id('privacyContentSettingsButton').click()
# browser.find_element_by_name('popups').click()
timeout_time=60
browser.set_page_load_timeout(timeout_time)
actions=ActionChains(browser)
wait60 = WebDriverWait(browser, 60)
wait10 = WebDriverWait(browser, 10)
wait5 = WebDriverWait(browser, 5)

def plotDoubleBar(excelName):
    import plotly
    import plotly.graph_objs as go

    image_filename = 'C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\logo_link.png'  # replace with your own image
    encoded_image = base64.b64encode(open(image_filename, 'rb').read())

    def read_row_in_xlsx(workbookName, sheetName, rowNumber, colMax):
        rowList=[]
        wb = load_workbook(filename=workbookName, read_only=True)
        ws = wb.get_sheet_by_name(sheetName)
        col_count = colMax+1
        column_read = 4
        while column_read<col_count:
            each = ws.cell(row=rowNumber+1, column=column_read).value
            if isinstance(each, float):
                rowList.append(int(round(Decimal(each),0)))
            else:
                rowList.append(each)
            column_read += 1
        return rowList

    def read_row_in_excel(workbookName, sheetName, rowNumber, colMax):
        rowList = []
        rb = xlrd.open_workbook(workbookName)
        sheet = rb.sheet_by_name(sheetName)
        # row_count = len(sheet.col_values(0))
        col_count = colMax
        column_read = 3
        while column_read < col_count:
            each = sheet.cell(rowNumber, column_read).value
            if isinstance(each, float):
                rowList.append(int(round(Decimal(each),0)))
            else:
                rowList.append(each)
            column_read += 1
        # return list of elements in row
        return rowList

    def getCumulative(inputList):
        outputList = []
        processingList = []
        for each in inputList:
            processingList.append(float(each))
        count = 1
        for each in processingList:
            outputList.append(int(round(Decimal(sum(processingList[0:count]),0))))
            count += 1
        return outputList

    def getScatterText(inputList):
        outputList = [''] * len(inputList)
        outputList[-1] =str(inputList[-1]) + '%'
        return outputList

    xList = read_row_in_excel(excelName, 'steve', 0, 13)
    print xList
    yList1 = read_row_in_excel(excelName, 'steve', 2, 13)
    print yList1
    yList2 = read_row_in_excel(excelName, 'steve', 6, 13)
    culmulativePercent = getCumulative(yList1)
    print culmulativePercent
    scatterText = getScatterText(culmulativePercent)

    yList1Width=[0.5] * len(yList1)
    ylist2Width=[0.1] * len(yList2)

    trace1 = go.Bar(
        x=xList,
        y=yList1,
        text=yList1,
        textposition='auto',
        width = yList1Width,
        name='Percentage of Mentions'
    )
    trace2 = go.Scatter(
        x=xList,
        y=culmulativePercent,
        mode='lines+markers+text',
        name='Cumulative Percentage',
        text=scatterText,
        textposition='bottom'
    )
    trace3 = go.Bar(
        x=xList,
        y=yList2,
        text=yList2,
        textposition='outside',
        width = ylist2Width,
        name='Percentage of Negative',
        # yaxis='y2',
        marker=dict(color='rgb(255, 0, 0)',)
    )

    data = [trace1, trace2,trace3]
    layout = go.Layout(
        images=[dict(
            source='data:image/png;base64,{}'.format(encoded_image),
            # source="https://wheregottext.com/img/logo_link.png",
            # source="https://images.plot.ly/language-icons/api-home/python-logo.png",
            xref="x",
            yref="y",
            x=4.5,
            y=80,
            sizex=10,
            sizey=60,
            sizing="contain",
            opacity=0.1,
            xanchor="center",
            yanchor="center",
            layer="above")],
        # autosize=True,
        barmode='grouped',
        legend=dict(orientation="h"),
        title='Top 10 Terms',
        font=dict(family='Helvetica Neue, Helvetica', size=20, color='#000000'),
        yaxis=dict(
            title='%',
            autorange=True,
        )
    )
    fig = go.Figure(data=data, layout=layout)
    plot_url = plotly.offline.plot(fig, filename='freqOfMentions',auto_open=False)

def drawPosTable(workbookName):
    import plotly
    import plotly.graph_objs as go

    def read_row_in_xlsx(sheetName, rowNumber, colMax):
        rowList = []
        wb = load_workbook(filename=workbookName, read_only=True)
        ws = wb.get_sheet_by_name(sheetName)
        col_count = colMax + 1
        column_read = 4
        while column_read < col_count:
            each = ws.cell(row=rowNumber + 1, column=column_read).value
            if isinstance(each, float):
                rowList.append(int(round(Decimal(each), 0)))
            else:
                rowList.append(each)
            column_read += 1
        return rowList

    def read_row_in_excel(sheetName, rowNumber, colMax):
        rowList = []
        rb = xlrd.open_workbook(workbookName)
        sheet = rb.sheet_by_name(sheetName)
        # row_count = len(sheet.col_values(0))
        col_count = colMax
        column_read = 3
        while column_read < col_count:
            each = sheet.cell(rowNumber, column_read).value
            if isinstance(each, float):
                rowList.append(int(round(Decimal(each) ,0)))
            else:
                rowList.append(each)
            column_read += 1
        # return list of elements in row
        return rowList


    wordList = read_row_in_excel('steve', 0, 13)

    print wordList

    newWordList = []

    for each in wordList:
        count = 1
        while count<4:
            newEach=each+str(count)
            newWordList.append(newEach)
            count+=1

    print newWordList

    verbatimListPos1 = read_row_in_excel('steve', 19, 13)
    verbatimListPos2 = read_row_in_excel('steve', 20, 13)
    verbatimListPos3 = read_row_in_excel('steve', 21, 13)

    newVerbatimList = []

    count=0
    for each in verbatimListPos1:
        newVerbatimList.append(each)
        newVerbatimList.append(verbatimListPos2[count])
        newVerbatimList.append(verbatimListPos3[count])
        count+=1

    print newVerbatimList

    values = [newWordList,
              newVerbatimList, #verbatimListNeg
              ]

    trace0 = go.Table(
        type='table',
        columnorder=[1, 2],
        columnwidth=[20, 200],
        header=dict(
            values=[['<b>Keywords</b>'],
                    ['<b>POSITIVE</b>'],
                    #['<b>NEGATIVE</b>']
                    ],
            line=dict(color='#506784'),
            fill=dict(color=['#119DFF','green','red']),
            align=['center'],
            font=dict(color='white', size=9),
            height=15
        ),
        cells=dict(
            values=values,
            line=dict(color='#506784'),
            fill=dict(color=['rgb(239,243,255)', 'white']),
            align=['center','left'],
            font=dict(color='#506784', size=12),
            height=30
        ))

    data = [trace0]

    layout = go.Layout(
        title='use ctrl and +/- keys to enlarge/shrink table',
        font=dict(family='Helvetica Neue, Helvetica', size=15, color='#000000')
        )

    fig = go.Figure(data=data, layout=layout)

    plot_url = plotly.offline.plot(fig, filename="Positives",auto_open=False)

def drawNegTable(workbookName):
    import plotly
    import plotly.graph_objs as go

    def read_row_in_xlsx(sheetName, rowNumber, colMax):
        rowList = []
        wb = load_workbook(filename=workbookName, read_only=True)
        ws = wb.get_sheet_by_name(sheetName)
        col_count = colMax + 1
        column_read = 4
        while column_read < col_count:
            each = ws.cell(row=rowNumber + 1, column=column_read).value
            if isinstance(each, float):
                rowList.append(int(round(Decimal(each), 0)))
            else:
                rowList.append(each)
            column_read += 1
        return rowList

    def read_row_in_excel(sheetName, rowNumber, colMax):
        rowList = []
        rb = xlrd.open_workbook(workbookName)
        sheet = rb.sheet_by_name(sheetName)
        # row_count = len(sheet.col_values(0))
        col_count = colMax
        column_read = 3
        while column_read < col_count:
            each = sheet.cell(rowNumber, column_read).value
            if isinstance(each, float):
                rowList.append(int(round(Decimal(each) ,0)))
            else:
                rowList.append(each)
            column_read += 1
        # return list of elements in row
        return rowList

    wordList = read_row_in_excel('steve', 0, 13)

    print wordList

    newWordList = []

    for each in wordList:
        count = 1
        while count<4:
            newEach=each+str(count)
            newWordList.append(newEach)
            count+=1

    print newWordList

    verbatimListPos1 = read_row_in_excel('steve', 22, 13)
    verbatimListPos2 = read_row_in_excel('steve', 23, 13)
    verbatimListPos3 = read_row_in_excel('steve', 24, 13)

    newVerbatimList = []

    count=0
    for each in verbatimListPos1:
        newVerbatimList.append(each)
        newVerbatimList.append(verbatimListPos2[count])
        newVerbatimList.append(verbatimListPos3[count])
        count+=1

    print newVerbatimList

    values = [newWordList,
              newVerbatimList, #verbatimListNeg
              ]

    trace0 = go.Table(
        type='table',
        columnorder=[1, 2],
        columnwidth=[20, 200],
        header=dict(
            values=[['<b>Keywords</b>'],
                    ['<b>Negative</b>'],
                    #['<b>NEGATIVE</b>']
                    ],
            line=dict(color='#506784'),
            fill=dict(color=['#119DFF','red']),
            align=['center'],
            font=dict(color='white', size=9),
            height=15
        ),
        cells=dict(
            values=values,
            line=dict(color='#506784'),
            fill=dict(color=['rgb(239,243,255)', 'white']),
            align=['center','left'],
            font=dict(color='#506784', size=12),
            height=30
        ))

    data = [trace0]

    layout = go.Layout(
        title='use ctrl and +/- keys to enlarge/shrink table',
        font=dict(family='Helvetica Neue, Helvetica', size=15, color='#000000')
        )

    fig = go.Figure(data=data, layout=layout)

    plot_url = plotly.offline.plot(fig, filename="Negatives",auto_open=False)

def open_and_save_xlsx(loadExcel):
    wb = load_workbook(filename=loadExcel)
    wb.save(loadExcel)

def refresh_excel(path):
    from win32com.client import Dispatch
    xl = Dispatch('Excel.Application')
    wb = xl.Workbooks.Open(path)
    wb.RefreshAll()
    wb.Close(True)

def read_column_in_excel_with_startRow(workbookName,sheetName,columnNumber,startRow):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=startRow
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def read_column_in_excel_limited(workbookName,sheetName,columnNumber,rowMax):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = rowMax
    row_read=0
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def save_column_in_excel(fileName,sheetNumber,columnNumber,columnList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=25
    for each in columnList:
        Sheet1.write(link_comments, columnNumber, each)
        link_comments += 1
    wb.save(fileName)

def save_row_in_excel(fileName,sheetNumber,rowNumber,rowList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=3
    for each in rowList:
        Sheet1.write(rowNumber, link_comments, each)
        link_comments += 1
    wb.save(fileName)

def save_row_in_xlsx(loadExcel,inputList,rowNum,columnNum):
    #Open an xlsx for reading
    wb = load_workbook(filename = loadExcel)
    #Get the current Active Sheet
    #ws = wb.get_active_sheet()
    #You can also select a particular sheet
    #based on sheet name
    ws = wb.get_sheet_by_name("steve")
    #save the csb file
    for col, val in enumerate(inputList, start=columnNum):
        ws.cell(row=rowNum, column=col).value = val
    wb.save(loadExcel)

def save_col_in_xlsx(loadExcel,inputList,rowNum,columnNum):
    #Open an xlsx for reading
    wb = load_workbook(filename = loadExcel)
    #Get the current Active Sheet
    #ws = wb.get_active_sheet()
    #You can also select a particular sheet
    #based on sheet name
    ws = wb.get_sheet_by_name("steve")
    #save the csb file
    for row, val in enumerate(inputList, start=rowNum):
        ws.cell(row=row, column=columnNum).value = val
    wb.save(loadExcel)

def moveFreqList(excelName,sheetName):
    wordList=read_column_in_excel_limited(excelName,sheetName,0,10)
    countList=read_column_in_excel_limited(excelName,sheetName,1,10)

    save_row_in_xlsx('textAnalysistestsaved.xlsx', wordList, 1, 4)
    save_row_in_xlsx('textAnalysistestsaved.xlsx', countList, 2, 4)

def checkCellValue(workbookName,sheetName,rowNum,colNum):
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    cellval = sheet.cell(rowNum,colNum).value

    if cellval!=int or cellval!=float or cellval!=long:
        cellval=0

    print type(cellval)
    print int(cellval)
    print type(cellval)
    return cellval

def clear_cols_in_xlsx(loadExcel,startRowNum,rowMax):
    #Open an xlsx for reading
    wb = load_workbook(filename = loadExcel)
    #wb_read = load_workbook(filename = loadExcel,data_only=True)
    #Get the current Active Sheet
    #ws = wb.get_active_sheet()
    #You can also select a particular sheet
    #based on sheet name
    ws = wb.get_sheet_by_name("steve")
    #ws_read = wb_read.get_sheet_by_name("steve")
    #save the csb file
    # row_count = ws.max_row
    #max_row=ws_read.cell(row=2, column=3).value
    max_row = rowMax+30
    print max_row
    #max_row = int(0 if max_row is None else max_row)+30
    count = startRowNum
    while count< max_row:
        ws.cell(row=count, column=1).value = ''
        count+=1
    count = startRowNum
    while count< max_row:
        ws.cell(row=count, column=2).value = ''
        count+=1
    print 'replace complete'
    wb.save(loadExcel)

def moveSentiScores(excelName,sheetName):
    scoreList = read_column_in_excel_with_startRow(excelName, sheetName, 0,0)

    save_col_in_xlsx('textAnalysistestsaved.xlsx', scoreList, 26, 2)

def moveVerbatim(excelName,sheetName):
    verbatimList = read_column_in_excel_with_startRow(excelName, sheetName, 0,0)

    save_col_in_xlsx('textAnalysistestsaved.xlsx', verbatimList, 26, 1)

def check_exists_by_class(classname):
    try:
        browser.find_element_by_class_name(classname)
    except NoSuchElementException:
        return False
    return True

def is_tmall_next_page_clickable():
    button = browser.find_element_by_link_text('下一页>>')
    if button.is_enabled():
        return True
    else:
        return False

def close_popup_clickable():
    try:
        browser.find_element_by_class_name('sufei-tb-dialog-close').click()
        return True
    except:
        print "Element is not clickable"
        return False

def check_exists_by_partial_link_text(text):
    try:
        browser.find_element_by_partial_link_text(text)
    except NoSuchElementException:
        return False
    return True

def get_page_source_repustate(url):
    def get_url_edit():
        print 'starting t1'
        browser.get(url)
        actions.send_keys(Keys.END)
        actions.perform()
        time.sleep(2)
        print 'ending t1'
    t1 = threading.Thread(target=get_url_edit)
    t1.setDaemon(True)
    t1.start()
    def return_page_source():
        print 'starting t2'
        # wait10.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="J_TabBar"]/li[2]')))
        # review_button = browser.find_element_by_xpath('//*[@id="J_TabBar"]/li[2]')
        # review_button.click()
        # wait10.until(EC.visibility_of_element_located((By.CLASS_NAME, 'tm-rate-fulltxt')))
        element_to_hover_over = browser.find_element_by_xpath("//*[@id=\"id_language\"]")
        hover = ActionChains(browser).move_to_element(element_to_hover_over)
        hover.perform()
        time.sleep(5)
        # for chinese:
        sort_by_lang = browser.find_element_by_xpath('//*[@id="id_language"]/option[2]')
        # for english:
        # sort_by_lang = browser.find_element_by_xpath('//*[@id="id_language"]/option[4]')
        # for french:
        # sort_by_lang = browser.find_element_by_xpath('//*[@id="id_language"]/option[5]')
        sort_by_lang.click()
        time.sleep(5)
        # first_review = browser.find_element_by_class_name('tm-rate-fulltxt').text
        # print first_review
        print 'ending t2'
    t2 = Timer(timeout_time+5, return_page_source)
    t2.start()
    t2.join()
    #return browser.page_source

def get_page_source_tmall(url):
    def get_url_edit():
        print 'starting t1'
        browser.get(url)
        browser.execute_script("window.scrollTo(0, 500);")
        #actions.send_keys(Keys.END)
        #actions.perform()
        time.sleep(5)
        print 'ending t1'
    t1 = threading.Thread(target=get_url_edit)
    t1.setDaemon(True)
    t1.start()
    def return_page_source():
        print 'starting t2'
        wait10.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="J_TabBar"]/li[2]')))
        try:
            # review_button = browser.find_element_by_xpath('//*[@id="J_TabBar"]/li[3]')
            review_button = browser.find_element_by_partial_link_text("累计评价 ")
            review_button.click()
        except:
            print '2nd try to click reviews button'
            # review_button = browser.find_element_by_xpath('//*[@id="J_TabBar"]/li[2]')
            review_button = browser.find_element_by_xpath("//*[@id=\"J_TabBar\"]/li[2]/a")
            review_button.click()
            time.sleep(2)
            if check_exists_by_class('sufei-tb-dialog-close') and close_popup_clickable():
                # import winsound
                # duration = 1000  # millisecond
                # freq = 440  # Hz
                # winsound.Beep(freq, duration)
                print "pop up class detected!!!!!!!!!!!!"
                time.sleep(3)
                browser.find_element_by_class_name('sufei-tb-dialog-close').click()
                time.sleep(2 + random.uniform(3.5, 1.9))
        wait10.until(EC.visibility_of_element_located((By.CLASS_NAME, 'tm-rate-fulltxt')))
        time.sleep(10)
        # first_review = browser.find_element_by_class_name('tm-rate-fulltxt').text
        # print first_review
        print 'ending t2'
    t2 = Timer(timeout_time+10, return_page_source)
    t2.start()
    t2.join()
    #return browser.page_source

def get_page_source_taobao(url):
    def get_url_edit():
        print 'starting t1'
        browser.get(url)
        browser.execute_script("window.scrollTo(0, 100);")
        #actions.send_keys(Keys.END)
        #actions.perform()
        time.sleep(5)
        print 'ending t1'
    t1 = threading.Thread(target=get_url_edit)
    t1.setDaemon(True)
    t1.start()
    def return_page_source():
        print 'starting t2'

        wait10.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="J_TabBar"]/li[2]/a')))
        review_button = browser.find_element_by_xpath('//*[@id="J_TabBar"]/li[2]/a')
        review_button.click()
        time.sleep(5)

        while check_exists_by_class('sufei-dialog-close'):
            time.sleep(5)
            browser.find_element_by_class_name('sufei-dialog-close').click()
            browser.get(url)
            time.sleep(10)
            review_button = browser.find_element_by_xpath('//*[@id="J_TabBar"]/li[2]/a')
            review_button.click()
            time.sleep(5)

        wait10.until(EC.visibility_of_element_located((By.CLASS_NAME, 'tb-revbd')))
        # element_to_hover_over = browser.find_element_by_xpath("//*[@id=\"J_Reviews\"]/div/div[5]/span[2]/div/div/span")
        # hover = ActionChains(browser).move_to_element(element_to_hover_over)
        # hover.perform()
        # time.sleep(5)
        # sort_by_time = browser.find_element_by_xpath('//*[@id="J_Reviews"]/div/div[5]/span[2]/div/div/ul/li[2]')
        # sort_by_time.click()
        time.sleep(10)
        # first_review = browser.find_element_by_class_name('tm-rate-fulltxt').text
        # print first_review
        print 'ending t2'
    t2 = Timer(timeout_time+10, return_page_source)
    t2.start()
    t2.join()
    #return browser.page_source

def get_page_source_amazon(url):
    def get_url_edit():
        print 'starting t1'
        browser.get(url)
        actions.send_keys(Keys.END)
        actions.perform()
        time.sleep(5)
        print 'ending t1'
    t1 = threading.Thread(target=get_url_edit)
    t1.setDaemon(True)
    t1.start()
    def return_page_source():
        print 'starting t2'
        try:
            wait10.until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, 'See all reviews')))
            review_button = browser.find_element_by_partial_link_text('See all reviews')
            review_button.click()
        except:
            e = sys.exc_info()[0]
            print 'there are multiple see alls'
        try:
            review_button = browser.find_element_by_xpath('//*[@id="reviews-medley-footer"]/div[1]/a')
            review_button.click()
        except:
            e = sys.exc_info()[0]
            print 'data hook failed'
        time.sleep(5)
        print 'ending t2'
    t2 = Timer(timeout_time+10, return_page_source)
    t2.start()
    t2.join()

def get_page_source_JD(url):
    def get_url_edit():
        print 'starting t1'
        browser.get(url)
        actions.send_keys(Keys.END)
        actions.perform()
        time.sleep(5)
        print 'ending t1'
    t1 = threading.Thread(target=get_url_edit)
    t1.setDaemon(True)
    t1.start()
    def return_page_source():
        print 'starting t2'
        wait10.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="detail"]/div[1]/ul/li[5]')))
        review_button = browser.find_element_by_xpath('//*[@id="detail"]/div[1]/ul/li[5]')
        review_button.click()
        #wait10.until(EC.visibility_of_element_located((By.CLASS_NAME, 'p-comment')))
        # element_to_hover_over = browser.find_element_by_xpath('//*[@id="comment"]/div[2]/div[2]/div[1]/div/div/div[1]/span')
        # hover = ActionChains(browser).move_to_element(element_to_hover_over)
        # hover.perform()
        # time.sleep(5)
        # sort_by_time = browser.find_element_by_xpath('//*[@id="comment"]/div[2]/div[2]/div[1]/div/div/div[2]/ul/li[2]')
        # sort_by_time.click()
        element_to_hover_over2 = browser.find_element_by_xpath(
            '//*[@id="detail"]/div[1]/ul/li[5]')
        hover = ActionChains(browser).move_to_element(element_to_hover_over2)
        hover.perform()
        time.sleep(10)
        #first_review = browser.find_element_by_class_name('tm-rate-fulltxt').text
        #print first_review
        print 'ending t2'
    t2 = Timer(timeout_time+10, return_page_source)
    t2.start()
    t2.join()
    #return browser.page_source

def create_new_workbook(fileName, sheetName):
    book = xlwt.Workbook()
    sh = book.add_sheet(sheetName)
    book.save(fileName)

# previous parameters: tmall_urls,excelName,recipientEmail,title
# tier is now a string
def get_pictures(urlList, tier):
    def get_page_source_pictures(url):
        def get_url_edit():
            print 'starting t1'
            browser.get(url)
            browser.execute_script("window.scrollTo(0, 500);")
            #actions.send_keys(Keys.END)
            #actions.perform()
            time.sleep(5)
            print 'ending t1'
        t1 = threading.Thread(target=get_url_edit)
        t1.setDaemon(True)
        t1.start()
        def return_page_source():
            print 'starting t2'
            wait10.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="J_TabBar"]/li[2]')))
            try:
                # review_button = browser.find_element_by_xpath('//*[@id="J_TabBar"]/li[3]')
                review_button = browser.find_element_by_partial_link_text("累计评价 ")
                review_button.click()
                wait10.until(EC.visibility_of_element_located((By.CLASS_NAME, 'tm-rate-fulltxt')))
                element_to_hover_over = browser.find_element_by_css_selector('.rate-list-picture.rate-radio-group')
                hover = ActionChains(browser).move_to_element(element_to_hover_over)
                hover.perform()
                time.sleep(1)
                # show all pictures section in tmall
                show_all_pics = browser.find_element_by_css_selector('.rate-list-picture.rate-radio-group')
                show_all_pics.click()
            except:
                # nested try except again to catch if its not a tmall url, and send email to say no pictures will be sent
                try:
                    print '2nd try to click reviews button'
                    # review_button = browser.find_element_by_xpath('//*[@id="J_TabBar"]/li[2]')
                    review_button = browser.find_element_by_xpath("//*[@id=\"J_TabBar\"]/li[2]/a")
                    review_button.click()
                    wait10.until(EC.visibility_of_element_located((By.CLASS_NAME, 'tm-rate-fulltxt')))
                    element_to_hover_over = browser.find_element_by_css_selector('.rate-list-picture.rate-radio-group')
                    hover = ActionChains(browser).move_to_element(element_to_hover_over)
                    hover.perform()
                    time.sleep(1)
                    # show all pictures section in tmall
                    show_all_pics = browser.find_element_by_css_selector('.rate-list-picture.rate-radio-group')
                    show_all_pics.click()
                except:
                    e = sys.exc_info()[0]
            time.sleep(5)
            print 'ending t2'
        t2 = Timer(timeout_time+10, return_page_source)
        t2.start()
        t2.join()

    dictToAWS={}

    for each in urlList:
        get_page_source_pictures(each)
        for x in range(2,110,1):
            try:
                time.sleep(5)
                print x
                html = browser.page_source
                soup = BeautifulSoup(unicode(html), "html.parser")
                #grab all entry classes
                all = soup.findAll('td', attrs={'class': 'tm-col-master'})

                # for each entry
                for each in all:
                    # comm is the comments for each entry and can be len 1 or 2
                    comm = each.findAll('div', attrs={'class': 'tm-rate-fulltxt'})
                    # get text for each comment
                    comm_text=[x.get_text() for x in comm]
                    # join all comments
                    commJoint='***'.join(comm_text)
                    print commJoint
                    # all the picLinks in each entry
                    picLinks=each.findAll('li')
                    for each in picLinks:
                        # strip off outerhtml
                        picLinksStripped=strip_string(str(each), '<li data-src="', '"')
                        # store each piclink into dict with corresponding comment
                        dictToAWS[picLinksStripped]=commJoint

                for each in dictToAWS:
                    print each
                    print dictToAWS[each]
                print "number of pics: "+str(len(dictToAWS))

                attempts = 0
                while(attempts < 3):
                    try:
                        wait10.until(EC.visibility_of_element_located((By.LINK_TEXT, '下一页>>'))).click()
                        time.sleep(3)
                        break
                    except:
                        print "next attempt"
                        time.sleep(8)
                        e = sys.exc_info()[0]
                    attempts+=1
                if attempts==3:
                    print "ran out of attempts"
                    sys.exit()
                time.sleep(5+random.uniform(1.5,1.9))
            except:
                e = sys.exc_info()[0]
                print e
                break

    import xlwt

    create_new_workbook(tier+"pictures.xls","BlankSheet")

    # open existing workbook
    rb = xlrd.open_workbook(tier+"pictures.xls", formatting_info=True)
    # make a copy of it
    wb = xl_copy(rb)
    # add sheet to workbook with existing sheets
    Sheet1 = wb.add_sheet('pictureLinks')

    link_comments=0
    for key in dictToAWS:
        Sheet1.write(link_comments, 0, key)
        Sheet1.write(link_comments, 1, dictToAWS[key])
        link_comments += 1

    wb.save(tier+"pictures.xls")

def progressbar_update(key,number):
    # # Fetch the service account key JSON file contents
    # cred = credentials.Certificate('C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\serviceAccountKey.json')
    #
    # # Initialize the app with a service account, granting admin privileges
    # firebase_admin.initialize_app(cred, {
    #     'databaseURL': 'https://text-mining-458e9.firebaseio.com/'
    # })

    ref = db.reference('messages')
    entireJson = ref.get()
    print entireJson

    users_ref = ref.child(key)
    users_ref.update({
        'progress': str(number)
    })

def progressbar_update_aws(username,timestamp,progress,table):
    response = table.update_item(
        Key={
            'username' : username,
            'timestamp': timestamp
        },
        ExpressionAttributeNames={"#s": "status"},
        UpdateExpression="set #s = :r",
        ExpressionAttributeValues={
            ':r': progress
        },
        ReturnValues="UPDATED_NEW"
    )

def clean_each_comment(text,char):
    sep = char
    rest = text.split(sep, 1)[0]
    return rest

def strip_string(string,to_strip,char):
    if to_strip:
        while string.startswith(to_strip):
            string = string[len(to_strip):]
        while string.endswith(to_strip):
            string = string[:-len(to_strip)]
    string=clean_each_comment(string,char)
    return string

def job(urlList,tier):
    comments_history = []
    dates_each_comment = []
    skuList = []
    urlTrack = []

    for eachUrl in urlList:
        if "tmall" in eachUrl or eachUrl[14:17]=='com':
            get_page_source_tmall(eachUrl)
            countExcept=0
            for x in range(2,300,1):
                try:
                    print 'before check'
                    # check if next page clickable
                    if is_tmall_next_page_clickable():
                        # returns false if not clickable
                        print 'after check'
                        print "x is: " + str(x)
                        time.sleep(8)
                        html = browser.page_source
                        soup = BeautifulSoup(unicode(html), "html.parser")
                        all = soup.findAll('td', attrs={'class': 'tm-col-master'})
                        # first_review = browser.find_element_by_class_name('tm-rate-fulltxt').text
                        first_review = browser.find_element_by_class_name('tm-rate-content').text
                        first_date = browser.find_element_by_class_name('tm-rate-date').text
                        first_sku = browser.find_element_by_class_name('rate-sku').text
                        # print first_review
                        # print first_date
                        html = browser.page_source
                        soup = BeautifulSoup(unicode(html), "html.parser")
                        # name_box = soup.findAll('div', attrs={'class': 'tm-rate-content'})
                        all = soup.findAll('td', attrs={'class': 'tm-col-master'})
                        skus = soup.findAll('div', attrs={'class': 'rate-sku'})
                        # print all
                        for eachSku in skus:
                            skuList.append(eachSku.get_text())
                        for each in all:
                            # findAll method differs from find such that findall returns a list of objects and includes the tags inside. find method removes the tags
                            name_box = each.findAll('div', attrs={'class': 'tm-rate-content'})
                            date_box = each.findAll('div', attrs={'class': 'tm-rate-date'})
                            name = map(unicode, name_box)
                            date = map(unicode, date_box)

                            # print name
                            # print date
                            def remove_cruft(s):
                                return s[0][
                                       58:-12]  # [0] because list has only 1 element, and [0] is to select that element

                            def remove_cruft_string(y):
                                return y[58:-12]

                            def remove_cruft_date(x):
                                return x[0][26:-6]

                            def remove_cruft_date_string(z):
                                return z[26:-6]

                            def replace_line_breaks(a):
                                intermediate = a.replace("<b>", "")
                                return intermediate.replace("</b>", "")

                            if len(name) > 1:  # means that name is a list with more than 1 element
                                name_string = [remove_cruft_string(y) for y in name]
                                name_string_no_b = [replace_line_breaks(a) for a in name_string]
                                comments_history.append(name_string_no_b[1])
                                urlTrack.append(eachUrl)
                            else:  # means that name is a list with 1 element
                                name_string = remove_cruft(name)
                                name_string_no_b = replace_line_breaks(name_string)
                                comments_history.append(name_string_no_b)
                                urlTrack.append(eachUrl)

                            # print name_string_no_b

                            if len(date) > 1:  # means that date is a list with more than 1 element
                                date_string = [remove_cruft_date_string(z) for z in date]
                                date_string_no_b = [replace_line_breaks(a) for a in date_string]
                                dates_each_comment.append(date_string_no_b[1])
                            else:  # means that date is a list with 1 element
                                date_string = remove_cruft_date(date)
                                date_string_no_b = replace_line_breaks(date_string)
                                dates_each_comment.append(date_string)

                        try:
                            print "locating next page link"
                            print "clicking next page"
                            nextPage = browser.find_element_by_link_text('下一页>>')
                            actions = ActionChains(browser)
                            actions.move_to_element(nextPage).perform()
                            nextPage.click()
                            time.sleep(5)
                            # while straight after clicking next page, popup appears
                            # then close popup and click next page again
                            while check_exists_by_class('sufei-tb-dialog-close') and close_popup_clickable():
                                print "pop up class detected after clicking next!!!!!!!!!!!!"
                                nextPage = browser.find_element_by_link_text('下一页>>')
                                actions = ActionChains(browser)
                                actions.move_to_element(nextPage).perform()
                                nextPage.click()
                                time.sleep(2 + random.uniform(3.5, 1.9))
                        # once while loop exits, that means we got to the next page. back to for loop above to scrape next page


                        # 2 cases for except

                        ###CASE1###
                        # what if popup comes just before clicking next page? then it will go to except below
                        # first, close popup
                        # then, click next page
                        # then, check for another popup and do another while loop to click next page and close popup as long as popup appears
                        ###CASE2###
                        # it goes to except also if all the pages have finished
                        except:
                            ###CASE1###
                            if check_exists_by_class('sufei-tb-dialog-close') and close_popup_clickable():
                                print "pop up class detected just before clicking next!!!!!!!!!!!!"
                                time.sleep(2 + random.uniform(3.5, 1.9))
                                nextPage = browser.find_element_by_link_text('下一页>>')  # no click yet
                                nextPage.click()
                                while check_exists_by_class('sufei-tb-dialog-close') and close_popup_clickable():
                                    print "pop up class detected after clicking next!!!!!!!!!!!!"
                                    nextPage = browser.find_element_by_link_text('下一页>>')
                                    actions = ActionChains(browser)
                                    actions.move_to_element(nextPage).perform()
                                    nextPage.click()
                                    time.sleep(2 + random.uniform(3.5, 1.9))
                            # once while loop exits, means we got to the next page.
                            ###CASE2###
                            print"all pages done"
                            time.sleep(8)
                            e = sys.exc_info()[0]
                            print e
                            break
                    # if this else is reached, that means next page is not clickable due to 1) only 1 page of comments or 2) finished clicking
                    else:
                        # next page not clickable because : 1) pop up exists for first page or see below for number 2)
                        if check_exists_by_class('sufei-tb-dialog-close') and close_popup_clickable():
                            print "pop up class detected just before clicking next!!!!!!!!!!!!"
                            time.sleep(2 + random.uniform(3.5, 1.9))
                        # 2) this else is if the url has only 1 page of comments
                        else:
                            print 'url has only one page of comments'
                            print "x is: " + str(x)
                            time.sleep(8)
                            html = browser.page_source
                            soup = BeautifulSoup(unicode(html), "html.parser")
                            all = soup.findAll('td', attrs={'class': 'tm-col-master'})
                            # first_review = browser.find_element_by_class_name('tm-rate-fulltxt').text
                            first_review = browser.find_element_by_class_name('tm-rate-content').text
                            first_date = browser.find_element_by_class_name('tm-rate-date').text
                            first_sku = browser.find_element_by_class_name('rate-sku').text
                            # print first_review
                            # print first_date
                            html = browser.page_source
                            soup = BeautifulSoup(unicode(html), "html.parser")
                            # name_box = soup.findAll('div', attrs={'class': 'tm-rate-content'})
                            all = soup.findAll('td', attrs={'class': 'tm-col-master'})
                            skus = soup.findAll('div', attrs={'class': 'rate-sku'})
                            # print all
                            for eachSku in skus:
                                skuList.append(eachSku.get_text())
                            for each in all:
                                # findAll method differs from find such that findall returns a list of objects and includes the tags inside. find method removes the tags
                                name_box = each.findAll('div', attrs={'class': 'tm-rate-content'})
                                date_box = each.findAll('div', attrs={'class': 'tm-rate-date'})
                                name = map(unicode, name_box)
                                date = map(unicode, date_box)

                                # print name
                                # print date
                                def remove_cruft(s):
                                    return s[0][
                                           58:-12]  # [0] because list has only 1 element, and [0] is to select that element

                                def remove_cruft_string(y):
                                    return y[58:-12]

                                def remove_cruft_date(x):
                                    return x[0][26:-6]

                                def remove_cruft_date_string(z):
                                    return z[26:-6]

                                def replace_line_breaks(a):
                                    intermediate = a.replace("<b>", "")
                                    return intermediate.replace("</b>", "")

                                if len(name) > 1:  # means that name is a list with more than 1 element
                                    name_string = [remove_cruft_string(y) for y in name]
                                    name_string_no_b = [replace_line_breaks(a) for a in name_string]
                                    comments_history.append(name_string_no_b[1])
                                    urlTrack.append(eachUrl)
                                else:  # means that name is a list with 1 element
                                    name_string = remove_cruft(name)
                                    name_string_no_b = replace_line_breaks(name_string)
                                    comments_history.append(name_string_no_b)
                                    urlTrack.append(eachUrl)

                                # print name_string_no_b

                                if len(date) > 1:  # means that date is a list with more than 1 element
                                    date_string = [remove_cruft_date_string(z) for z in date]
                                    date_string_no_b = [replace_line_breaks(a) for a in date_string]
                                    dates_each_comment.append(date_string_no_b[1])
                                else:  # means that date is a list with 1 element
                                    date_string = remove_cruft_date(date)
                                    date_string_no_b = replace_line_breaks(date_string)
                                    dates_each_comment.append(date_string)
                            # break from for loop after scraping first page
                            break
                # will reach this except if elements on page not clickable, something wrong
                except:
                    countExcept += 1
                    # do a check for pop up anyway
                    if check_exists_by_class('sufei-tb-dialog-close') and close_popup_clickable():
                        print "pop up class detected just before clicking next!!!!!!!!!!!!"
                        time.sleep(2 + random.uniform(3.5, 1.9))
                    # try to get the comments again anyway
                    else:
                        try:
                            print 'url has only one page of comments'
                            print "x is: " + str(x)
                            time.sleep(8)
                            html = browser.page_source
                            soup = BeautifulSoup(unicode(html), "html.parser")
                            all = soup.findAll('td', attrs={'class': 'tm-col-master'})
                            # first_review = browser.find_element_by_class_name('tm-rate-fulltxt').text
                            first_review = browser.find_element_by_class_name('tm-rate-content').text
                            first_date = browser.find_element_by_class_name('tm-rate-date').text
                            first_sku = browser.find_element_by_class_name('rate-sku').text
                            # print first_review
                            # print first_date
                            html = browser.page_source
                            soup = BeautifulSoup(unicode(html), "html.parser")
                            # name_box = soup.findAll('div', attrs={'class': 'tm-rate-content'})
                            all = soup.findAll('td', attrs={'class': 'tm-col-master'})
                            skus = soup.findAll('div', attrs={'class': 'rate-sku'})
                            # print all
                            for eachSku in skus:
                                skuList.append(eachSku.get_text())
                            for each in all:
                                # findAll method differs from find such that findall returns a list of objects and includes the tags inside. find method removes the tags
                                name_box = each.findAll('div', attrs={'class': 'tm-rate-content'})
                                date_box = each.findAll('div', attrs={'class': 'tm-rate-date'})
                                name = map(unicode, name_box)
                                date = map(unicode, date_box)

                                # print name
                                # print date
                                def remove_cruft(s):
                                    return s[0][
                                           58:-12]  # [0] because list has only 1 element, and [0] is to select that element

                                def remove_cruft_string(y):
                                    return y[58:-12]

                                def remove_cruft_date(x):
                                    return x[0][26:-6]

                                def remove_cruft_date_string(z):
                                    return z[26:-6]

                                def replace_line_breaks(a):
                                    intermediate = a.replace("<b>", "")
                                    return intermediate.replace("</b>", "")

                                if len(name) > 1:  # means that name is a list with more than 1 element
                                    name_string = [remove_cruft_string(y) for y in name]
                                    name_string_no_b = [replace_line_breaks(a) for a in name_string]
                                    comments_history.append(name_string_no_b[1])
                                    urlTrack.append(eachUrl)
                                else:  # means that name is a list with 1 element
                                    name_string = remove_cruft(name)
                                    name_string_no_b = replace_line_breaks(name_string)
                                    comments_history.append(name_string_no_b)
                                    urlTrack.append(eachUrl)

                                # print name_string_no_b

                                if len(date) > 1:  # means that date is a list with more than 1 element
                                    date_string = [remove_cruft_date_string(z) for z in date]
                                    date_string_no_b = [replace_line_breaks(a) for a in date_string]
                                    dates_each_comment.append(date_string_no_b[1])
                                else:  # means that date is a list with 1 element
                                    date_string = remove_cruft_date(date)
                                    date_string_no_b = replace_line_breaks(date_string)
                                    dates_each_comment.append(date_string)
                        except:
                            # break from for loop after scraping first page
                            break
                    e = sys.exc_info()[0]
                    print e
                    # no break but it means will keep trying to click the next number and the next... until:
                    if countExcept == 5:
                        break  # give up after 5 tries
            time.sleep(8)

        elif "taobao" in eachUrl:
            get_page_source_taobao(eachUrl)
            first_review_list=[]
            for x in range(2, 150, 1):
                try:
                    print "x is: " + str(x)
                    time.sleep(8)
                    first_review = browser.find_element_by_class_name('review-details').text
                    print first_review
                    first_review_list.append(first_review)
                    if len(first_review_list) == 1 or first_review_list[-1] != first_review_list[-2]:
                        html = browser.page_source
                        soup = BeautifulSoup(unicode(html), "html.parser")
                        # name_box = soup.findAll('div', attrs={'class': 'tm-rate-content'})
                        all = soup.findAll('div', attrs={'class': 'tb-rev-item '})
                        skus = soup.findAll('div', attrs={'class': 'rate-sku'})
                        # print all
                        for eachSku in skus:
                            skuList.append(eachSku.get_text())
                        for each in all:
                            # findAll method differs from find such that findall returns a list of objects and includes the tags inside. find method removes the tags
                            name_box = each.findAll('div', attrs={'class': 'J_KgRate_ReviewContent tb-tbcr-content '})
                            date_box = each.findAll('span', attrs={'class': 'tb-r-date'})
                            name = map(unicode, name_box)
                            date = map(unicode, date_box)

                            def remove_cruft(s):
                                return s[0][
                                       58:-12]  # [0] because list has only 1 element, and [0] is to select that element
                            def remove_cruft_string(y):
                                return y[58:-12]

                            def remove_cruft_date(x):
                                return x[0][26:-6]

                            def remove_cruft_date_string(z):
                                return z[26:-6]

                            def replace_line_breaks(a):
                                intermediate = a.replace("<b>", "")
                                return intermediate.replace("</b>", "")

                            if len(name) > 1:  # means that name is a list with more than 1 element
                                name_string = [remove_cruft_string(y) for y in name]
                                name_string_no_b = [replace_line_breaks(a) for a in name_string]
                                comments_history.append(name_string_no_b[1])
                                urlTrack.append(eachUrl)
                            else:  # means that name is a list with 1 element
                                name_string = remove_cruft(name)
                                name_string_no_b = replace_line_breaks(name_string)
                                comments_history.append(name_string_no_b)
                                urlTrack.append(eachUrl)

                            # print name_string_no_b

                            if len(date) > 1:  # means that date is a list with more than 1 element
                                date_string = [remove_cruft_date_string(z) for z in date]
                                date_string_no_b = [replace_line_breaks(a) for a in date_string]
                                dates_each_comment.append(date_string_no_b[1])
                            else:  # means that date is a list with 1 element
                                date_string = remove_cruft_date(date)
                                date_string_no_b = replace_line_breaks(date_string)
                                dates_each_comment.append(date_string)

                        print str(len(first_review_list)) + ' is the len of first review list'

                        try:
                            print "locating next page link"
                            print "clicking next page"
                            nextPage = browser.find_element_by_class_name('pg-next')
                            actions = ActionChains(browser)
                            actions.move_to_element(nextPage).perform()
                            time.sleep(8)
                            # wait10.until(EC.visibility_of_element_located((By.LINK_TEXT, '下一页>>'))).click()
                            nextPage.click()
                            while check_exists_by_class('sufei-dialog-close'):
                                print 'pop up!!!!!!!!!'
                                time.sleep(3)
                                browser.find_element_by_class_name('sufei-dialog-close').click()
                            time.sleep(3)
                        except:
                            print "next attempt"
                            time.sleep(8)
                            e = sys.exc_info()[0]
                    else:
                        break
                except:
                    e = sys.exc_info()[0]
                    print e
                    break
            time.sleep(8)

        elif "amazon" in eachUrl:
            get_page_source_amazon(eachUrl)
            for x in range(2, 200, 1):
                try:
                    html = browser.page_source
                    soup = BeautifulSoup(html, "html.parser")
                    allComments = soup.findAll('span', attrs={'class': 'a-size-base review-text'})
                    allDatesIncludingFirstTwo = soup.findAll('span', attrs={'class': 'a-size-base a-color-secondary review-date'})
                    allDates = allDatesIncludingFirstTwo[2:]
                    all_string = []
                    for each in allComments:
                        eachstringUnfil1 = unicode(each)
                        eachstringUnfil2 = eachstringUnfil1.replace('<span class="a-size-base review-text" data-hook="review-body">', '')
                        eachstring = eachstringUnfil2.replace('</span>', '')
                        all_string.append(eachstring)
                        urlTrack.append(eachUrl)

                    comments_history.extend(all_string)

                    all_dates = []
                    for eachDate in allDates:
                        eachdatestringUnfil1 = unicode(eachDate)
                        eachdatestringUnfil2 = eachdatestringUnfil1.replace('<span class="a-size-base a-color-secondary review-date" data-hook="review-date">', '')
                        eachDateString = eachdatestringUnfil2.replace('</span>', '')
                        all_dates.append(eachDateString)
                    # print all_string
                    dates_each_comment.extend(all_dates)

                    print comments_history
                    print dates_each_comment

                    attempts = 0
                    while (attempts < 3):
                        try:
                            print "locating next page link"
                            print "clicking next page"
                            nextPage = browser.find_element_by_partial_link_text('Next')
                            actions = ActionChains(browser)
                            actions.move_to_element(nextPage).perform()
                            time.sleep(5)
                            nextPage.click()
                            time.sleep(3)
                            break
                        except:
                            print "next attempt"
                            time.sleep(8)
                            e = sys.exc_info()[0]
                        attempts += 1
                    if attempts == 3:
                        print "ran out of attempts"
                        sys.exit()

                    time.sleep(5)
                    time.sleep(2 + random.uniform(3.5, 1.9))
                except:
                    e = sys.exc_info()[0]
                    print e
                    break
                time.sleep(5)

        elif "jd" in eachUrl:
            get_page_source_JD(eachUrl)
            for x in range(1, 101, 1):
                try:
                    print x
                    time.sleep(5)
                    # first_review = browser.find_element_by_class_name('tm-rate-fulltxt').text
                    first_review = browser.find_element_by_xpath('//*[@id="comment-0"]/div[1]/div[2]/p').text
                    print first_review
                    first_date = browser.find_element_by_class_name('order-info').text
                    print first_date
                    first_rating = browser.find_element_by_xpath('//*[@id="comment-0"]/div[1]/div[2]').text
                    # print first_rating
                    html = browser.page_source
                    soup = BeautifulSoup(unicode(html), "html.parser")
                    # name_box = soup.findAll('div', attrs={'class': 'tm-rate-content'})
                    all = soup.findAll('div', attrs={'class': 'comment-item'})
                    for each in all:
                        # findAll method differs from find such that findall returns a list of objects and includes the tags inside. find method removes the tags
                        name_box = each.findAll('p', attrs={'class': 'comment-con'})
                        date_box = each.findAll('div', attrs={'class': 'order-info'})
                        rating_box = each.findAll('div', attrs={'class': 'comment-column J-comment-column'})
                        name = map(unicode, name_box) #name is a list
                        date = map(unicode, date_box)
                        rating = map(unicode, rating_box)
                        nameString=''.join(name) #turn the list into string
                        dateString=''.join(date)
                        print nameString
                        print dateString
                        print rating
                        nameStringFiltered1=nameString.replace('<p class="comment-con">','')
                        nameStringFiltered2=nameStringFiltered1.replace('</p>','')

                        dateStringFiltered1=dateString.replace('<div class="order-info">','')
                        dateStringFiltered2=dateStringFiltered1.replace('<span>','')
                        dateStringFiltered3=dateStringFiltered2.replace('</span>','')
                        dateStringFiltered4=dateStringFiltered3.replace('</div>','')

                        print nameStringFiltered2

                        comments_history.append(nameStringFiltered2)
                        dates_each_comment.append(dateStringFiltered4)
                    # comments_history is a list
                    # next_page_button = browser.find_element_by_xpath('//*[@id="J_Reviews"]/div/div[7]/div/a['+str(x)+']')
                    # next_page_button =wait10.until(EC.visibility_of_element_located((By.LINK_TEXT, str(x))))
                    attempts = 0
                    while (attempts < 3):
                        try:
                            wait10.until(EC.visibility_of_element_located((By.LINK_TEXT, '下一页'))).click()
                            time.sleep(3)
                            break
                        except:
                            print "next attempt"
                            time.sleep(8)
                            e = sys.exc_info()[0]
                        attempts += 1
                    if attempts == 3:
                        print "ran out of attempts"
                        sys.exit()
                    # next_page_button = browser.find_element_by_link_text(str(x))
                    # next_page_button.click()
                    time.sleep(10 + random.uniform(1.5, 1.9))
                except:
                    e = sys.exc_info()[0]
                    print e
                    break
            time.sleep(5)

    time.sleep(2)

    import xlwt

    def output(filename, sheet):
        book = xlwt.Workbook()
        sh = book.add_sheet(sheet)

        book.save(filename)

    def addSheet(filename, sheetName):
        # open existing workbook
        rb = xlrd.open_workbook(filename, formatting_info=True)
        # make a copy of it
        wb = xl_copy(rb)
        # add sheet to workbook with existing sheets
        Sheet1 = wb.add_sheet(sheetName)
        # save it!
        wb.save(filename)

    output(tier+".xls","frequencyList")

    # open existing workbook
    rb = xlrd.open_workbook(tier+".xls", formatting_info=True)
    # make a copy of it
    wb = xl_copy(rb)
    # add sheet to workbook with existing sheets
    Sheet1 = wb.add_sheet('yourData')

    row_comments=0
    for each in comments_history:
        each=clean_each_comment(each,'<')
        Sheet1.write(row_comments, 0, each)
        row_comments += 1

    row_dates = 0
    for each in dates_each_comment:
        Sheet1.write(row_dates, 1, each)
        row_dates += 1

    row_urls = 0
    for each in urlTrack:
        Sheet1.write(row_urls, 2, each)
        row_urls += 1

    row_skus = 0
    if skuList!=[]:
        for each in skuList:
            Sheet1.write(row_skus, 3, each)
            row_skus += 1

    wb.save(tier+".xls")

    chineseSegmenter.job(tier+".xls",'yourData')

    # addSheet(tier+".xls","sentiScores")

    # sentiGen.job(tier+".xls",'yourData',get_page_source_repustate,browser)

    # googleNL.job(tier+".xls",'yourData')

    # progressbar_update_aws(username, int(excelName), 100, table)