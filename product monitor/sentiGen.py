# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import time
from selenium import webdriver
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import xlwt
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from time import sleep
import threading
from threading import Thread
from threading import Timer
from selenium.webdriver import ActionChains
import xlrd
from xlutils.copy import copy as xl_copy
import sys
import random

# path_to_chromedriver = 'C:\Users\Public\chromedriver.exe'
# # browser = webdriver.Chrome(executable_path = path_to_chromedriver)
# browser = webdriver.Chrome('C:\Users\Public\chromedriver.exe')
# # browser.get('chrome://settings/advanced')
# # browser.find_element_by_id('privacyContentSettingsButton').click()
# # browser.find_element_by_name('popups').click()

# browser.set_page_load_timeout(timeout_time)
# actions=ActionChains(browser)

def save_column_in_excel(fileName,sheetNumber,columnNumber,columnList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=0
    for each in columnList:
        Sheet1.write(link_comments, columnNumber, each)
        link_comments += 1
    wb.save(fileName)

# def get_page_source_repustate(url):
#     def get_url_edit():
#         print 'starting t1'
#         browser.get(url)
#         actions.send_keys(Keys.END)
#         actions.perform()
#         time.sleep(2)
#         print 'ending t1'
#     t1 = threading.Thread(target=get_url_edit)
#     t1.setDaemon(True)
#     t1.start()
#     def return_page_source():
#         print 'starting t2'
#         # wait10.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="J_TabBar"]/li[2]')))
#         # review_button = browser.find_element_by_xpath('//*[@id="J_TabBar"]/li[2]')
#         # review_button.click()
#         # wait10.until(EC.visibility_of_element_located((By.CLASS_NAME, 'tm-rate-fulltxt')))
#         element_to_hover_over = browser.find_element_by_xpath("//*[@id=\"id_language\"]")
#         hover = ActionChains(browser).move_to_element(element_to_hover_over)
#         hover.perform()
#         time.sleep(5)
#         # for chinese:
#         sort_by_lang = browser.find_element_by_xpath('//*[@id="id_language"]/option[2]')
#         # for english:
#         # sort_by_lang = browser.find_element_by_xpath('//*[@id="id_language"]/option[4]')
#         # for french:
#         # sort_by_lang = browser.find_element_by_xpath('//*[@id="id_language"]/option[5]')
#         sort_by_lang.click()
#         time.sleep(5)
#         # first_review = browser.find_element_by_class_name('tm-rate-fulltxt').text
#         # print first_review
#         print 'ending t2'
#     t2 = Timer(timeout_time+1, return_page_source)
#     t2.start()
#     t2.join()
#     #return browser.page_source

def job(excelName,sheetName,get_page_source_repustate,browser):
    timeout_time = 20
    repustate = 'https://www.repustate.com/api-demo/'
    list_of_score = []
    list_of_comments = []
    rb = xlrd.open_workbook(excelName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read = 0
    while row_read < row_count:
        each = sheet.cell(row_read, 0).value
        list_of_comments.append(each)
        row_read += 1
    tmall_urls = [repustate]
    for each in tmall_urls:
        get_page_source_repustate(each)
        for x in range(0,len(list_of_comments),1):
            try:
                clickonit = browser.find_element_by_xpath('//*[@id=\"id_text\"]')
                clickonit.click()
                # inputElement = browser.find_element_by_xpath("//*[@id=\"id_text\"]")
                # inputElement.send_keys('haha')

                browser.find_element_by_xpath('//*[@id=\"id_text\"]').clear()

                i = list_of_comments[x]
                #print list_of_comments[x]
                try:
                    browser.find_element_by_xpath("//*[@id=\"id_text\"]").send_keys(i)
                except:
                    browser.find_element_by_xpath("//*[@id=\"id_text\"]").send_keys('nil')

                time.sleep(3)

                attempt = 0
                while attempt < 3:
                    try:
                        clickonit2 = browser.find_element_by_xpath('//*[@id="container"]/div[1]/div[2]/div/div/form/div[2]/button')
                        clickonit2.click()
                        break
                    except:
                        print "trying again"
                        attempt += 1
                        e = sys.exc_info()[0]
                        print e

                time.sleep(3)

                score = browser.find_element_by_xpath('//*[@id="container"]/div[1]/div[2]/div/div/form/table/tbody/tr/td[4]/strong').text
                print score
                list_of_score.append(score)

            except:
                e = sys.exc_info()[0]
                print e
                #refresh and try once more
                time.sleep(10)
                try:
                    browser.refresh()
                    time.sleep(8)
                    clickonit = browser.find_element_by_xpath('//*[@id=\"id_text\"]')
                    clickonit.click()
                    # inputElement = browser.find_element_by_xpath("//*[@id=\"id_text\"]")
                    # inputElement.send_keys('haha')

                    browser.find_element_by_xpath('//*[@id=\"id_text\"]').clear()

                    i = list_of_comments[x]
                    # print list_of_comments[x]
                    try:
                        browser.find_element_by_xpath("//*[@id=\"id_text\"]").send_keys(i)
                    except:
                        browser.find_element_by_xpath("//*[@id=\"id_text\"]").send_keys('nil')

                    time.sleep(3)

                    clickonit2 = browser.find_element_by_xpath(
                        '//*[@id="container"]/div[1]/div[2]/div/div/form/div[2]/button')
                    clickonit2.click()

                    time.sleep(3)

                    score = browser.find_element_by_xpath(
                        '//*[@id="container"]/div[1]/div[2]/div/div/form/table/tbody/tr/td[4]/strong').text
                    print score
                    list_of_score.append(score)

                except:
                    e = sys.exc_info()[0]
                    print e
                    break

        print list_of_score
        save_column_in_excel(excelName, 2, 0, list_of_score)
        time.sleep(5)

