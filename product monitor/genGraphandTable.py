# coding: utf-8

from openpyxl import load_workbook
import xlwt
import xlrd
from decimal import Decimal
import base64

def plotDoubleBar(excelName):
    import plotly
    import plotly.graph_objs as go

    image_filename = 'C:\\Users\\ZL\\Desktop\\For a better life\\wgt interactive\\logo_link.png'  # replace with your own image
    encoded_image = base64.b64encode(open(image_filename, 'rb').read())

    def read_column_in_excel(workbookName, sheetName, columnNumber):
        columnList = []
        rb = xlrd.open_workbook(workbookName)
        sheet = rb.sheet_by_name(sheetName)
        row_count = len(sheet.col_values(0))
        row_read = 0
        while row_read < row_count:
            each = sheet.cell(row_read, columnNumber).value
            columnList.append(each)
            row_read += 1
        # return list of elements in column
        return columnList

    def read_column_in_excel_limited(workbookName, sheetName, columnNumber, rowMax):
        columnList = []
        rb = xlrd.open_workbook(workbookName)
        sheet = rb.sheet_by_name(sheetName)
        row_count = rowMax
        row_read = 0
        while row_read < row_count:
            each = sheet.cell(row_read, columnNumber).value
            columnList.append(each)
            row_read += 1
        # return list of elements in column
        return columnList

    wordList = read_column_in_excel_limited(excelName, 'frequencyList', 0, 10)
    countList = read_column_in_excel_limited(excelName, 'frequencyList', 1, 10)
    sentiList = read_column_in_excel(excelName, 'sentiScores', 0)
    verbatimList = read_column_in_excel(excelName, 'yourData', 0)

    xList = wordList

    def genlistOfTenLists(wordList, verbatimList, sentiList):
        listOfTenLists = []
        for eachWord in wordList:
            countVerbatim = 0
            eachWordList = []
            for eachVerbatim in verbatimList:
                if eachWord in eachVerbatim:
                    eachWordList.append(sentiList[countVerbatim])
                else:
                    eachWordList.append('')
                countVerbatim += 1
            listOfTenLists.append(eachWordList)

        return listOfTenLists

    listofTenLists = genlistOfTenLists(wordList, verbatimList, sentiList)

    def findPercentageList(countList, verbatimList):
        percentageList = []
        for eachcount in countList:
            eachPercentage = round((float(eachcount) / len(verbatimList)) * 100, 2)
            percentageList.append(eachPercentage)

        return percentageList

    yList1 = findPercentageList(countList, verbatimList)

    def findCulmulativeList(percentageList):
        culmulativeList=[]
        count=0
        for eachPer in percentageList:
            culmulativeList.append(sum(percentageList[:count])+eachPer)
            count+=1

        return culmulativeList

    culmulativePercent=findCulmulativeList(yList1)

    def acculList(percentageList):
        outputList = [''] * len(percentageList)
        finalElement = sum(percentageList)
        outputList[-1] = str(finalElement) + '%'

        return outputList

    scatterText = acculList(yList1)

    def listOfNegPercentage(listofTenLists):
        listOfNeg = []
        for eachList in listofTenLists:
            countNum = 0.0
            countNeg = 0.0
            for eachval in eachList:
                if eachval != '':
                    countNum += 1
                    eachvalconvert = float(eachval)
                    if eachvalconvert < 0.0:
                        countNeg += 1
            negPercent = round((countNeg / countNum) * 100, 0)
            listOfNeg.append(negPercent)

        return listOfNeg

    yList2 = listOfNegPercentage(listofTenLists)

    yList1Width=[0.5] * len(yList1)
    ylist2Width=[0.1] * len(yList2)

    trace1 = go.Bar(
        x=xList,
        y=yList1,
        text=yList1,
        textposition='auto',
        width = yList1Width,
        name='Percentage of Mentions'
    )
    trace2 = go.Scatter(
        x=xList,
        y=culmulativePercent,
        mode='lines+markers+text',
        name='Cumulative Percentage',
        text=scatterText,
        textposition='bottom'
    )
    trace3 = go.Bar(
        x=xList,
        y=yList2,
        text=yList2,
        textposition='outside',
        width = ylist2Width,
        name='Percentage of Negative',
        # yaxis='y2',
        marker=dict(color='rgb(255, 0, 0)',)
    )

    data = [trace1, trace2,trace3]
    layout = go.Layout(
        images=[dict(
            source='data:image/png;base64,{}'.format(encoded_image),
            # source="https://wheregottext.com/img/logo_link.png",
            # source="https://images.plot.ly/language-icons/api-home/python-logo.png",
            xref="x",
            yref="y",
            x=4.5,
            y=80,
            sizex=10,
            sizey=60,
            sizing="contain",
            opacity=0.1,
            xanchor="center",
            yanchor="center",
            layer="above")],
        # autosize=True,
        barmode='grouped',
        legend=dict(orientation="h"),
        title='Top 10 Terms',
        font=dict(family='Helvetica Neue, Helvetica', size=20, color='#000000'),
        yaxis=dict(
            title='%',
            autorange=True,
        )
    )
    fig = go.Figure(data=data, layout=layout)
    plot_url = plotly.offline.plot(fig, filename='freqOfMentions',auto_open=False)

def drawPosTable(workbookName):
    import plotly
    import plotly.graph_objs as go

    def read_column_in_excel(workbookName, sheetName, columnNumber):
        columnList = []
        rb = xlrd.open_workbook(workbookName)
        sheet = rb.sheet_by_name(sheetName)
        row_count = len(sheet.col_values(0))
        row_read = 0
        while row_read < row_count:
            each = sheet.cell(row_read, columnNumber).value
            columnList.append(each)
            row_read += 1
        # return list of elements in column
        return columnList

    def read_column_in_excel_limited(workbookName, sheetName, columnNumber, rowMax):
        columnList = []
        rb = xlrd.open_workbook(workbookName)
        sheet = rb.sheet_by_name(sheetName)
        row_count = rowMax
        row_read = 0
        while row_read < row_count:
            each = sheet.cell(row_read, columnNumber).value
            columnList.append(each)
            row_read += 1
        # return list of elements in column
        return columnList

    wordList = read_column_in_excel_limited(workbookName, 'frequencyList', 0, 10)
    verbatimList = read_column_in_excel(workbookName, 'yourData', 0)
    sentiList = read_column_in_excel(workbookName, 'sentiScores', 0)

    def genlistOfTenLists(wordList, verbatimList, sentiList):
        listOfTenLists = []
        for eachWord in wordList:
            countVerbatim = 0
            eachWordList = []
            for eachVerbatim in verbatimList:
                if eachWord in eachVerbatim:
                    eachWordList.append(sentiList[countVerbatim])
                else:
                    eachWordList.append('')
                countVerbatim += 1
            listOfTenLists.append(eachWordList)

        return listOfTenLists

    listofTenLists = genlistOfTenLists(wordList, verbatimList, sentiList)

    print wordList

    newWordList = []

    for each in wordList:
        count = 1
        while count<4:
            newEach=each+str(count)
            newWordList.append(newEach)
            count+=1

    print newWordList

    def top3senti(verbatimList, listofTenLists):
        listOfTop3SentiLists = []
        listofUsedPositions = []
        for eachList in listofTenLists:
            # making sure all numerical chars are float in eachList
            newEachList = []
            for eachVal in eachList:
                if eachVal != '':
                    newEachList.append(float(eachVal))
                else:
                    newEachList.append('')
            # create a list variable to limit 3 appends to used positions
            limit3 = []
            # list to store top 3 verbatim for each list
            top3SentiList = []
            # m is max value for eachList
            # seems lik max always picks out the '' blanks
            m = max([i for i in newEachList if i != ''])
            # this is a list to store the positions where the maxvalue is found
            maxPositions = [i for i, j in enumerate(newEachList) if j == m]
            print 'maxPos ' + str(maxPositions)
            # there is a chance that there are 3 or more 0.95 values
            if len(maxPositions) > 2:
                for eachPos in maxPositions:
                    if eachPos not in listofUsedPositions:
                        if len(limit3) < 3:
                            top3SentiList.append(verbatimList[eachPos])
                            # filter to top 3 verbatim
                            top3SentiList = top3SentiList[:3]
                            listofUsedPositions.append(eachPos)
                            limit3.append('count')
                # second chance to add more into top3sentilist
                if len(limit3) < 3:
                    listOfmoreValues = [i for i in newEachList if 0.1 < i < 0.95]
                    maxPositionsExtend = [i for i, j in enumerate(newEachList) if j in listOfmoreValues]
                    print 'extended maxPos ' + str(maxPositionsExtend)
                    for eachPos in maxPositionsExtend:
                        if eachPos not in listofUsedPositions:
                            top3SentiList.append(verbatimList[eachPos])
                            # filter to top 3 verbatim
                            top3SentiList = top3SentiList[:3]
                            listofUsedPositions.append(eachPos)
                            limit3.append('count')
                        if len(limit3) == 3:
                            break
                # now fill up the blanks with 'check others for overlap'
                while len(limit3) < 3:
                    top3SentiList.append('check other terms for overlap')
                    listofUsedPositions.append('**')
                    limit3.append('count')


            # what if there are less than 3 0.95 values
            else:
                # finding senti values betwen 0.7 and 1
                listOfmoreValues = [i for i in newEachList if 0.1 < i < 1]
                # finding positions that have senti in that range
                maxPositionsExtend = [i for i, j in enumerate(newEachList) if j in listOfmoreValues]
                print 'extended maxPos ' + str(maxPositionsExtend)
                for eachPos in maxPositionsExtend:
                    if eachPos not in listofUsedPositions:
                        if len(limit3) < 3:
                            top3SentiList.append(verbatimList[eachPos])
                            # filter to top 3 verbatim
                            top3SentiList = top3SentiList[:3]
                            listofUsedPositions.append(eachPos)
                            limit3.append('count')
                while len(limit3) < 3:
                    top3SentiList.append('check other terms for overlap')
                    listofUsedPositions.append('**')
                    limit3.append('count')
            print 'usedPos ' + str(listofUsedPositions)
            listOfTop3SentiLists.append(top3SentiList)
            print 'len of listoftop3sentilist ' + str(len(listOfTop3SentiLists))
        return listOfTop3SentiLists

    intermediateList = top3senti(verbatimList, listofTenLists)

    newVerbatimList=[]
    for eachlist in intermediateList:
        for eachcomment in eachlist:
            newVerbatimList.append(eachcomment)

    values = [newWordList,
              newVerbatimList, #verbatimListNeg
              ]

    trace0 = go.Table(
        type='table',
        columnorder=[1, 2],
        columnwidth=[20, 200],
        header=dict(
            values=[['<b>Keywords</b>'],
                    ['<b>POSITIVE</b>'],
                    #['<b>NEGATIVE</b>']
                    ],
            line=dict(color='#506784'),
            fill=dict(color=['#119DFF','green','red']),
            align=['center'],
            font=dict(color='white', size=9),
            height=15
        ),
        cells=dict(
            values=values,
            line=dict(color='#506784'),
            fill=dict(color=['rgb(239,243,255)', 'white']),
            align=['center','left'],
            font=dict(color='#506784', size=12),
            height=30
        ))

    data = [trace0]

    layout = go.Layout(
        title='use ctrl and +/- keys to enlarge/shrink table',
        font=dict(family='Helvetica Neue, Helvetica', size=15, color='#000000')
        )

    fig = go.Figure(data=data, layout=layout)

    plot_url = plotly.offline.plot(fig, filename="Positives",auto_open=False)

def drawNegTable(workbookName):
    import plotly
    import plotly.graph_objs as go

    def read_column_in_excel(workbookName, sheetName, columnNumber):
        columnList = []
        rb = xlrd.open_workbook(workbookName)
        sheet = rb.sheet_by_name(sheetName)
        row_count = len(sheet.col_values(0))
        row_read = 0
        while row_read < row_count:
            each = sheet.cell(row_read, columnNumber).value
            columnList.append(each)
            row_read += 1
        # return list of elements in column
        return columnList

    def read_column_in_excel_limited(workbookName, sheetName, columnNumber, rowMax):
        columnList = []
        rb = xlrd.open_workbook(workbookName)
        sheet = rb.sheet_by_name(sheetName)
        row_count = rowMax
        row_read = 0
        while row_read < row_count:
            each = sheet.cell(row_read, columnNumber).value
            columnList.append(each)
            row_read += 1
        # return list of elements in column
        return columnList

    wordList = read_column_in_excel_limited(workbookName, 'frequencyList', 0, 10)
    verbatimList = read_column_in_excel(workbookName, 'yourData', 0)
    sentiList = read_column_in_excel(workbookName, 'sentiScores', 0)

    def genlistOfTenLists(wordList, verbatimList, sentiList):
        listOfTenLists = []
        for eachWord in wordList:
            countVerbatim = 0
            eachWordList = []
            for eachVerbatim in verbatimList:
                if eachWord in eachVerbatim:
                    eachWordList.append(sentiList[countVerbatim])
                else:
                    eachWordList.append('')
                countVerbatim += 1
            listOfTenLists.append(eachWordList)

        return listOfTenLists

    listofTenLists = genlistOfTenLists(wordList, verbatimList, sentiList)
    print wordList

    newWordList = []

    for each in wordList:
        count = 1
        while count<4:
            newEach=each+str(count)
            newWordList.append(newEach)
            count+=1

    print newWordList

    def btm3senti(verbatimList, listofTenLists):
        listOfTop3SentiLists = []
        listofUsedPositions = []
        for eachList in listofTenLists:
            # making sure all numerical chars are float in eachList
            newEachList = []
            for eachVal in eachList:
                if eachVal != '':
                    newEachList.append(float(eachVal))
                else:
                    newEachList.append('')
            # create a list variable to limit 3 appends to used positions
            limit3 = []
            # list to store top 3 verbatim for each list
            top3SentiList = []
            # m is max value for eachList
            # seems lik max always picks out the '' blanks
            m = min([i for i in newEachList if i != ''])
            # this is a list to store the positions where the maxvalue is found
            maxPositions = [i for i, j in enumerate(newEachList) if j == m]
            print 'minPos ' + str(maxPositions)
            # there is a chance that there are 3 or more 0.95 values
            if len(maxPositions) > 2:
                for eachPos in maxPositions:
                    if eachPos not in listofUsedPositions:
                        if len(limit3) < 3:
                            top3SentiList.append(verbatimList[eachPos])
                            # filter to top 3 verbatim
                            top3SentiList = top3SentiList[:3]
                            listofUsedPositions.append(eachPos)
                            limit3.append('count')
                # second chance to add more into top3sentilist
                if len(limit3) < 3:
                    listOfmoreValues = [i for i in newEachList if -0.95 < i < 0]
                    maxPositionsExtend = [i for i, j in enumerate(newEachList) if j in listOfmoreValues]
                    print 'extended minPos ' + str(maxPositionsExtend)
                    for eachPos in maxPositionsExtend:
                        if eachPos not in listofUsedPositions:
                            top3SentiList.append(verbatimList[eachPos])
                            # filter to top 3 verbatim
                            top3SentiList = top3SentiList[:3]
                            listofUsedPositions.append(eachPos)
                            limit3.append('count')
                        if len(limit3) == 3:
                            break
                # now fill up the blanks with 'check others for overlap'
                while len(limit3) < 3:
                    top3SentiList.append('check other terms for overlap/ no negatives')
                    listofUsedPositions.append('**')
                    limit3.append('count')


            # what if there are less than 3 0.95 values
            else:
                # finding senti values betwen 0.7 and 1
                listOfmoreValues = [i for i in newEachList if -1.1 < i < 0]
                # finding positions that have senti in that range
                maxPositionsExtend = [i for i, j in enumerate(newEachList) if j in listOfmoreValues]
                print 'extended minPos ' + str(maxPositionsExtend)
                for eachPos in maxPositionsExtend:
                    if eachPos not in listofUsedPositions:
                        if len(limit3) < 3:
                            top3SentiList.append(verbatimList[eachPos])
                            # filter to top 3 verbatim
                            top3SentiList = top3SentiList[:3]
                            listofUsedPositions.append(eachPos)
                            limit3.append('count')
                while len(limit3) < 3:
                    top3SentiList.append('check other terms for overlap/ no negatives')
                    listofUsedPositions.append('**')
                    limit3.append('count')
            print 'usedPos ' + str(listofUsedPositions)
            listOfTop3SentiLists.append(top3SentiList)
            print 'len of listoftop3sentilist ' + str(len(listOfTop3SentiLists))
        return listOfTop3SentiLists

    intermediateList = btm3senti(verbatimList, listofTenLists)

    newVerbatimList = []
    for eachlist in intermediateList:
        for eachcomment in eachlist:
            newVerbatimList.append(eachcomment)

    values = [newWordList,
              newVerbatimList, #verbatimListNeg
              ]

    trace0 = go.Table(
        type='table',
        columnorder=[1, 2],
        columnwidth=[20, 200],
        header=dict(
            values=[['<b>Keywords</b>'],
                    ['<b>Negative</b>'],
                    #['<b>NEGATIVE</b>']
                    ],
            line=dict(color='#506784'),
            fill=dict(color=['#119DFF','red']),
            align=['center'],
            font=dict(color='white', size=9),
            height=15
        ),
        cells=dict(
            values=values,
            line=dict(color='#506784'),
            fill=dict(color=['rgb(239,243,255)', 'white']),
            align=['center','left'],
            font=dict(color='#506784', size=12),
            height=30
        ))

    data = [trace0]

    layout = go.Layout(
        title='use ctrl and +/- keys to enlarge/shrink table',
        font=dict(family='Helvetica Neue, Helvetica', size=15, color='#000000')
        )

    fig = go.Figure(data=data, layout=layout)

    plot_url = plotly.offline.plot(fig, filename="Negatives",auto_open=False)

def job(excelName):
    plotDoubleBar(excelName)
    drawPosTable(excelName)
    drawNegTable(excelName)
