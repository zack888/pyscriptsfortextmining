import os
import time
from multiprocessing import Process, current_process
import Polling

def executePolling(tier):
    proc_name = current_process().name
    Polling.main(tier)
    print('{0} handled by {1}'.format(
        tier, proc_name))


if __name__ == '__main__':
    numbers = [1,2,3]
    procs = []
    # proc = Process(target=doubler, args=(5,))

    for index, number in enumerate(numbers):
        proc = Process(target=executePolling, args=(number,))
        procs.append(proc)
        proc.start()
        time.sleep(10)
        # this will make the processes run by turn
        proc.join()

    # this is just so that we wait for all proc to end before quitting the script
    # for proc in procs:
    #     proc.join()

    # this is just to show how to start 1 process and name it
    # proc = Process(target=doubler, name='Test', args=(2,))
    # proc.start()

    # procs.append(proc)

