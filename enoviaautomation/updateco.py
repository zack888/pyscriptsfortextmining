# -*- coding: utf-8 -*-

import time
from selenium import webdriver
from selenium.webdriver import ActionChains
import xlrd
from xlutils.copy import copy as xl_copy
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

path_to_chromedriver = 'C:\Users\Public\chromedriver.exe'
browser = webdriver.Chrome('C:\Users\Public\chromedriver.exe')
# path_to_chromedriver = 'C:\\Users\\tan.z.1\\Desktop\\geckodriver.exe'
# browser = webdriver.Firefox(executable_path=path_to_chromedriver)

def read_column_in_excel(workbookName,sheetName,columnNumber,numOfRows=None):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    if numOfRows is None:
        row_count = len(sheet.col_values(0))
    else:
        row_count = numOfRows
    row_read=0
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def save_list_in_excel(fileName,sheetNumber,columnNumber,inputList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=0
    for each in inputList:
        Sheet1.write(link_comments, columnNumber, str(each))
        link_comments += 1
    wb.save(fileName)

def check_exists_by_xpath(xpath):
    try:
        browser.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True

def get_Enovia(url):
    print 'browsing to Enovia...'
    browser.get(url)
    # browser.execute_script("window.scrollTo(0, 500);")
    time.sleep(5)

    print 'Welcome to Enovia'
    browser.switch_to.default_content()

def sendKeysAndLogIn(userName,password):
    usernameField = browser.find_element_by_xpath("/html/body/div/form/fieldset/div[3]/label/input")
    # usernameField.clear()
    usernameField.click()
    usernameField.send_keys(userName)

    passwordField = browser.find_element_by_xpath("/html/body/div/form/fieldset/div[4]/label/input")
    # passwordField.clear()
    passwordField.click()
    passwordField.send_keys(password)

    time.sleep(3)

    logIn = browser.find_element_by_xpath('/html/body/div/form/div/input')
    logIn.click()

def scroll_element_into_view(linkTextInput):
    try:
        print 'checking if item is in view'
        scrollTo = browser.find_element_by_link_text(linkTextInput)
        actions = ActionChains(browser)
        actions.move_to_element(scrollTo).perform()
        return True
    except:
        return False

def switchToIframeBOM():
    print 'switching to iframe'
    # switch to first iframe
    iframe = browser.find_element_by_xpath('//*[@id="content"]')
    browser.switch_to.frame(iframe)

    time.sleep(3)

    print 'switching to second iframe'
    # switch to second iframe
    iframe2 = browser.find_element_by_xpath('//*[@id="unique1"]/iframe')
    browser.switch_to.frame(iframe2)

    time.sleep(3)

    print 'switching to third iframe'
    # switch to second iframe
    iframe2 = browser.find_element_by_xpath('//*[@id="divPageBody"]/iframe')
    browser.switch_to.frame(iframe2)

    print 'switching to fourth iframe'
    # switch to second iframe
    iframe2 = browser.find_element_by_xpath('//*[@id="divPvChannel-1-1"]/div[2]/iframe')
    browser.switch_to.frame(iframe2)

def switchToIframeCM():
    print 'switching to iframe'
    # switch to first iframe
    iframe = browser.find_element_by_xpath('//*[@id="content"]')
    browser.switch_to.frame(iframe)

    time.sleep(3)

    print 'switching to second iframe'
    # switch to second iframe
    iframe2 = browser.find_element_by_xpath('//*[@id="unique1"]/iframe')
    browser.switch_to.frame(iframe2)

    time.sleep(3)

    print 'switching to third iframe'
    # switch to second iframe
    iframe2 = browser.find_element_by_xpath('//*[@id="divPageBody"]/iframe')
    browser.switch_to.frame(iframe2)

    print 'switching to fourth iframe'
    # switch to second iframe
    iframe2 = browser.find_element_by_xpath('//*[@id="divPvChannel-1-1"]/div[2]/iframe')
    browser.switch_to.frame(iframe2)

def switchToIframeReleased():
    print 'switching to iframe'
    # switch to first iframe
    iframe = browser.find_element_by_xpath('//*[@id="windowShadeFrame"]')
    browser.switch_to.frame(iframe)

    time.sleep(5)

    print 'switching to second iframe'
    # switch to second iframe
    iframe2 = browser.find_element_by_xpath('//*[@id="structure_browser"]')
    browser.switch_to.frame(iframe2)

    print 'finding released spec...'

def switchToIframePopup():
    print 'switching to iframe'
    # switch to first iframe
    iframe = browser.find_element_by_xpath('//*[@id="content"]')
    browser.switch_to.frame(iframe)

    time.sleep(2)

    print 'switching to second iframe'
    # switch to second iframe
    iframe2 = browser.find_element_by_xpath('//*[@id="unique0"]/iframe')
    browser.switch_to.frame(iframe2)

    time.sleep(2)

    print 'switching to third iframe'
    # switch to second iframe
    iframe2 = browser.find_element_by_xpath('//*[@id="divPageBody"]/iframe')
    browser.switch_to.frame(iframe2)

    time.sleep(2)

    print 'switching to fourth iframe'
    # switch to second iframe
    iframe2 = browser.find_element_by_xpath('//*[@id="divPvChannel-1-1"]/div[2]/iframe')
    browser.switch_to.frame(iframe2)

def job():

    fpcList = read_column_in_excel('updateCP.xls','Sheet1',0)

    print fpcList
    print len(fpcList)

    get_Enovia('https://plmprod.pg.com/enovia/common/emxSecurityContextSelection.jsp')

    userName = raw_input("Type in your Username\n")

    password = raw_input("Type in your Password\n")

    sendKeysAndLogIn(userName,password)

    print "logging in at Enovia Speed.............Count to 10"

    time.sleep(10)

    clickOK=browser.find_element_by_xpath('//*[@id="submitButton"]')
    clickOK.click()

    time.sleep(10)

    searchField = browser.find_element_by_xpath("//*[@id=\"GlobalNewTEXT\"]")

    fpcDesList=[]

    for eachFPC in fpcList:

        try:

            print str(eachFPC)

            browser.switch_to.default_content()

            searchField.click()
            searchField.send_keys(str(eachFPC))

            searchButton = browser.find_element_by_xpath('//*[@id="GTBsearchDiv"]/div/div[3]/ul/li[2]/a')
            searchButton.click()

            print 'Enovia.... load faster dammit'

            time.sleep(15)

            switchToIframeReleased()

            # html = browser.page_source
            # soup = BeautifulSoup(unicode(html), "html.parser")
            # findReleased = soup.findAll('td', attrs={'position': '5'})
            #
            # # print findReleased
            #
            # count=0
            # for each in findReleased:
            #     if each.get_text()=='Release':
            #         print 'released found'
            #         break
            #     else:
            #         count+=1
            #
            # print 'count is ' + str(count)

            releasedSpec = browser.find_element_by_xpath('//*[@id="0,0"]/td[3]/a')
            releasedSpec.click()

            time.sleep(5)

            browser.switch_to.default_content()

            # click to open EBOM
            changeManagement = browser.find_element_by_xpath('//*[@id="catMenu"]/ul/li[7]/label')
            changeManagement.click()

            time.sleep(8)

            print 'waiting for change management to load...'

            switchToIframeCM()

            checkbox = browser.find_element_by_xpath('//*[@id="rmbrow-0,0"]')
            checkbox.click()

            remove = browser.find_element_by_xpath('//*[@id="ECMContextChangeRemoveActionLink"]')
            remove.click()

            time.sleep(5)

            browser.switch_to.alert.accept()
            print "alert accepted"

            time.sleep(5)

            add = browser.find_element_by_xpath('//*[@id="ECMAddToExistingCO"]/img')
            add.click()

            mainWindow = browser.window_handles[0]

            time.sleep(5)

            window1 = browser.window_handles[1]
            browser.switch_to.window(window1)
            print 'switched to popup window'

            time.sleep(5)

            # switchToIframePopup()
            browser.switch_to.window(window1)
            Name =  browser.find_element_by_xpath('//*[@id="txtTextSearch"]')
            Name.click()
            time.sleep(3)
            Name.send_keys('CO-0081908')

            search = browser.find_element_by_xpath('//*[@id="mx_btn-search"]')
            search.click()

            time.sleep(10)

            print 'switching to iframe'
            # switch to first iframe
            iframe = browser.find_element_by_xpath('//*[@id="structure_browser"]')
            browser.switch_to.frame(iframe)

            time.sleep(5)

            circle = browser.find_element_by_xpath('//*[@id="rmbrow-0,0"]')
            circle.click()

            submit =  browser.find_element_by_xpath('//*[@id="divPageFootButtons"]/table/tbody/tr/td[2]/a/button')
            submit.click()

            # browser.close()
            time.sleep(5)
            browser.switch_to.window(mainWindow)

            # print descriptionList
            # fpcDesList.append(descriptionList)
            # print fpcDesList
            # save_list_in_excel('inputFPCs.xls',0,1,fpcDesList)
            # print str(eachFPC) + "saved"

        except:
            print str(eachFPC)+ " failed"

job()