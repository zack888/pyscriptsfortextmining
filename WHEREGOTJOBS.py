import sys
from time import sleep
from apscheduler.scheduler import Scheduler
import logging
logging.basicConfig()
import PGtest_SG, mckin_SG, jnj_SG,siemens_SG,BP_SG,Shell_SG,ABB_SG,illumina_SG,roche_SG,dhl_SG,ge_SG,cag_SG,ups_SG,appliedmaterials_SG,hp_SG,exxon_SG,SQ_SG,uber_SG,grab_SG,rr_SG
from selenium import webdriver
from selenium.webdriver import ActionChains
import threading
from threading import Thread
from selenium.webdriver.common.keys import Keys
from threading import Timer
import time

#EDITED#
import os
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

path_to_chromedriver = 'C:\\Users\\admin\\Desktop\\chromedriver.exe'
browser = webdriver.Chrome(executable_path=path_to_chromedriver)

def get_page_source(url):
    timeout_time = 30
    browser.set_page_load_timeout(timeout_time)
    actions = ActionChains(browser)
    def get_url_edit():
        print 'starting t1'
        browser.get(url)
        actions.send_keys(Keys.END)
        actions.perform()
        time.sleep(25)
        print 'ending t1'
    t1 = threading.Thread(target=get_url_edit)
    t1.setDaemon(True)
    t1.start()
    def return_page_source():
        print 'starting t2'
        time.sleep(8)
        # location = browser.find_element_by_xpath(
        #     '//*[@id="open_positions"]/div[2]/div[1]/div/div/div[3]/span/div/button')
        # location.click()
        # time.sleep(3)
        # location = browser.find_element_by_xpath(
        #     '//*[@id="open_positions"]/div[2]/div[1]/div/div/div[3]/span/div/ul/li[3]/a/label')
        # location.click()
        # time.sleep(3)
        print 'ending t2'
    t2 = Timer(timeout_time+10, return_page_source)
    t2.start()
    t2.join()
    #return browser.page_source


cwd = os.getcwd()
cred = credentials.Certificate(cwd + '/WhereGotJobs-3b11d5f78df8.json')
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://where-got-jobs-c2f17.firebaseio.com/'
})
ref = db.reference('sg')
#END EDITED#

sched = Scheduler()
sched.start()  # start the scheduler

PGtest_SG.job(ref,0,0,0,get_page_source,browser)
mckin_SG.job(ref,1,1,1,get_page_source,browser)
jnj_SG.job(ref,2,2,2,get_page_source,browser)
siemens_SG.job(ref,3,3,3,get_page_source,browser)
BP_SG.job(ref,4,4,4,get_page_source,browser)
Shell_SG.job(ref,5,5,5,get_page_source,browser)
ABB_SG.job(ref,6,6,6,get_page_source,browser)
illumina_SG.job(ref,7,7,7,get_page_source,browser)
roche_SG.job(ref,8,8,8,get_page_source,browser)
dhl_SG.job(ref,9,9,9,get_page_source,browser)
ge_SG.job(ref,10,10,10,get_page_source,browser)
cag_SG.job(ref,11,11,11,get_page_source,browser)
ups_SG.job(ref,12,12,12,get_page_source,browser)
appliedmaterials_SG.job(ref,13,13,13,get_page_source,browser)
hp_SG.job(ref,14,14,14,get_page_source,browser)
exxon_SG.job(ref,15,15,15,get_page_source,browser)
SQ_SG.job(ref,16,16,16,get_page_source,browser)
uber_SG.job(ref,17,17,17,get_page_source,browser)
#grab_SG.job(ref,18,18,18,get_page_source,path_to_chromedriver)
rr_SG.job(ref,19,19,19,get_page_source,browser)


def my_job():
    PGtest_SG.job(ref, 0, 0, 0, get_page_source, browser)
    mckin_SG.job(ref, 1, 1, 1, get_page_source, browser)
    jnj_SG.job(ref, 2, 2, 2, get_page_source, browser)
    siemens_SG.job(ref, 3, 3, 3, get_page_source, browser)
    BP_SG.job(ref, 4, 4, 4, get_page_source, browser)
    Shell_SG.job(ref, 5, 5, 5, get_page_source, browser)
    ABB_SG.job(ref, 6, 6, 6, get_page_source, browser)
    illumina_SG.job(ref, 7, 7, 7, get_page_source, browser)
    roche_SG.job(ref, 8, 8, 8, get_page_source, browser)
    dhl_SG.job(ref, 9, 9, 9, get_page_source, browser)
    ge_SG.job(ref, 10, 10, 10, get_page_source, browser)
    cag_SG.job(ref, 11, 11, 11, get_page_source, browser)
    ups_SG.job(ref, 12, 12, 12, get_page_source, browser)
    appliedmaterials_SG.job(ref, 13, 13, 13, get_page_source, browser)
    hp_SG.job(ref, 14, 14, 14, get_page_source, browser)
    exxon_SG.job(ref, 15, 15, 15, get_page_source, browser)
    SQ_SG.job(ref, 16, 16, 16, get_page_source, browser)
    uber_SG.job(ref, 17, 17, 17, get_page_source, browser)
    # grab_SG.job(ref,18,18,18,get_page_source,path_to_chromedriver)
    rr_SG.job(ref, 19, 19, 19, get_page_source, browser)

def main():
    sched.add_interval_job(my_job, hours=6)
    while True:
        sleep(1)
        # sys.stdout.write('.');
        sys.stdout.flush()


##############################################################

if __name__ == "__main__":
    main()