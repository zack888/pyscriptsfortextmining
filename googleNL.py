# -*- coding: utf-8 -*-

import os

os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="C:\\Users\\admin\\PycharmProjects\\host\\Text Mining-63e9bb436eb9.json"

import argparse
import xlrd
from xlutils.copy import copy as xl_copy
import sys
import random
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types
import six
import time

def save_column_in_excel(fileName,sheetNumber,columnNumber,columnList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=0
    for each in columnList:
        Sheet1.write(link_comments, columnNumber, each)
        link_comments += 1
    wb.save(fileName)

def read_column_in_excel(workbookName, sheetName, columnNumber):
    columnList = []
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read = 0
    while row_read < row_count:
        each = sheet.cell(row_read, columnNumber).value
        columnList.append(each)
        row_read += 1
    # return list of elements in column
    return columnList

def sentiment_text(text):
    """Detects sentiment in the text."""
    client = language.LanguageServiceClient()

    if isinstance(text, six.binary_type):
        text = text.decode('utf-8')

    # Instantiates a plain text document.
    document = types.Document(
        content=text,
        type=enums.Document.Type.PLAIN_TEXT)

    # Detects sentiment in the document. You can also analyze HTML with:
    #   document.type == enums.Document.Type.HTML
    sentiment = client.analyze_sentiment(document).document_sentiment
    lang = client.analyze_sentiment(document).language


    # print 'Score: {}'.format(sentiment.score)
    # print 'Magnitude: {}'.format(sentiment.magnitude)
    # print 'Language: {}'.format(lang)
    print sentiment.score
    return sentiment.score
    # print type(sentiment.score)

def job(excelName,sheetName):
    inputList = read_column_in_excel(excelName, sheetName, 0)
    outputList = []
    for each in inputList:
        attempt=0
        while attempt<1000:
            try:
                time.sleep(0.5)
                eachScore=sentiment_text(each)
                outputList.append(eachScore)
                break
            except:
                print "trying again"
                attempt+=1
                e = sys.exc_info()[0]
                print e


    print outputList
    save_column_in_excel(excelName, 2, 0, outputList)

# job('1519176075131.xls','yourData')