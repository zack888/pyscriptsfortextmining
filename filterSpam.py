# -*- coding: utf-8 -*-

from difflib import SequenceMatcher
import pandas as pd
import re
from collections import Counter
import xlrd
from xlutils.copy import copy as xl_copy
from datetime import datetime
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import numpy
import jieba

startTime = datetime.now()

def read_column_in_excel_with_startRow(workbookName,sheetName,columnNumber,startRow):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=startRow
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def similar(a, b, score):
    if SequenceMatcher(None, a, b).ratio() > score:
        return True
    else:
        return False

def jiebaTokenizer(text):
    text=re.sub("\W+", "", text) # remove punctuation
    return list(jieba.cut(text, cut_all=False))

def spamCheck(inputList):
    tfVectorizer = CountVectorizer(tokenizer=jiebaTokenizer)
    tfVector=tfVectorizer.fit_transform(inputList)
    
    similarity_matrix = cosine_similarity(tfVector)
    
    zz=[]
    for i in range(0, len(inputList)):
        xx=similarity_matrix[i,:]
        xx[i]=0
        #if numpy.any(xx>0.95):
        if numpy.all(xx<0.95):
            zz.append(inputList[i])
    return zz

def job(bookName,sheetName):
    column = read_column_in_excel_with_startRow(bookName,sheetName,0,0)
    print (len(column))
    column = list(set(column))
    print (len(column))
    # filteredList = []
    # for j in range(len(column)-1):
    #     for i in range(len(column)):
    #         if j!=i and similar(column[j],column[i], 0.95):
    #             break
    #             # print j
    #             # print column[j]
    #             # print i
    #             # print column[i]
    #             # print
    #             # print "*************************************
    #     else:
    #         filteredList.append(column[j])

    filteredList = spamCheck(column)

    print (len(filteredList))

    # open existing workbook
    rb = xlrd.open_workbook(bookName, formatting_info=True)
    # make a copy of it
    wb = xl_copy(rb)
    # add sheet to workbook with existing sheets
    Sheet1 = wb.add_sheet('removedSpam')

    row = 0
    for each in filteredList:
        Sheet1.write(row, 0, each)
        row += 1

    wb.save(bookName)

    print (datetime.now() - startTime)

if __name__=="__main__":
    job("test.xls",'yourData')