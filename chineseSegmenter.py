# -*- coding: utf-8 -*-

from xlutils.copy import copy as xl_copy
import xlwt
import xlrd
from nltk.tokenize import StanfordSegmenter
import time
import jieba
from openpyxl import load_workbook
from decimal import Decimal
import sys

def jieba_segment_all_comments(all_comments_list):
    super_long_string = ""
    for each in all_comments_list:
        super_long_string = super_long_string + each
    output_list = jieba.lcut(super_long_string)
    # outputs list of segmented characters
    return output_list

def segment_all_comments_simple(all_comments_list):
    output_list=[]
    super_long_string=""
    for each in all_comments_list:
        super_long_string=super_long_string+each
    super_long_string=" ".join(super_long_string)
    output_list.extend(super_long_string.split())

    #outputs list of segmented characters
    return output_list

def segment_all_comments(all_comments_list):
    output_list=[]
    jar = 'C:/Users/ZL/Desktop/For a better life/wgt interactive/textMiningForMoney/stanford-segmenter-2017-06-09/stanford-segmenter-3.8.0.jar'
    slf= 'C:/Users/ZL/Desktop/For a better life/wgt interactive/textMiningForMoney/stanford-corenlp-full-2017-06-09/slf4j-api.jar'
    segmenter = StanfordSegmenter(
        path_to_jar=jar,
        path_to_slf4j=slf,
        path_to_sihan_corpora_dict="C:/Users/ZL/Desktop/For a better life/wgt interactive/textMiningForMoney/stanford-segmenter-2017-06-09/data/",
        path_to_model="C:/Users/ZL/Desktop/For a better life/wgt interactive/textMiningForMoney/stanford-segmenter-2017-06-09/data/pku.gz",
        path_to_dict="C:/Users/ZL/Desktop/For a better life/wgt interactive/textMiningForMoney/stanford-segmenter-2017-06-09/data/dict-chris6.ser.gz"
    )
    for each in all_comments_list:
        res = segmenter.segment(each)
        res.encode('utf-8')
        output_list.extend(res.split())
    # returns list of segmented terms
    return output_list

def counter_all(inputList):
    from collections import Counter
    cnt=Counter()
    for each in inputList:
        cnt[each]+=1
    #returns a dictionary of counted words
    return cnt

def read_column_in_excel(workbookName,sheetName,columnNumber):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=0
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def create_new_workbook(fileName, sheetName):
    book = xlwt.Workbook()
    sh = book.add_sheet(sheetName)
    book.save(fileName)

def save_column_in_excel(fileName,sheetNumber,columnNumber,columnList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=0
    for each in columnList:
        Sheet1.write(link_comments, columnNumber, each)
        link_comments += 1
    wb.save(fileName)

def remove_stop_words(inputList):
    stop_words_list=read_column_in_excel('stopwords.xlsx','chinese',0)
    filtered_words = [word for word in inputList if word not in stop_words_list]
    return filtered_words

def job(excelName,sheetName):
    all_comments=read_column_in_excel(excelName,sheetName,0)
    segmented_list=jieba_segment_all_comments(all_comments)
    print segmented_list
    filtered_list=remove_stop_words(segmented_list)
    print filtered_list
    counted_terms=counter_all(filtered_list)
    print counted_terms
    sorted_terms=counted_terms.most_common()
    print sorted_terms

    all_terms=[]
    all_frequency=[]

    for each in sorted_terms:
        print 'terms '+ each[0]
        all_terms.append(each[0])
        print type(each[0])
        print 'frequency '+str(each[1])
        all_frequency.append(str(each[1]))

    save_column_in_excel(excelName, 0, 1, all_frequency)
    save_column_in_excel(excelName,0,0,all_terms)
