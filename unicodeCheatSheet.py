# -- coding: utf-8 --
import sys

reload(sys)
sys.setdefaultencoding('utf8')

string1=u"因为喜欢迪丽热巴去买的，感觉还不错，只要一直代言就会一直买飘柔。"

print string1

#因为喜欢迪丽热巴去买的，感觉还不错，只要一直代言就会一直买飘柔。

print string1.encode('unicode-escape')

#\u56e0\u4e3a\u559c\u6b22\u8fea\u4e3d\u70ed\u5df4\u53bb\u4e70\u7684\uff0c\u611f\u89c9\u8fd8\u4e0d\u9519\uff0c\u53ea\u8981\u4e00\u76f4\u4ee3\u8a00\u5c31\u4f1a\u4e00\u76f4\u4e70\u98d8\u67d4\u3002

print unicode(string1)

#因为喜欢迪丽热巴去买的，感觉还不错，只要一直代言就会一直买飘柔。

print string1.encode('utf-8')

#因为喜欢迪丽热巴去买的，感觉还不错，只要一直代言就会一直买飘柔。


#save characters from excel to text so that you can open the text doc and read the chinese chars
from codecs import open
import json
with open(str(eachItem['timestamp'])+'.json', 'w', encoding='utf-8') as fp:
    json.dump(imgJson, fp, ensure_ascii=False)