from bs4 import BeautifulSoup
import time
from selenium import webdriver
import xlrd
from xlutils.copy import copy as xl_copy
import sys
import random

pg_sg='https://pg.taleo.net/careersection/10000/jobsearch.ftl?lang=en&location=1186'

seek_urls = [pg_sg]

jobsListCount = 0
locationsCount = 0
jobTypesCount = 0
allLinksCount = 0
allTimeCount = 0
# variables to add to output json
runCount = 0
falseTrueChecks = []
listOfJobLists = []
# jsonForUpdate = {"url":pg_sg,"title": "Procter and Gamble", "updated": ""}

def job(ref,queueNum,jobCheckRow,sgJobSheetNum,get_page_source,browser):
    jsonForUpdate = {"url": pg_sg, "title": "Procter and Gamble", "updated": True, "lastChecked": "", "selfField": ""}
    jobsList = []
    locations = []
    jobTypes = []
    allLinks = []

    # variables to add to output json
    global listOfJobLists
    global runCount

    for each in seek_urls:
        get_page_source(each)
        for x in range(2,3,1):
            try:
                print x
                html = browser.page_source
                soup = BeautifulSoup(unicode(html), "html.parser")
                #name_box = soup.findAll('div', attrs={'class': 'tm-rate-content'})
                allDiv = soup.findAll('div', attrs={'class': 'absolute'})
                print allDiv

                for div in allDiv:
                    links = div.findAll('a')
                    for a in links:
                        # print a['href']
                        allLinks.append('https://pg.taleo.net'+ a['href'])

                print allLinks
                # for each in allDiv:
                #     print each.get_text()

                lengthAllDiv = len(allDiv)

                print len(allDiv)

                for each in range(0,lengthAllDiv,3):
                    jobsList.append(allDiv[each].get_text())

                print jobsList
                # variables to add to output json
                listOfJobLists.append(jobsList)
                print len(jobsList)

                for each in range(1,lengthAllDiv,3):
                    locations.append(allDiv[each].get_text())

                print locations
                print len(locations)

                for each in range(2,lengthAllDiv,3):
                    jobTypes.append(allDiv[each].get_text())

                print jobTypes
                print len(jobTypes)

                time.sleep(10+random.uniform(1.5,1.9))

            except:
                e = sys.exc_info()[0]
                print e
                break
        time.sleep(0)

    # open existing workbook
    rb = xlrd.open_workbook('SGJobs.xls', formatting_info=True)
    # make a copy of it
    wb = xl_copy(rb)
    # pull a sheet by name
    Sheet1 = wb.get_sheet(sgJobSheetNum)

    global jobsListCount
    global locationsCount
    global jobTypesCount
    global allLinksCount
    global allTimeCount

    for each in jobsList:
        Sheet1.write(jobsListCount, 0, each)
        jobsListCount += 1

    for each in locations:
        Sheet1.write(locationsCount, 1, each)
        locationsCount += 1

    for each in jobTypes:
        Sheet1.write(jobTypesCount, 2, each)
        jobTypesCount += 1

    for each in allLinks:
        Sheet1.write(allLinksCount, 3, each)
        Sheet1.write(allLinksCount, 4, time.asctime( time.localtime(time.time()) ))
        allLinksCount += 1

    wb.save("SGJobs.xls")

    # chunk of code that checks for updates
    if runCount > 0:
        print "checking if there has been any updates in jobs"
        if listOfJobLists[runCount] == listOfJobLists[runCount - 1]:
            print "False"
            falseTrueChecks.append('False')
            jsonForUpdate["updated"] = False
        else:
            print "True"
            falseTrueChecks.append('True')
            jsonForUpdate["updated"] = True
    print "The runcount is: " + str(runCount)
    runCount += 1
    print falseTrueChecks
    print "jsonForUpdate = " + str(jsonForUpdate["updated"])

    # open existing workbook
    rb = xlrd.open_workbook('JobsCheck.xls', formatting_info=True)
    # make a copy of it
    wb = xl_copy(rb)
    # pull a sheet by name
    Sheet1 = wb.get_sheet(0)

    Sheet1.write(jobCheckRow, 0, jsonForUpdate['title'])
    Sheet1.write(jobCheckRow, 1, jsonForUpdate['updated'])
    Sheet1.write(jobCheckRow, 2, time.asctime( time.localtime(time.time())))
    jsonForUpdate["lastChecked"]=time.asctime(time.localtime(time.time()))

    wb.save("JobsCheck.xls")
    # EDITED#

    users_ref = ref.child('items').child(str(queueNum)).set(jsonForUpdate)
    print 'sent to backend'
