import xlrd
from xlutils.copy import copy as xl_copy
import plotly
import plotly.graph_objs as go

rb = xlrd.open_workbook('dove.xls', formatting_info=True)
    # make a copy of it
sheet = rb.sheet_by_name("frequencyList")

row_read=0
wordList=[]
countList=[]

sheet2 = rb.sheet_by_name("yourData")
comments_rows = float(len(sheet2.col_values(0)))

while row_read < 10:
    each=sheet.cell(row_read,0).value
    print each
    wordList.append(each)
    eachCount = sheet.cell(row_read, 1).value
    eachCountInt=float(eachCount)
    print eachCountInt
    eachCountIntPercentage=(eachCountInt/comments_rows)*100
    print eachCountIntPercentage
    countList.append(round(eachCountIntPercentage,1))
    row_read+=1

data = [go.Bar(
        x=wordList,
        y=countList,
        text=countList,
        textposition = 'auto',
                    marker=dict(
                        color='rgb(26, 118, 255)',
                    )
    )]

layout = go.Layout(
    title='Percentage of Each Term',
    font=dict(family='Helvetica Neue, Helvetica', size=30, color='#000000')
)

fig = go.Figure(data=data, layout=layout)

plotly.offline.plot(fig, filename='WordList')