# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import time
from selenium import webdriver
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import xlwt
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from time import sleep
import threading
from threading import Thread
from threading import Timer
from selenium.webdriver import ActionChains
import xlrd
from xlutils.copy import copy as xl_copy
import sys as sys
import xlwt
import random
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
import smtplib
from email import encoders
import chineseSegmenter
from selenium.common.exceptions import NoSuchElementException
from openpyxl import load_workbook
from decimal import Decimal
import base64
import testEmail
import testEmailPictures
import testEmailNoPics
import time
import pyautogui
import json
import os

import sys
reload(sys)
sys.setdefaultencoding("utf8")

# path_to_chromedriver = 'C:\Users\ZL\Desktop\geckodriver.exe'
# browser = webdriver.Firefox(executable_path = path_to_chromedriver)
# browser.set_page_load_timeout(10)
# timeout_time=10

path_to_chromedriver = 'C:\\Users\\admin\\Desktop\\chromedriver.exe'
chrome_options = webdriver.ChromeOptions()
prefs = {"profile.default_content_setting_values.notifications" : 2}
chrome_options.add_experimental_option("prefs",prefs)
browser = webdriver.Chrome(executable_path = path_to_chromedriver,chrome_options=chrome_options)


# browser.get('chrome://settings/advanced')
# browser.find_element_by_id('privacyContentSettingsButton').click()
# browser.find_element_by_name('popups').click()
timeout_time=5
browser.set_page_load_timeout(timeout_time)
actions=ActionChains(browser)
wait60 = WebDriverWait(browser, 60)
wait10 = WebDriverWait(browser, 10)
wait5 = WebDriverWait(browser, 5)

def read_column_in_excel_with_startRow(workbookName,sheetName,columnNumber,startRow):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = len(sheet.col_values(0))
    row_read=startRow
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def read_column_in_excel_limited(workbookName,sheetName,columnNumber,rowMax):
    columnList=[]
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    row_count = rowMax
    row_read=0
    while row_read < row_count:
        each=sheet.cell(row_read,columnNumber).value
        columnList.append(each)
        row_read+=1
    #return list of elements in column
    return columnList

def save_column_in_excel(fileName,sheetNumber,columnNumber,columnList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=25
    for each in columnList:
        Sheet1.write(link_comments, columnNumber, each)
        link_comments += 1
    wb.save(fileName)

def save_row_in_excel(fileName,sheetNumber,rowNumber,rowList):
    rb = xlrd.open_workbook(fileName, formatting_info=True)
    wb = xl_copy(rb)
    Sheet1 = wb.get_sheet(sheetNumber)
    link_comments=3
    for each in rowList:
        Sheet1.write(rowNumber, link_comments, each)
        link_comments += 1
    wb.save(fileName)

def save_row_in_xlsx(loadExcel,inputList,rowNum,columnNum):
    #Open an xlsx for reading
    wb = load_workbook(filename = loadExcel)
    #Get the current Active Sheet
    #ws = wb.get_active_sheet()
    #You can also select a particular sheet
    #based on sheet name
    ws = wb.get_sheet_by_name("steve")
    #save the csb file
    for col, val in enumerate(inputList, start=columnNum):
        ws.cell(row=rowNum, column=col).value = val
    wb.save(loadExcel)

def save_col_in_xlsx(loadExcel,inputList,rowNum,columnNum):
    #Open an xlsx for reading
    wb = load_workbook(filename = loadExcel)
    #Get the current Active Sheet
    #ws = wb.get_active_sheet()
    #You can also select a particular sheet
    #based on sheet name
    ws = wb.get_sheet_by_name("steve")
    #save the csb file
    for row, val in enumerate(inputList, start=rowNum):
        ws.cell(row=row, column=columnNum).value = val
    wb.save(loadExcel)

def checkCellValue(workbookName,sheetName,rowNum,colNum):
    rb = xlrd.open_workbook(workbookName)
    sheet = rb.sheet_by_name(sheetName)
    cellval = sheet.cell(rowNum,colNum).value

    if cellval!=int or cellval!=float or cellval!=long:
        cellval=0

    print type(cellval)
    print int(cellval)
    print type(cellval)
    return cellval

def check_exists_by_class(classname):
    try:
        browser.find_element_by_class_name(classname)
    except NoSuchElementException:
        return False
    return True

def check_exists_by_id(id):
    try:
        browser.find_element_by_id(id)
    except NoSuchElementException:
        return False
    return True

def check_exists_by_xpath(xpath):
    try:
        browser.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True

def close_popup_clickable():
    try:
        browser.find_element_by_class_name('sufei-tb-dialog-close').click()
        return True
    except:
        print "Element is not clickable"
        return False

def check_exists_by_partial_link_text(text):
    try:
        browser.find_element_by_partial_link_text(text)
    except NoSuchElementException:
        return False
    return True

def check_exists_by_link_text(text):
    try:
        browser.find_element_by_link_text(text)
    except NoSuchElementException:
        return False
    return True

def get_page_source(url):
    def get_url_edit():
        print 'starting t1'
        print 'getting tmall url'
        browser.get(url)
        print 'ending t1'
    t1 = threading.Thread(target=get_url_edit)
    t1.setDaemon(True)
    t1.start()
    def return_page_source():
        print 'starting t2'
        # first_review = browser.find_element_by_class_name('tm-rate-fulltxt').text
        # print first_review
        print 'ending t2'
    t2 = Timer(timeout_time+5, return_page_source)
    t2.start()
    t2.join()
    #return browser.page_source

def create_new_workbook(fileName, sheetName):
    book = xlwt.Workbook()
    sh = book.add_sheet(sheetName)
    book.save(fileName)

def signInFB(inputUserName,inputPassword):

    usernameField = browser.find_element_by_xpath("//*[@id=\"email\"]")
    usernameField.click()
    usernameField.send_keys(inputUserName)

    time.sleep(1)

    passwordField = browser.find_element_by_xpath("//*[@id=\"pass\"]")
    passwordField.click()
    passwordField.send_keys(inputPassword)

    time.sleep(1)

    try:
        login = browser.find_element_by_id('loginbutton')
        login.click()
    except:
        pass

    time.sleep(2)

    browser.switch_to.default_content()

# def open_posts(inputSearch):
def open_posts():
    # searchField = browser.find_element_by_css_selector('._1frb')
    # searchField.send_keys(inputSearch)
    # time.sleep(2)
    # actions = ActionChains(browser)
    # actions.send_keys(Keys.ENTER)
    # actions.perform()
    # time.sleep(2)
    attempts = 5
    while not check_exists_by_class('_5f9g') and attempts>0:
        browser.find_element_by_link_text('Posts').click()
        time.sleep(2)
        actions = ActionChains(browser)
        actions.send_keys(Keys.END)
        actions.perform()
        time.sleep(3)
        actions.send_keys(Keys.END)
        actions.perform()
        time.sleep(2)
        seeAll = browser.find_element_by_class_name('_5f9g')
        actions.move_to_element(seeAll)
        actions.perform()
        time.sleep(2)
        attempts-=1
    # browser.find_element_by_link_text('See all').click()
    seeAll = browser.find_element_by_class_name('_5f9g')
    actions = ActionChains(browser)
    actions.move_to_element(seeAll).perform()
    try:
        seeAll.click()
    except:
        time.sleep(5)
        browser.find_element_by_link_text('See all')
    time.sleep(10)

def load_articles_facebook(numTimes):
    for i in range(numTimes):
        try:
            articles = browser.find_elements_by_partial_link_text('Comments')
            actions = ActionChains(browser)
            actions.move_to_element(articles[-1]).perform()
            time.sleep(5)
        except:
            print 'Failed to move to comment '+str(articles[-1])
            print sys.exc_info()[0]

def scroll_element_into_view(linkTextInput):
    try:
        print 'checking if item is in view'
        scrollTo = browser.find_element_by_link_text(linkTextInput)
        actions = ActionChains(browser)
        actions.move_to_element(scrollTo).perform()
        return True
    except:
        return False

class StoreBef:
    def __init__(self,StoreBef):
        self.b = StoreBef

class StoreAft:
    def __init__(self,StoreAft):
        self.a = StoreAft

def addAbitScrollHeight(className,scrollHeight):
    try:
        nextPage = browser.find_element_by_class_name(className)
        actions = ActionChains(browser)
        actions.move_to_element(nextPage).perform()
        nextPage.click()
    except:
        print 'trying to add abit to scroll height'
        scrollHeight += 10
        browser.execute_script("window.scrollTo(0, " + str(scrollHeight) + ");")
        time.sleep(2)
        nextPage = browser.find_element_by_class_name(className)
        actions = ActionChains(browser)
        actions.move_to_element(nextPage).perform()
        nextPage.click()

def view_more_comments():
    view_more_comments = browser.find_elements_by_class_name('UFIPagerLink')
    actions = ActionChains(browser)
    actions.move_to_element(view_more_comments[-1]).perform()
    print "Clicking view more comments"
    view_more_comments[-1].click()
    time.sleep(10)
    # open up all the replies
    replies = browser.find_elements_by_class_name('UFIReplySocialSentenceVerified')
    for each in replies:
        try:
            print 'opening/ closing reply'
            each.click()
        except:
            print 'can\'t open/close reply'
        time.sleep(5)

def facebook_job(urls,timestamp):
    total_comments = []
    articles_to_comments = {}

    get_page_source('https://www.facebook.com')
    signInFB('zltan12340@gmail.com', 'jimpoland')
    time.sleep(1)

    for eachUrl in urls:
        get_page_source(eachUrl)
        open_posts()
        load_articles_facebook(3) # can't be too high else browser will lag

        # get each main article before opening comments
        html = browser.page_source
        soup = BeautifulSoup(html, "html.parser")
        allArticles = soup.findAll('div', attrs={'class': '_5pcr userContentWrapper'})

        # comment_buttons = browser.find_elements_by_partial_link_text('Comments')
        comment_buttons = browser.find_elements_by_class_name('_-56')
        # click on view comments for each post first
        for i in range(len(comment_buttons)):
            try:
                print comment_buttons[i].text
                print "Clicking comment button"
                actions = ActionChains(browser)
                actions.move_to_element(comment_buttons[i]).perform()
                comment_buttons[i].click()
                time.sleep(5)
                try:
                    view_more_comments()
                    # try again
                    view_more_comments()
                    # try again
                    view_more_comments()
                    # try again
                    view_more_comments()
                    # try again
                    view_more_comments()
                except:
                    print "Seems like no more comments to open"
                print "saving article and comments into dict"
                html = browser.page_source
                soup = BeautifulSoup(html, "html.parser")
                allComments = soup.findAll('span', attrs={'class': 'UFICommentBody'})
                articles_to_comments[allArticles[i].get_text()] = [eachComment.get_text() for eachComment in allComments]
                time.sleep(10)
                # print "Clicking on comment button to close comments"
                # # seems like when you click on the comments button again, the comments disappear from html
                # actions = ActionChains(browser)
                # actions.move_to_element(comment_buttons[i]).perform()
                # time.sleep(10)
                # comment_buttons[i].click()
                # print comment_buttons[i].text + "Comments closed"
            except:
                print "**********EXCEPTION************"

                print sys.exc_info()[0]

        # just get all comments
        html = browser.page_source
        soup = BeautifulSoup(html, "html.parser")
        allComments = soup.findAll('span', attrs={'class': 'UFICommentBody'})
        for each in allComments:
            total_comments.append(each.get_text())

        for eachComment in total_comments:
            if total_comments != []:
                print eachComment.encode("utf-8")
                print '****************************'
                filename = timestamp + 'Comments.txt'
                if os.path.exists(filename):
                    append_write = 'a'  # append if already exists
                else:
                    append_write = 'w'  # make a new file if not
                file = open(filename, append_write)
                file.write(eachComment.encode("utf-8") + '\n')
                file.close()
            else:
                print "total_comments list is empty"

        filename = timestamp + 'ArtiNComm.json'
        # if os.path.exists(filename):
        #     append_write = 'a'  # append if already exists
        # else:
        #     append_write = 'w'  # make a new file if not

        # cannot append else will end up with a few dicts in the json file
        with open(filename, 'w') as outfile:
            json.dump(articles_to_comments, outfile)

    return timestamp+'ArtiNComm.json'
# facebook_job(['https://www.facebook.com/search/str/avengers/keywords_search'],'11111')

    # chineseSegmenter.job(excelName+".xls",'yourData')

    # addSheet(excelName+".xls","sentiScores")

    # sentiGen.job(excelName+".xls",'yourData',get_page_source_repustate,browser)

    # googleNL.job(excelName+".xls",'yourData')

    # print "Generating graphs start"

    # genGraphandTable.job(excelName+".xls")

    # emailSuccess = testEmail.job(excelName + ".xls", recipientEmail, title)
    #
    # if emailSuccess == False:
    #     import sys
    #     sys.exit()